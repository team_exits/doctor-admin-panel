;(this['webpackJsonpdr-app-admin-panel'] =
  this['webpackJsonpdr-app-admin-panel'] || []).push([
  [0],
  {
    102: function (e, t, a) {},
    1046: function (e, t, a) {},
    1251: function (e, t, a) {
      'use strict'
      a.r(t)
      var n = a(1),
        i = a.n(n),
        c = a(17),
        l = a.n(c),
        o = (a(694), a(484), a(30)),
        s = a(53),
        r = a(65),
        d = (a(675), a(727), a(2)),
        p = new Date(),
        j =
          (new Date(p.getFullYear(), p.getMonth(), p.getDate() - 7),
          a(729),
          a(31)),
        u = a(408),
        b = a.n(u),
        h = a(677),
        O = a(81),
        m = a(12),
        f = a(316),
        x = Object(O.a)(function (e) {
          return {
            root: { flexGrow: 1 },
            search: Object(j.a)(
              {
                position: 'relative',
                borderRadius: e.shape.borderRadius,
                backgroundColor: Object(m.e)(e.palette.common.white, 0.15),
                '&:hover': {
                  backgroundColor: Object(m.e)(e.palette.common.white, 0.25),
                },
                marginLeft: 0,
                width: '100%',
              },
              e.breakpoints.up('sm'),
              { marginLeft: e.spacing(1), width: 'auto' }
            ),
            searchIcon: {
              padding: e.spacing(0, 2),
              height: '100%',
              position: 'absolute',
              pointerEvents: 'none',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            },
            inputRoot: { color: 'inherit' },
            inputInput: Object(j.a)(
              {
                padding: e.spacing(1, 1, 1, 0),
                paddingLeft: 'calc(1em + '.concat(e.spacing(4), 'px)'),
                transition: e.transitions.create('width'),
                width: '100%',
              },
              e.breakpoints.up('sm'),
              { width: '12ch', '&:focus': { width: '10ch' } }
            ),
          }
        })
      function g() {
        Object(s.g)()
        var e = x()
        return Object(d.jsxs)('div', {
          className: 'feed',
          children: [
            Object(d.jsxs)('div', {
              className: 'searchbar',
              children: [
                Object(d.jsx)('div', {
                  className: 'left',
                  children: Object(d.jsxs)('div', {
                    className: e.search,
                    children: [
                      Object(d.jsx)('div', {
                        className: e.searchIcon,
                        children: Object(d.jsx)(b.a, {}),
                      }),
                      Object(d.jsx)(h.a, {
                        placeholder: 'Search\u2026',
                        classes: { root: e.inputRoot, input: e.inputInput },
                        inputProps: { 'aria-label': 'search' },
                      }),
                    ],
                  }),
                }),
                Object(d.jsx)('div', {
                  className: 'right',
                  children: Object(d.jsxs)('h5', {
                    style: { marginTop: 5, fontSize: 15 },
                    children: [Object(d.jsx)(f.a, {}), ' Set Date'],
                  }),
                }),
              ],
            }),
            Object(d.jsxs)('div', {
              className: 'middle-bar',
              children: [
                Object(d.jsxs)('div', {
                  className: 'outside',
                  children: [
                    Object(d.jsx)('h1', {
                      style: { marginTop: 15, color: 'white' },
                      children: 'WELCOME',
                    }),
                    Object(d.jsx)('p', {
                      style: { marginTop: 15, color: 'white' },
                    }),
                    Object(d.jsxs)('div', {
                      className: 'box',
                      children: [
                        Object(d.jsxs)('div', {
                          className: 'inside-box',
                          style: {
                            marginTop: 15,
                            height: 60,
                            width: 180,
                            backgroundColor: 'white',
                            borderRadius: '10px',
                          },
                          children: [
                            Object(d.jsx)('div', {
                              className: 'inside-box-1',
                              children: Object(d.jsx)('h5', {
                                style: {
                                  fontSize: '20px',
                                  paddingLeft: '1rem',
                                  width: '50%',
                                  color: '#fff',
                                  border: '1px solid',
                                  borderRadius: ' 6px',
                                  padding: '4px',
                                  textAlign: 'center',
                                  background: '#5c65fe',
                                  marginRight: '10px',
                                },
                                children: '25',
                              }),
                            }),
                            Object(d.jsxs)('div', {
                              className: 'inside-box-2',
                              children: [
                                Object(d.jsx)('h5', { children: 'Amazon' }),
                                Object(d.jsx)('p', {
                                  style: { fontSize: 10 },
                                  children: 'Books',
                                }),
                              ],
                            }),
                          ],
                        }),
                        Object(d.jsxs)('div', {
                          className: 'inside-box-main',
                          style: {
                            marginTop: 15,
                            paddingleft: 30,
                            height: 60,
                            width: 180,
                            backgroundColor: 'white',
                            borderRadius: '10px',
                          },
                          children: [
                            Object(d.jsx)('div', {
                              className: 'inside-box-3',
                              children: Object(d.jsx)('h5', {
                                style: {
                                  fontSize: '20px',
                                  paddingLeft: '1rem',
                                  width: '50%',
                                  color: '#fff',
                                  border: '1px solid',
                                  borderRadius: ' 6px',
                                  padding: '4px',
                                  textAlign: 'center',
                                  background: '#5c65fe',
                                },
                                children: '25',
                              }),
                            }),
                            Object(d.jsxs)('div', {
                              className: 'inside-box-4',
                              children: [
                                Object(d.jsx)('h5', {
                                  children: 'Total Task :20',
                                }),
                                Object(d.jsx)('p', {
                                  style: { fontSize: 10 },
                                  children: 'Pending Task',
                                }),
                              ],
                            }),
                          ],
                        }),
                      ],
                    }),
                  ],
                }),
                Object(d.jsxs)('div', {
                  className: 'inside',
                  children: [
                    Object(d.jsx)(f.b, {
                      size: 40,
                      color: 'white',
                      style: { margin: 30 },
                    }),
                    Object(d.jsx)('h3', {
                      style: { color: 'white', marginTop: 5, paddingLeft: 20 },
                      children: 'Orders: 20',
                    }),
                    Object(d.jsx)('h6', {
                      style: { color: 'white', marginTop: 10, paddingLeft: 20 },
                      children: 'Processed Orders : 20',
                    }),
                    Object(d.jsx)('h6', {
                      style: { color: 'white', paddingLeft: 20 },
                      children: 'Pending Orders : 40',
                    }),
                    Object(d.jsx)('br', {}),
                  ],
                }),
              ],
            }),
            Object(d.jsx)('div', { className: 'end-bar' }),
          ],
        })
      }
      var v = function () {
          return Object(d.jsx)('div', {
            style: { flex: 1, overflowY: 'scroll', display: 'flex' },
            children: Object(d.jsx)('div', {
              style: { flex: 0.6 },
              children: Object(d.jsx)(g, {}),
            }),
          })
        },
        y = a(657),
        T = a.n(y),
        w = a(656),
        S = a.n(w),
        C = a(655),
        P = a.n(C),
        A = a(249),
        I = a(658),
        k = a.n(I),
        N = (a(731), a(25)),
        D = a.n(N),
        E = a(59),
        R = a(143),
        F = a(20),
        V = a.n(F),
        L = { IP: '13.232.211.114', PORT: '3000' },
        q = Object(R.b)({
          name: 'login',
          initialState: {
            login: null,
            loading: !1,
            isLogged: !1,
            token: null,
            user: null,
            error: null,
          },
          reducers: {
            setLogin: function (e, t) {
              e.login = t.payload.login
            },
            setLogout: function (e) {
              ;(e.login = null), (e.isLogged = !1)
            },
            setLoading: function (e, t) {
              e.loading = t.payload
            },
            setIsLogged: function (e, t) {
              ;(e.login = t.payload.login),
                (e.isLogged = t.payload.loggedIn),
                (e.token = t.payload.token)
            },
          },
        }),
        B = q.actions,
        U = (B.setLogin, B.setLogout),
        M = B.setLoading,
        G = B.setIsLogged,
        z = B.setError,
        H = q.reducer,
        _ = function () {
          var e = Object(s.g)(),
            t = Object(r.b)()
          return Object(d.jsxs)('div', {
            className: 'navbar',
            children: [
              Object(d.jsx)('div', {
                children: Object(d.jsx)('h1', {
                  onClick: function () {
                    return e.push('/')
                  },
                  style: {
                    color: '#F2F3F4',
                    paddingLeft: '2rem',
                    cursor: 'pointer',
                  },
                  children: 'Vedsys',
                }),
              }),
              Object(d.jsxs)('div', {
                style: { display: 'flex', alignItems: 'center' },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)(A.a, {
                      children: Object(d.jsx)(P.a, {
                        style: { color: '#fff' },
                      }),
                    }),
                  }),
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)(A.a, {
                      children: Object(d.jsx)(S.a, {
                        style: { color: '#fff' },
                      }),
                    }),
                  }),
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)(A.a, {
                      children: Object(d.jsx)(T.a, {
                        style: { color: '#fff' },
                      }),
                    }),
                  }),
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)(A.a, {
                      onClick: function () {
                        return t(U())
                      },
                      children: Object(d.jsx)(k.a, {
                        style: { color: '#fff' },
                      }),
                    }),
                  }),
                ],
              }),
            ],
          })
        },
        W = a(18),
        Q = a(37),
        Y = (a(750), a(317)),
        K = a(172),
        X = a(126),
        J =
          (a(751),
          function () {
            var e = Object(s.g)(),
              t = i.a.useState(!1),
              a = Object(W.a)(t, 2),
              n = a[0],
              c = a[1]
            return Object(d.jsx)(Q.c, {
              collapsed: n,
              children: Object(d.jsxs)(Q.a, {
                iconShape: 'square',
                children: [
                  Object(d.jsx)(Q.b, {
                    iconShape: 'round',
                    onClick: function () {
                      c(!n)
                    },
                    icon: n ? Object(d.jsx)(Y.a, {}) : Object(d.jsx)(Y.b, {}),
                    children: Object(d.jsx)('div', {
                      style: { padding: '0.5rem' },
                      children: Object(d.jsx)('h4', {
                        style: { color: '#fff' },
                        children: 'Vedsys',
                      }),
                    }),
                  }),
                  Object(d.jsx)(Q.b, {
                    onClick: function () {
                      return e.push('/')
                    },
                    iconShape: 'circle',
                    icon: Object(d.jsx)(K.a, {}),
                    children: 'Dashboard',
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Medicine',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-medicine')
                        },
                        iconShape: 'round',
                        children: 'View Medicine',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-medicine')
                        },
                        iconShape: 'round',
                        children: 'Add Medicine',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Vitallog',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-Vitallog')
                        },
                        iconShape: 'round',
                        children: 'View Vital Log',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-Vitallog')
                        },
                        iconShape: 'round',
                        children: 'Add Vital Log',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Category',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-category')
                        },
                        iconShape: 'round',
                        children: 'View Category',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-category')
                        },
                        iconShape: 'round',
                        children: 'Add Category',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Prescription',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-prescription')
                        },
                        iconShape: 'round',
                        children: 'View Prescription',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-prescription')
                        },
                        iconShape: 'round',
                        children: 'Add Prescription',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Medicine History',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-medhistory')
                        },
                        iconShape: 'round',
                        children: 'View Medicine History',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-medhistory')
                        },
                        iconShape: 'round',
                        children: 'Add Medicine History',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Order',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-Order')
                        },
                        iconShape: 'round',
                        children: 'View Order',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-Order')
                        },
                        iconShape: 'round',
                        children: 'Add Order',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'OrderItem',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-OrderItem')
                        },
                        iconShape: 'round',
                        children: 'View OrderItem',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-OrderItem')
                        },
                        iconShape: 'round',
                        children: 'Add OrderItem',
                      }),
                    ],
                  }),
                  Object(d.jsxs)(Q.d, {
                    title: 'Delivery',
                    icon: Object(d.jsx)(X.a, {}),
                    children: [
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/view-delivery')
                        },
                        iconShape: 'round',
                        children: 'View Delivery',
                      }),
                      Object(d.jsx)(Q.b, {
                        onClick: function () {
                          return e.push('/add-delivery')
                        },
                        iconShape: 'round',
                        children: 'Add Delivery',
                      }),
                    ],
                  }),
                ],
              }),
            })
          }),
        Z = a(180),
        $ = a(1255),
        ee = a(1284),
        te = a(1294),
        ae = a(7),
        ne = a(203),
        ie = a(58),
        ce = a(8),
        le = a(108)
      var oe = function (e) {
          var t = e.children
          return Object(d.jsx)('div', { className: 'error', children: t })
        },
        se = ['label', 'name', 'value'],
        re = function (e) {
          var t = e.label,
            a = e.name,
            n = (e.value, Object(le.a)(e, se))
          return Object(d.jsxs)('div', {
            className: 'form-control',
            children: [
              Object(d.jsx)('label', { htmlFor: a, children: t }),
              Object(d.jsx)(
                ie.b,
                Object(ce.a)({ className: 'input', id: a, name: a }, n)
              ),
              Object(d.jsx)(ie.a, { name: a, component: oe }),
            ],
          })
        },
        de = ['label', 'name']
      var pe = function (e) {
          var t = e.label,
            a = e.name,
            n = Object(le.a)(e, de)
          return Object(d.jsxs)('div', {
            className: 'form-control',
            children: [
              Object(d.jsx)('label', { htmlFor: a, children: t }),
              Object(d.jsx)(
                ie.b,
                Object(ce.a)({ as: 'textarea', id: a, name: a }, n)
              ),
              Object(d.jsx)(ie.a, { component: oe, name: a }),
            ],
          })
        },
        je = i.a.createContext()
      function ue(e) {
        var t = e.children,
          a = i.a.useState(!1),
          n = Object(W.a)(a, 2),
          c = n[0],
          l = n[1],
          o = i.a.useState({}),
          s = Object(W.a)(o, 2),
          r = s[0],
          p = s[1]
        return Object(d.jsx)(je.Provider, {
          value: {
            show: c,
            setShow: l,
            close: function () {
              return l(!1)
            },
            allSelect: function (e, t, a) {
              p({ value: e, name: t, level: a })
            },
            options: r,
          },
          children: t,
        })
      }
      var be = [
        'label',
        'name',
        'options',
        'level',
        'handleMcq',
        'handleStandard',
        'handleValue',
        'title',
      ]
      var he = function (e) {
          var t = e.label,
            a = e.name,
            n = e.options,
            c = e.level,
            l = e.handleMcq,
            o = e.handleStandard,
            s = e.handleValue,
            r = e.title,
            p = Object(le.a)(e, be),
            j = Object(ie.e)(e),
            u = Object(W.a)(j, 1)[0]
          console.log(r)
          var b = r || null,
            h = i.a.useContext(je),
            O = (h.show, h.close, h.setShow, h.allSelect)
          return (
            i.a.useEffect(
              function () {
                void 0 !== u.value &&
                  (console.log(u.value),
                  b &&
                    ('Standard' == u.value ? o() : 'MCQ' == u.value ? l() : s(),
                    O(u.value, a, c)))
              },
              [u.value]
            ),
            Object(d.jsxs)('div', {
              className: 'form-control',
              children: [
                Object(d.jsx)('label', { htmlFor: a, children: t }),
                Object(d.jsx)(
                  ie.b,
                  Object(ce.a)(
                    Object(ce.a)(
                      { as: 'select', id: a, name: a, className: 'select' },
                      p
                    ),
                    {},
                    {
                      children: n.map(function (e, t) {
                        return 0 === t
                          ? Object(d.jsx)(
                              'option',
                              { value: e.value, selected: !0, children: e.key },
                              e.value
                            )
                          : Object(d.jsx)(
                              'option',
                              { value: e.value, children: e.key },
                              e.value
                            )
                      }),
                    }
                  )
                ),
                Object(d.jsx)(ie.a, { component: oe, name: a }),
              ],
            })
          )
        },
        Oe = ['label', 'name', 'options', 'level']
      var me = function (e) {
          var t = e.label,
            a = e.name,
            n = e.options,
            c = e.level,
            l = Object(le.a)(e, Oe),
            o = i.a.useState(''),
            s = Object(W.a)(o, 2),
            r = s[0],
            p = s[1],
            j = Object(ie.e)(e),
            u = (Object(W.a)(j, 1)[0], i.a.useContext(je).allSelect)
          return (
            i.a.useEffect(
              function () {
                console.log(r), void 0 !== r && u(r, a, c)
              },
              [r]
            ),
            Object(d.jsxs)('div', {
              className: 'form-control',
              children: [
                Object(d.jsx)('label', { children: t }),
                Object(d.jsx)(ie.b, {
                  name: a,
                  children: function (e) {
                    var t = e.field,
                      a = t.value || 'CNR_NO'
                    return (
                      p(a),
                      n.map(function (e) {
                        return Object(d.jsxs)(
                          i.a.Fragment,
                          {
                            children: [
                              Object(d.jsx)(
                                'input',
                                Object(ce.a)(
                                  Object(ce.a)(
                                    Object(ce.a)(
                                      { type: 'radio', id: e.value },
                                      t
                                    ),
                                    l
                                  ),
                                  {},
                                  { value: e.value, checked: a === e.value }
                                )
                              ),
                              Object(d.jsx)('label', {
                                htmlFor: e.value,
                                children: e.key,
                              }),
                            ],
                          },
                          e.key
                        )
                      })
                    )
                  },
                }),
                Object(d.jsx)(ie.a, { component: oe, name: a }),
              ],
            })
          )
        },
        fe = ['label', 'name', 'options']
      var xe = function (e) {
          var t = e.label,
            a = e.name,
            n = e.options,
            c = Object(le.a)(e, fe)
          return Object(d.jsxs)('div', {
            className: 'form-control',
            children: [
              Object(d.jsx)('label', { children: t }),
              Object(d.jsx)(ie.b, {
                name: a,
                children: function (e) {
                  var t = e.field
                  return n.map(function (e) {
                    return Object(d.jsxs)(
                      i.a.Fragment,
                      {
                        children: [
                          Object(d.jsx)(
                            'input',
                            Object(ce.a)(
                              Object(ce.a)(
                                Object(ce.a)(
                                  { type: 'checkbox', id: e.value },
                                  t
                                ),
                                c
                              ),
                              {},
                              {
                                value: e.value,
                                checked: t.value.includes(e.value),
                              }
                            )
                          ),
                          Object(d.jsx)('label', {
                            htmlFor: e.value,
                            children: e.key,
                          }),
                        ],
                      },
                      e.key
                    )
                  })
                },
              }),
              Object(d.jsx)(ie.a, { component: oe, name: a }),
            ],
          })
        },
        ge = function () {
          return Object(d.jsx)('div', {})
        },
        ve = function () {
          return Object(d.jsx)('div', {})
        },
        ye = ['control'],
        Te = function (e) {
          var t = e.control,
            a = Object(le.a)(e, ye)
          switch (t) {
            case 'input':
              return Object(d.jsx)(re, Object(ce.a)({}, a))
            case 'textarea':
              return Object(d.jsx)(pe, Object(ce.a)({}, a))
            case 'select':
              return Object(d.jsx)(he, Object(ce.a)({}, a))
            case 'radio':
              return Object(d.jsx)(me, Object(ce.a)({}, a))
            case 'checkbox':
              return Object(d.jsx)(xe, Object(ce.a)({}, a))
            case 'date':
              return Object(d.jsx)(ge, Object(ce.a)({}, a))
            case 'chakraInput':
              return Object(d.jsx)(ve, Object(ce.a)({}, a))
            default:
              return console.log('default'), null
          }
        },
        we = a(497),
        Se =
          (a(752),
          Object(ae.a)(function (e) {
            return {
              root: {
                color: e.palette.getContrastText(we.a[500]),
                backgroundColor: '#3246D3',
                '&:hover': { backgroundColor: '#3457D5' },
              },
            }
          })(Z.a)),
        Ce = function (e) {
          var t = e.inputData,
            a = e.onSubmit,
            n = e.handleStandard,
            i = e.handleMcq,
            c = e.handleValue
          console.log(t.initialInput)
          var l = function (e) {
            var t
            return e || (t = 'Required'), t
          }
          return Object(d.jsx)(ie.d, {
            enableReinitialize: !0,
            initialValues: t.initialInput || {},
            onSubmit: a,
            children: function (e) {
              return Object(d.jsxs)(ie.c, {
                children: [
                  Object(d.jsx)('div', {
                    className: t.columns,
                    children: t.formData.map(function (e) {
                      return Object(d.jsx)(d.Fragment, {
                        children:
                          !e.hideField &&
                          Object(d.jsx)(Te, {
                            control: e.control,
                            type: e.type,
                            label: e.label,
                            name: e.name,
                            options: e.options,
                            validate: l,
                            level: e.level,
                            checked: e.checked,
                            handleStandard: n,
                            handleMcq: i,
                            handleValue: c,
                            title: t.title,
                          }),
                      })
                    }),
                  }),
                  Object(d.jsx)(Se, {
                    fullWidth: !0,
                    type: 'submit',
                    children: Object(d.jsx)('b', { children: 'Submit' }),
                  }),
                ],
              })
            },
          })
        },
        Pe = i.a.memo(Ce),
        Ae = a(211),
        Ie =
          (a(753),
          Object(O.a)(function (e) {
            return Object(Ae.a)({
              backdrop: { zIndex: e.zIndex.drawer + 1, color: '#fff' },
            })
          })),
        ke =
          (Object(ae.a)(function (e) {
            return {
              root: {
                color: e.palette.getContrastText(ne.a[500]),
                backgroundColor: '#0039a6',
                '&:hover': { backgroundColor: ne.a[700] },
              },
            }
          })(Z.a),
          Object(ae.a)(function (e) {
            return {
              root: {
                color: e.palette.getContrastText(ne.a[500]),
                backgroundColor: '#151142',
                '&:hover': { backgroundColor: '#00308F' },
              },
            }
          })(Z.a),
          {
            formData: [
              {
                name: 'username',
                type: 'text',
                control: 'input',
                label: 'Username',
              },
              {
                name: 'password',
                type: 'password',
                control: 'input',
                label: 'Password',
              },
            ],
            initialInput: { username: '', password: '' },
            columns: 'form_grid_col_1',
          }),
        Ne = function () {
          var e = Object(r.b)(),
            t = Ie(),
            a = Object(r.c)(function (e) {
              return e.login
            }),
            n = a.error,
            i = a.loading
          return Object(d.jsxs)('div', {
            className: 'login',
            children: [
              Object(d.jsxs)('div', {
                className: 'login__form',
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('h1', { children: 'Vedsys' }),
                  }),
                  n && Object(d.jsx)(te.a, { severity: 'error', children: n }),
                  Object(d.jsx)('center', {
                    children: Object(d.jsx)('h2', { children: 'Login' }),
                  }),
                  Object(d.jsx)(Pe, {
                    inputData: ke,
                    onSubmit: function (t) {
                      var a
                      e(
                        ((a = t),
                        (function () {
                          var e = Object(E.a)(
                            D.a.mark(function e(t) {
                              var n, i, c
                              return D.a.wrap(
                                function (e) {
                                  for (;;)
                                    switch ((e.prev = e.next)) {
                                      case 0:
                                        return (
                                          console.log(a),
                                          t(G({ login: 'data', loggedIn: !0 })),
                                          (e.prev = 2),
                                          t(M(!0)),
                                          (n = {
                                            headers: {
                                              'Content-Type':
                                                'application/json',
                                            },
                                          }),
                                          (e.next = 7),
                                          V.a.post(
                                            'http://'
                                              .concat(L.IP, ':')
                                              .concat(
                                                L.PORT,
                                                '/api/auth/signin'
                                              ),
                                            {
                                              username: a.username,
                                              password: a.password,
                                            },
                                            n
                                          )
                                        )
                                      case 7:
                                        ;(i = e.sent),
                                          (c = i.data).success
                                            ? t(
                                                G({
                                                  login: c,
                                                  loggedIn: c.success,
                                                  token: c.accessToken,
                                                })
                                              )
                                            : t(
                                                z({
                                                  error:
                                                    'something went wrong please try again',
                                                })
                                              ),
                                          t(M(!1)),
                                          (e.next = 16)
                                        break
                                      case 13:
                                        ;(e.prev = 13),
                                          (e.t0 = e.catch(2)),
                                          t(M(!1))
                                      case 16:
                                      case 'end':
                                        return e.stop()
                                    }
                                },
                                e,
                                null,
                                [[2, 13]]
                              )
                            })
                          )
                          return function (t) {
                            return e.apply(this, arguments)
                          }
                        })())
                      )
                    },
                  }),
                  Object(d.jsxs)('div', {
                    style: { display: 'flex' },
                    children: [
                      Object(d.jsx)('div', {
                        style: {
                          flex: 0.5,
                          paddingRight: '1rem',
                          paddingTop: '1rem',
                        },
                      }),
                      Object(d.jsx)('div', {
                        style: {
                          flex: 0.5,
                          paddingLeft: '0.3rem',
                          paddingTop: '1rem',
                        },
                      }),
                    ],
                  }),
                ],
              }),
              Object(d.jsx)($.a, {
                className: t.backdrop,
                open: i,
                children: Object(d.jsx)(ee.a, { color: 'inherit' }),
              }),
            ],
          })
        },
        De =
          (a(498),
          a(181),
          a(755),
          a(130),
          Object(R.b)({
            name: 'cases',
            initialState: {
              loading: !1,
              caseData: [],
              caseById: null,
              error: null,
              caseDataById: null,
              modal: !1,
            },
            reducers: {
              setCase: function (e, t) {
                e.caseData = t.payload
              },
              setCaseById: function (e, t) {
                e.caseById = t.payload
              },
              setError: function (e, t) {
                e.error = t.payload
              },
              setLoading: function (e, t) {
                e.loading = t.payload
              },
              setCaseDataById: function (e, t) {
                e.caseDataById = t.payload
              },
              setModal: function (e, t) {
                e.modal = t.payload
              },
            },
          })),
        Ee = De.actions,
        Re =
          (Ee.setModal,
          Ee.setCase,
          Ee.setError,
          Ee.setLoading,
          Ee.setCaseDataById,
          Ee.setCaseById,
          De.reducer),
        Fe = a(55),
        Ve = a(661),
        Le = a.n(Ve),
        qe = a(345),
        Be = a(333),
        Ue = a.n(Be),
        Me = a(419),
        Ge = a.n(Me),
        ze = (a(1046), a(0))
      function He(e) {
        e.navigation
        var t = Object(n.useState)([]),
          a = Object(W.a)(t, 2),
          i = a[0],
          c = a[1],
          l = Object(n.useState)([]),
          o = Object(W.a)(l, 2),
          s =
            (o[0],
            o[1],
            Object(n.useState)([
              'Name',
              { name: 'Actions', options: { filter: !1, sort: !1 } },
            ])),
          r = Object(W.a)(s, 2),
          p = r[0],
          u = r[1],
          b = Object(n.useState)({
            filterType: 'textField',
            responsive: 'scroll',
          }),
          h = Object(W.a)(b, 2),
          O = h[0],
          m = h[1]
        Object(n.useEffect)(function () {
          m(e.options),
            e.data &&
              (u(
                [].concat(Object(Fe.a)(e.cols), [
                  { name: 'Actions', options: { filter: !1, sort: !1 } },
                ])
              ),
              (function (t) {
                var a = t.map(function (e) {
                    return console.log('objx', e), Object.values(e)
                  }),
                  n = []
                console.log('tblData', a),
                  a &&
                    ((n = a.map(function (t) {
                      return [].concat(Object(Fe.a)(t), [
                        Object(d.jsxs)(qe.a, {
                          container: !0,
                          direction: 'row',
                          justify: 'space-around',
                          alignItems: 'center',
                          style: { color: '#000', height: '100%' },
                          spacing: 1,
                          children: [
                            Object(d.jsx)(qe.a, {
                              item: !0,
                              children: Object(d.jsx)(Z.a, {
                                color: 'primary',
                                size: 'large',
                                onClick: function () {
                                  console.log('d0', t, e.editRoute),
                                    e.navigation.history.push(e.editRoute)
                                },
                                children: Object(d.jsx)(ze.b.Provider, {
                                  children: Object(d.jsx)('div', {
                                    style: {
                                      padding: '1rem 1rem',
                                      backgroundColor: '#4caf50',
                                      borderRadius: 3,
                                      color: 'white',
                                    },
                                    children: Object(d.jsx)(Ge.a, {}),
                                  }),
                                }),
                              }),
                            }),
                            Object(d.jsx)(qe.a, {
                              item: !0,
                              children: Object(d.jsx)(Z.a, {
                                color: 'secondary',
                                size: 'large',
                                onClick: function () {
                                  return console.log(t[0])
                                },
                                children: Object(d.jsx)(ze.b.Provider, {
                                  children: Object(d.jsx)('div', {
                                    style: {
                                      padding: '1rem 1rem',
                                      backgroundColor: '#f44335',
                                      borderRadius: 3,
                                      color: 'white',
                                    },
                                    children: Object(d.jsx)(Ue.a, {}),
                                  }),
                                }),
                              }),
                            }),
                          ],
                        }),
                      ])
                    })),
                    c(n))
              })(e.data))
        }, []),
          Object(n.useEffect)(
            function () {
              m(e.options)
            },
            [e.options]
          ),
          Object(n.useEffect)(
            function () {
              console.log('props. viewApi', e.viewApi, e.apiCol),
                !e.data &&
                  e.viewApi &&
                  V()({
                    url: e.viewApi,
                    method: e.apiType,
                    headers: Object(ce.a)(
                      Object(ce.a)(
                        { 'Content-Type': 'application/json' },
                        e.headers
                      ),
                      e.auth
                    ),
                    params: Object(ce.a)({}, e.params),
                    data: {},
                  }).then(
                    function (e) {
                      console.log('/request', e.data), f(e.data.results)
                    },
                    function (e) {
                      console.log(Object(ce.a)({}, e))
                    }
                  )
            },
            [e.viewApi, e.data]
          )
        var f = function (t) {
            var a = e.cols
            console.log('viewApiDEFS', t, a, typeof a[0])
            var n = []
            if (
              ('string' == typeof a[0]
                ? (console.log('yourapicols', a), u(a))
                : 'object' == typeof a &&
                  (Object.keys(a).map(function (e) {
                    if (
                      (console.log('viewApi acol', e, typeof a[e]),
                      a[e] || (n = [].concat(Object(Fe.a)(n), [e])),
                      'string' == typeof a[e] &&
                        (n = [].concat(Object(Fe.a)(n), [a[e]])),
                      'function' == typeof a[e])
                    ) {
                      var i = a[e](t).colName
                      ;(n = [].concat(Object(Fe.a)(n), [i])),
                        console.log('viewApiX', a[e], n)
                    }
                  }),
                  (n = [].concat(Object(Fe.a)(n), [
                    { name: 'Actions', options: { filter: !1, sort: !1 } },
                  ]))),
              console.log('tempCol', n),
              u(n),
              'object' === typeof t)
            ) {
              var i = t || e.data
              if (
                (console.log('defxobject', typeof e.cols[0], e.cols[0], e.cols),
                e.cols[0],
                !e.cols[0] && 'object' == typeof e.cols)
              ) {
                var l = function (t) {
                    return Object(d.jsxs)(qe.a, {
                      container: !0,
                      direction: 'row',
                      justify: 'space-around',
                      alignItems: 'center',
                      style: { color: '#000', height: '100%', minWidth: 200 },
                      spacing: 1,
                      children: [
                        Object(d.jsx)(qe.a, {
                          item: !0,
                          children: Object(d.jsx)(Z.a, {
                            color: 'primary',
                            size: 'large',
                            onClick: function () {
                              console.log('EDIT ', t, e.editRoute),
                                e.navigation.history.push(e.editRoute + '/' + t)
                            },
                            children: Object(d.jsx)(ze.b.Provider, {
                              children: Object(d.jsx)('div', {
                                style: {
                                  padding: '1rem 1rem',
                                  backgroundColor: '#4caf50',
                                  borderRadius: 3,
                                  color: 'white',
                                },
                                children: Object(d.jsx)(Ge.a, {}),
                              }),
                            }),
                          }),
                        }),
                        Object(d.jsx)(qe.a, {
                          item: !0,
                          children: Object(d.jsx)(Z.a, {
                            color: 'secondary',
                            size: 'large',
                            onClick: function () {
                              console.log('DELETE ', t),
                                e.deleteRoute &&
                                  e.navigation.history.push(e.deleteRoute),
                                x(t)
                            },
                            children: Object(d.jsx)(ze.b.Provider, {
                              children: Object(d.jsx)('div', {
                                style: {
                                  padding: '1rem 1rem',
                                  backgroundColor: '#f44335',
                                  borderRadius: 3,
                                  color: 'white',
                                },
                                children: Object(d.jsx)(Ue.a, {}),
                              }),
                            }),
                          }),
                        }),
                      ],
                    })
                  },
                  o = {},
                  s = []
                Object.keys(i).map(function (t) {
                  console.log('loop dataCols', t, i[0]),
                    Object.keys(e.cols).map(function (a) {
                      var n,
                        c,
                        s = null,
                        r = a,
                        d = i[t]._id
                      ;((s = i[t][a]), 'string' == typeof e.cols[a] && s) &&
                        ((r = e.cols[a]),
                        (o = Object(ce.a)(
                          Object(ce.a)({}, o),
                          {},
                          ((n = {}),
                          Object(j.a)(n, r, s),
                          Object(j.a)(n, 'Actions', l(d)),
                          n)
                        )))
                      !e.cols[a] &&
                        s &&
                        (o = Object(ce.a)(
                          Object(ce.a)({}, o),
                          {},
                          ((c = {}),
                          Object(j.a)(c, r, s),
                          Object(j.a)(c, 'Actions', l(d)),
                          c)
                        ))
                      if ('function' == typeof e.cols[a] && s) {
                        var p,
                          u = e.cols[a]
                        console.log('tmpDt 1', s, i[t], a, e.cols[a]),
                          (s = u(i[t]).return),
                          (r = u(i[t]).colName),
                          console.log('tmpDt 2', s, a, e.cols[a]),
                          (o = Object(ce.a)(
                            Object(ce.a)({}, o),
                            {},
                            ((p = {}),
                            Object(j.a)(p, r, s),
                            Object(j.a)(p, 'Actions', l(d)),
                            p)
                          ))
                      }
                    }),
                    (s = [].concat(Object(Fe.a)(s), [o]))
                }),
                  c(s),
                  console.log('loop tempData', s)
              }
            }
          },
          x = function (t) {
            t &&
              e.deleteApi &&
              V()({
                url: e.deleteApi + '/' + t,
                method: e.deleteApiType,
                headers: Object(ce.a)(
                  Object(ce.a)(
                    { 'Content-Type': 'application/json' },
                    e.headers
                  ),
                  e.auth
                ),
                params: Object(ce.a)({}, e.params),
                data: {},
              }).then(
                function (e) {
                  console.log('/request', e.data),
                    f(e.data.results),
                    window.location.reload()
                },
                function (e) {
                  console.log(Object(ce.a)({}, e))
                }
              )
          }
        return (
          console.log('loop DATA<==>COLS', i, p),
          Object(d.jsx)('div', {
            style: { maxWidth: '100%' },
            children: Object(d.jsx)(Le.a, {
              title: 'List',
              data: i,
              columns: p,
              options: O,
            }),
          })
        )
      }
      a(137)
      function _e(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/medicine',
                cols: {
                  title: null,
                  description: null,
                  exp_date: null,
                  mfd_date: null,
                  price: null,
                },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicine'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicine'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicine'),
              }),
            ],
          }),
        })
      }
      var We = a(245),
        Qe = (a(591), a(146)),
        Ye = a.n(Qe),
        Ke = a(173),
        Xe = a(212),
        Je = a.n(Xe),
        Ze = a(1285),
        $e = a(1290),
        et = a(1291),
        tt = a(1292),
        at = (a(1149), a(1288)),
        nt = a(1289),
        it = a(596),
        ct = a(668),
        lt = a(1293)
      a(102)
      function ot(e) {
        console.log('updateDataxFileUpload', e.response)
        var t = Object(n.useRef)(null),
          a = Object(n.useState)({}),
          i = Object(W.a)(a, 2),
          c = i[0],
          l = i[1],
          o = Object(n.useState)({}),
          s = Object(W.a)(o, 2),
          r = s[0],
          p = s[1],
          u = Object(n.useState)(!1),
          b = Object(W.a)(u, 2),
          h = b[0],
          O = b[1],
          m = Object(n.useState)(!1),
          f = Object(W.a)(m, 2),
          x = f[0],
          g = f[1],
          v = function () {
            return g(!1)
          },
          y = e.params,
          T = void 0 === y ? {} : y,
          w = e.token,
          S = void 0 === w ? null : w,
          C = e.api,
          P =
            void 0 === C
              ? {
                  method: 'POST',
                  url: 'http://localhost:8080/upload',
                  headers: {},
                  getUrl: 'http://localhost:8081/uploads',
                }
              : C,
          A = e.model,
          I = void 0 === A ? null : A,
          k = e.model_id,
          N = void 0 === k ? null : k,
          D = e.storage_path,
          E = void 0 === D ? null : D,
          R = e.model_key,
          F = void 0 === R ? null : R,
          L = e.data,
          q = void 0 === L ? {} : L
        Object(n.useEffect)(
          function () {
            if (
              (console.log('props.isSubmitting uE'),
              e.isSubmitting && 'undefined' !== typeof c && c.length > 0)
            ) {
              console.log('props.isSubmitting IF')
              var t = new FormData()
              Object.keys(c).map(function (a) {
                t.append(e.name, c[a])
              }),
                I &&
                  N &&
                  (t.append('model', I),
                  t.append('model_id', N),
                  F && t.append('model_key', F)),
                E && t.append('storage_path', E),
                (q = t),
                V()({
                  method: P.method,
                  url: P.url,
                  headers: Object(ce.a)(
                    {
                      Authorization: 'Bearer ' + S,
                      Accept: '*/*',
                      'Content-Type': 'multipart/form-data',
                    },
                    P.headers
                  ),
                  params: T,
                  data: q,
                }).then(
                  function (t) {
                    console.log('FileUploadProps /request axios', t.data),
                      e.onChange(
                        null,
                        Object(ce.a)(
                          Object(ce.a)({}, c),
                          {},
                          { response: t.data }
                        )
                      )
                  },
                  function (e) {
                    console.log('axios', Object(ce.a)({}, e))
                  }
                )
            }
          },
          [e.isSubmitting, c]
        )
        var B = function () {
            t.current.click()
          },
          U = function () {
            var e = c
            p(e), Object.keys(e).length > 0 && B()
          },
          M = function () {
            l({}), O(!1)
          },
          G = function (e) {
            var t =
              arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0
            return Object(d.jsx)(d.Fragment, {
              children: Object(d.jsx)('div', {
                class: 'col-xs-4 col-sm-4 col-md-4 col-lg-4',
                style: { position: 'relative', height: 250 },
                children: Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsxs)('div', {
                    style: {
                      flex: 1,
                      flexDirection: 'row',
                      flexGrow: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: '100%',
                      maxWidth: '100%',
                      color: '#000',
                      height: '100%',
                    },
                    children: [
                      Object(d.jsx)('span', {
                        onClick: function () {
                          H(t)
                        },
                        style: {
                          position: 'relative',
                          marginBottom: -10,
                          marginRight: -5,
                          zIndex: 1,
                          float: 'right',
                          backgroundColor: '#F00',
                          borderRadius: '50%',
                          paddingLeft: 1,
                          paddingTop: 1,
                          paddingBottom: 0,
                          width: 22,
                          height: 22,
                        },
                        children: Object(d.jsx)(at.a, {
                          size: 'large',
                          style: {
                            width: 20,
                            height: 20,
                            fontWeight: 'bold',
                            color: '#fff',
                          },
                        }),
                      }),
                      e.type.search(/image/i) > -1 ? z(e, 'img') : null,
                      e.type.search(/pdf/i) > -1 ? z(e, 'pdf') : null,
                      e.type.search(/doc/i) > -1 ? z(e, 'doc') : null,
                      e.type.search(/xls/i) > -1 ? z(e, 'xls') : null,
                      e.type.search(/(rar|tar|gz|bz|archive|zip)/i) > -1
                        ? z(e, 'archive')
                        : null,
                      e.type.search(/sql/i) > -1 ? z(e, 'sql') : null,
                      e.type.search(/json/i) > -1 ? z(e, 'json') : null,
                      e.type.search(/text/i) > -1 &&
                      e.type.search(/plain/i) > -1
                        ? z(e, 'text')
                        : null,
                      e.type.search(
                        /text|txt|pdf|doc|xls|rtf|image|jpg|jpeg|png|mp4|video|webm|json/i
                      ) < 0
                        ? z(e, 'others')
                        : null,
                      e.type.search(/video/i) > -1 ? z(e, 'video') : null,
                      Object(d.jsxs)('div', {
                        className: 'caption',
                        children: [
                          Object(d.jsx)('b', {
                            style: {
                              flex: 1,
                              flexWrap: 'wrap',
                              flexShrink: 1,
                              overflowWrap: 'break-word',
                            },
                            children: e.name,
                          }),
                          Object(d.jsx)('hr', {
                            style: { padding: 0, margin: 0 },
                          }),
                          Object(d.jsxs)('p', {
                            children: [
                              Math.ceil(e.size / 1024),
                              ' KB |',
                              ' ',
                              Object(d.jsx)('span', {
                                style: {
                                  flex: 1,
                                  flexWrap: 'wrap',
                                  flexShrink: 1,
                                  overflowWrap: 'break-word',
                                },
                                children: e.type,
                              }),
                            ],
                          }),
                          Object(d.jsx)('p', {}),
                        ],
                      }),
                    ],
                  }),
                }),
              }),
            })
          },
          z = function (e, t) {
            d.Fragment
            switch (t) {
              case 'img':
                return Object(d.jsx)('img', {
                  'data-src': '#',
                  alt: '',
                  src: URL.createObjectURL(e),
                  style: {
                    position: 'relative',
                    width: '100%',
                    maxWidth: 200,
                    height: '100%',
                    maxHeight: 150,
                    zIndex: 0,
                  },
                })
              case 'others':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.f, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'text':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.g, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'json':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.e, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'sql':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.a, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'pdf':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(it.a, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#F00',
                    },
                  }),
                })
              case 'doc':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.c, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'xls':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.b, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'archive':
                return Object(d.jsx)(d.Fragment, {
                  children: Object(d.jsx)(nt.d, {
                    size: 'large',
                    style: {
                      width: 80,
                      height: 80,
                      fontWeight: 'bold',
                      color: '#fff',
                    },
                  }),
                })
              case 'video':
                return Object(d.jsx)(ct.a, {
                  title: 'Animated Cartoon',
                  message: '10M views',
                  preview: URL.createObjectURL(e),
                  width: '100%',
                  height: '100%',
                  maxHeight: 200,
                  muted: !0,
                  badge: '',
                  badgeBg: 'none',
                })
              default:
                return Object(d.jsxs)(d.Fragment, {
                  children: [
                    Object(d.jsx)('span', {
                      className: '',
                      style: { color: '#000', width: '100%' },
                      children: e.name,
                    }),
                    Object(d.jsxs)('ul', {
                      style: {
                        listStyle: 'none',
                        marginLeft: 10,
                        textAlign: 'left',
                      },
                      children: [
                        Object(d.jsx)('li', {
                          style: { padding: 5, paddingBottom: 0 },
                          children: 'Name:',
                        }),
                        Object(d.jsxs)('li', {
                          style: { padding: 5, paddingBottom: 0 },
                          children: ['Size: ', Math.ceil(e.size / 1024)],
                        }),
                        Object(d.jsxs)('li', {
                          style: { padding: 5, paddingBottom: 0 },
                          children: ['Type: ', e.type],
                        }),
                      ],
                    }),
                  ],
                })
            }
          },
          H = function (e) {
            var t = c,
              a = []
            Object.keys(t).map(function (n) {
              n !== e && a.push(t[n])
            }),
              l(a)
          }
        return Object(d.jsxs)(d.Fragment, {
          children: [
            Object(d.jsx)('input', {
              type: 'file',
              className: 'form-control',
              name: 'upload_file',
              onChange: function (t) {
                var a = t.target.files
                'object' === typeof r && Object.keys(r).length > 0
                  ? ((a = {}),
                    Object.keys(t.target.files).map(function (e) {
                      a = Object.assign(
                        {},
                        a,
                        Object(j.a)(
                          {},
                          Date.now() + Math.floor(999999 * Math.random()),
                          t.target.files[e]
                        )
                      )
                    }),
                    Object.keys(r).map(function (e) {
                      a = Object.assign(
                        {},
                        a,
                        Object(j.a)(
                          {},
                          Date.now() + Math.floor(999999 * Math.random()),
                          r[e]
                        )
                      )
                    }),
                    l(a),
                    O(!0))
                  : (l(a), O(!0)),
                  e.onChange(null, a)
              },
              style: { display: 'none' },
              ref: t,
              multiple: !0,
            }),
            e.response && e.preview
              ? Object.keys(e.response).map(function (t) {
                  var a = e.response[t]
                  return Object(d.jsxs)(d.Fragment, {
                    children: [
                      Object(d.jsx)('br', {}),
                      Object(d.jsx)('img', {
                        src: P.getUrl + '/' + a,
                        alt: '',
                        style: { width: 100, height: 100 },
                      }),
                    ],
                  })
                })
              : null,
            Object(d.jsx)('br', {}),
            Object.keys(c).length <= 0
              ? Object(d.jsx)('button', {
                  type: 'button',
                  className: 'btn btn-primary',
                  onClick: B,
                  style: Object(ce.a)({}, e.style),
                  children: 'Upload A File',
                })
              : null,
            c && Object.keys(c).length > 0
              ? Object(d.jsxs)('div', {
                  style: Object(ce.a)({}, e.style),
                  children: [
                    Object(d.jsx)('button', {
                      type: 'button',
                      className: 'btn btn-warning',
                      onClick: U,
                      style: { marginRight: 7 },
                      children: 'Add More',
                    }),
                    Object(d.jsx)('button', {
                      type: 'button',
                      className: 'btn btn-danger',
                      onClick: M,
                      style: { marginRight: 7 },
                      children: 'Remove Files',
                    }),
                    Object(d.jsx)('a', {
                      className: 'btn btn-primary',
                      'data-toggle': 'modal',
                      href: '#fileUploadModal',
                      onClick: function () {
                        return g(!0)
                      },
                      style: { marginRight: 7 },
                      children: 'Show Files',
                    }),
                  ],
                })
              : null,
            Object(d.jsx)('div', {
              children: Object(d.jsxs)(lt.a, {
                show: x,
                style: {
                  position: 'absolute',
                  marginTop: '30%',
                  height: '100%',
                },
                onHide: v,
                children: [
                  Object(d.jsx)(lt.a.Header, {
                    children: Object(d.jsxs)(lt.a.Title, {
                      style: { paddingTop: '30%' },
                      children: [
                        'Your Files',
                        ' ',
                        Object(d.jsx)('span', {
                          className: 'pull-right',
                          onClick: v,
                          style: {
                            fontSize: 20,
                            marginTop: -10,
                            cursor: 'pointer',
                            backgroundColor: '#F00',
                            borderRadius: '50%',
                            paddingLeft: 7,
                            paddingRight: 7,
                            paddingTop: 5,
                          },
                          children: Object(d.jsx)(at.a, {
                            size: 'large',
                            style: {
                              width: 20,
                              height: 20,
                              fontWeight: 'bold',
                              color: '#fff',
                            },
                          }),
                        }),
                      ],
                    }),
                  }),
                  Object(d.jsx)(lt.a.Body, {
                    children: Object(d.jsx)('div', {
                      class: 'well well-lg',
                      style: { height: 400, overflow: 'scroll' },
                      children:
                        h && c && c.size > 0
                          ? G(c)
                          : h && 'object' === typeof c
                          ? Object(d.jsx)('div', {
                              class: 'row',
                              children: Object.keys(c).map(function (e) {
                                var t = c[e]
                                return G(t, e)
                              }),
                            })
                          : null,
                    }),
                  }),
                  Object(d.jsxs)(lt.a.Footer, {
                    children: [
                      Object.keys(c).length <= 0
                        ? Object(d.jsx)('button', {
                            type: 'button',
                            className: 'btn btn-primary',
                            onClick: B,
                            style: { marginRight: 7 },
                            children: 'Upload A File',
                          })
                        : null,
                      c && Object.keys(c).length > 0
                        ? Object(d.jsxs)(d.Fragment, {
                            children: [
                              Object(d.jsx)('button', {
                                type: 'button',
                                className: 'btn btn-danger',
                                onClick: M,
                                children: 'Remove Files',
                              }),
                              Object(d.jsx)('button', {
                                type: 'button',
                                className: 'btn btn-warning',
                                onClick: U,
                                style: { marginRight: 7 },
                                children: 'Add More',
                              }),
                            ],
                          })
                        : null,
                      Object(d.jsx)('span', {
                        className: 'btn btn-primary',
                        variant: 'secondary',
                        onClick: v,
                        children: 'Close',
                      }),
                    ],
                  }),
                ],
              }),
            }),
          ],
        })
      }
      var st = a(208),
        rt = a(670),
        dt = a(38),
        pt = a(39),
        jt = a(66),
        ut = a(67),
        bt = a(671),
        ht = (function (e) {
          Object(jt.a)(a, e)
          var t = Object(ut.a)(a)
          function a() {
            var e
            Object(dt.a)(this, a)
            for (var n = arguments.length, i = new Array(n), c = 0; c < n; c++)
              i[c] = arguments[c]
            return (
              ((e = t.call.apply(t, [this].concat(i))).state = {
                api: {
                  method: 'POST',
                  url: 'http://localhost:8080/upload',
                  headers: {},
                  getUrl: 'http://localhost:8081/uploads',
                },
                options: [
                  { value: null, label: 'Select An Option', color: '#d7d7d7' },
                ],
                selectedOption: null,
                fetchData: !1,
                loading: !0,
              }),
              (e.filterOptionsFromState = function (t) {
                return e.state.options.filter(function (e) {
                  e.label.toLowerCase().includes(t.toLowerCase())
                })
              }),
              (e.promiseOptions = function (t) {
                return new Promise(function (a) {
                  e.state.fetchData ||
                    setTimeout(function () {
                      a(e.filterOptionsFromState(t))
                    }, 1e3)
                })
              }),
              e
            )
          }
          return (
            Object(pt.a)(a, [
              {
                key: 'componentDidMount',
                value: function () {
                  var e = this,
                    t = this.props.api,
                    a = this.props.options
                  if (
                    (console.log('options', a, typeof a),
                    t &&
                      'undefined' == typeof a &&
                      this.setState(function (e) {
                        return Object(ce.a)(
                          Object(ce.a)({}, e),
                          {},
                          { api: t, fetchData: !0 }
                        )
                      }),
                    'object' === typeof a &&
                      (this.setState(function (e) {
                        return Object(ce.a)(
                          Object(ce.a)({}, e),
                          {},
                          {
                            options: [
                              {
                                value: null,
                                label: 'Select An Option',
                                color: '#d7d7d7',
                              },
                            ].concat(Object(Fe.a)(a)),
                            fetchData: !1,
                          }
                        )
                      }),
                      this.props.value && null == this.state.selectedOption))
                  ) {
                    var n = this.state.options.filter(function (t) {
                      return t.value == e.props.value
                    })
                    ;(n = 'object' === typeof n ? n[0] : null),
                      this.setState(function (e) {
                        return Object(ce.a)(
                          Object(ce.a)({}, e),
                          {},
                          { selectedOption: n }
                        )
                      })
                  }
                },
              },
              {
                key: 'componentDidUpdate',
                value: function (e, t) {
                  var a = this
                  if (
                    'undefined' !== typeof this.props.value &&
                    this.props.value &&
                    null == this.state.selectedOption &&
                    t.options !== this.state.options
                  ) {
                    var n = this.state.options.filter(function (e) {
                      return e.value == a.props.value
                    })
                    'object' === typeof n && (n = n[0]),
                      this.setState(function (e) {
                        return Object(ce.a)(
                          Object(ce.a)({}, e),
                          {},
                          { selectedOption: n }
                        )
                      })
                  }
                  t.fetchData !== this.state.fetchData &&
                    this.state.fetchData &&
                    this.state.fetchData &&
                    V()({
                      method: this.state.api.method,
                      url: this.state.api.url,
                      headers: Object(ce.a)(
                        {
                          Authorization: 'Bearer ' + this.props.token,
                          Accept: '*/*',
                          'Content-Type': 'multipart/form-data',
                        },
                        this.state.api.headers
                      ),
                      params: Object(ce.a)({}, this.props.params),
                      data: Object(ce.a)({}, this.props.reqData),
                    }).then(
                      function (e) {
                        var t = a.props.pick
                        if (
                          e.data.results &&
                          'object' === typeof e.data.results &&
                          e.data.results.length > 0
                        ) {
                          var n = []
                          e.data.results.map(function (e) {
                            var a = Object.values(t)[0]
                            if ('object' === typeof a) {
                              var i = ''
                              a.map(function (t) {
                                e[t] ? (i += e[t]) : (i += t)
                              }),
                                (a = i)
                            } else a = 'function' === typeof a ? a(e) : e[a]
                            n.push({ value: e[Object.keys(t)[0]], label: a })
                          }),
                            a.setState(function (e) {
                              return Object(ce.a)(
                                Object(ce.a)({}, e),
                                {},
                                {
                                  options: [
                                    {
                                      value: null,
                                      label: 'Select An Option',
                                      color: '#d7d7d7',
                                    },
                                  ].concat(n),
                                }
                              )
                            })
                        }
                      },
                      function (e) {}
                    )
                },
              },
              {
                key: 'render',
                value: function () {
                  var e = this,
                    t = this.props
                  return Object(d.jsx)(bt.a, {
                    className: 'basic-single',
                    classNamePrefix: 'select',
                    defaultValue: this.state.selectedOption,
                    value: this.state.selectedOption,
                    isDisabled: this.props.isDisabled,
                    isLoading: this.state.isLoading,
                    isClearable: this.props.isClearable,
                    isRtl: this.props.isRtl,
                    isSearchable: !0,
                    name: 'color',
                    options: this.state.options,
                    onChange: function (a) {
                      t.onChange(t.name, a.value),
                        e.setState(function (e) {
                          return Object(ce.a)(
                            Object(ce.a)({}, e),
                            {},
                            { selectedOption: a }
                          )
                        })
                    },
                  })
                },
              },
            ]),
            a
          )
        })(n.Component),
        Ot = a(421),
        mt = a.n(Ot),
        ft =
          (a(1248),
          function (e) {
            console.log('MCQxProps', e)
            var t = Object(n.useState)({}),
              a = Object(W.a)(t, 2),
              i = a[0],
              c = a[1]
            Object(n.useEffect)(
              function () {
                e.value && c(e.value)
              },
              [e.value]
            )
            return Object(d.jsxs)('div', {
              children: [
                Object(d.jsx)('span', {
                  class: 'label label-danger',
                  style: { marginBottom: 1, cursor: 'pointer' },
                  onClick: function () {
                    !(function () {
                      console.log('ADDMCQ', Object.keys(i).length, i)
                      var t = e.name + (+Object.keys(i).length + 1),
                        a = Object(ce.a)(
                          Object(ce.a)({}, i),
                          {},
                          Object(j.a)({}, t, null)
                        )
                      c(a)
                    })()
                  },
                  children: Object(d.jsx)('b', { children: ' + Add Another ' }),
                }),
                Object.keys(i).map(function (t, a) {
                  console.log('MCQx loop', t)
                  var n = i[t]
                  return Object(d.jsx)(d.Fragment, {
                    children: Object(d.jsxs)('div', {
                      class: 'row',
                      style: {
                        display: 'flex',
                        flex: 1,
                        height: '100%',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                      },
                      children: [
                        Object(d.jsx)('div', {
                          class: 'col-xs-1 col-sm-1 col-md-1 col-lg-1',
                          style: { marginTop: 10 },
                          children: Object(d.jsxs)('b', {
                            children: ['#\xa0', a + 1],
                          }),
                        }),
                        Object(d.jsx)('div', {
                          class: 'col-xs-10 col-sm-10 col-md-10 col-lg-10',
                          children: Object(d.jsx)('input', {
                            type: 'text',
                            name: t,
                            className: 'form-control',
                            value: n,
                            onChange: function (a) {
                              !(function (t, a) {
                                console.log('MCQxCDU', t, a, i, e)
                                var n = Object(ce.a)(
                                  Object(ce.a)({}, i),
                                  {},
                                  Object(j.a)({}, t, a)
                                )
                                c(n), e.onChange(e.name, i)
                              })(t, a.target.value)
                            },
                          }),
                        }),
                        Object(d.jsx)('div', {
                          class: 'col-xs-1 col-sm-1 col-md-1 col-lg-1',
                          children: Object(d.jsx)('button', {
                            type: 'button',
                            className: 'btn btn-danger btn-sm btn-small',
                            style: { marginTop: 5 },
                            onClick: function () {
                              !(function (t, a) {
                                var n = i
                                delete n[t]
                                var l = {}
                                Object.values(n).map(function (t, a) {
                                  var n = e.name + (+a + 1)
                                  l = Object(ce.a)(
                                    Object(ce.a)({}, l),
                                    {},
                                    Object(j.a)({}, n, t)
                                  )
                                }),
                                  c(l),
                                  e.onChange(e.name, l)
                              })(t)
                            },
                            children: Object(d.jsx)(We.a, {
                              style: { marginTop: 3, marginBottom: 0 },
                            }),
                          }),
                        }),
                      ],
                    }),
                  })
                }),
              ],
            })
          }),
        xt = Object(Ke.a)({
          overrides: {
            MuiPickersToolbar: { toolbar: { backgroundColor: Je.a.A200 } },
            MuiPickersCalendarHeader: { switchHeader: {} },
            MuiPickersDay: {
              day: { color: '#000' },
              daySelected: { backgroundColor: Je.a[400] },
              dayDisabled: { color: Je.a[100] },
              current: { color: Je.a[900] },
            },
            MuiPickersModal: { dialogAction: { color: Je.a[400] } },
          },
        })
      function gt(e) {
        var t = Object(n.useState)(e.value),
          a = Object(W.a)(t, 2),
          i = a[0],
          c = a[1],
          l = Object(n.useState)(Object(d.jsx)('h1', { children: 'Hi' })),
          o = Object(W.a)(l, 2),
          s = o[0],
          r = o[1],
          p = Object(n.useState)(
            e.value ? Ye()(e.value, e.inputFormat).format('Y-M-D') : null
          ),
          u = Object(W.a)(p, 2),
          b = u[0],
          h = u[1],
          O = Object(n.useState)(e.value ? Ye()(e.value, e.inputFormat) : null),
          m = Object(W.a)(O, 2),
          f = m[0],
          x = m[1],
          g = Object(n.useState)(e.value ? Ye()(e.value, e.inputFormat) : null),
          v = Object(W.a)(g, 2),
          y = (v[0], v[1]),
          T = Object(n.useState)(),
          w = Object(W.a)(T, 2),
          S = w[0],
          C = w[1],
          P = Object(n.useState)({}),
          A = Object(W.a)(P, 2),
          I = A[0],
          k = A[1],
          N = Object(n.useState)(!1),
          D = Object(W.a)(N, 2),
          E = D[0],
          R = D[1],
          F = Object(n.useState)(st.EditorState.createEmpty()),
          V = Object(W.a)(F, 2),
          L = V[0],
          q = V[1],
          B = Object(n.useState)(),
          U = Object(W.a)(B, 2),
          M = U[0],
          G = U[1]
        return (
          Object(n.useEffect)(
            function () {
              'text' == e.type && c(e.value), 'radio' == e.type && C(e.value)
            },
            [e.value]
          ),
          Object(n.useEffect)(
            function () {
              switch (e.type) {
                case 'text':
                  r(
                    Object(d.jsx)(
                      'input',
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          type: 'text',
                          name: e.name,
                          label: e.label,
                          value: i,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || '',
                          onChange: function (t) {
                            c(t.target.value),
                              e.handleChange(e.name, t.target.value)
                          },
                          style: Object(ce.a)(
                            { marginTop: 10, marginBottom: 10 },
                            e.style
                          ),
                        }
                      )
                    )
                  )
                  break
                case 'mcq':
                  r(
                    Object(d.jsx)(
                      ft,
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          type: 'text',
                          name: e.name,
                          label: e.label,
                          value: i,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || '',
                          onChange: function (t, a) {
                            e.handleChange(e.name, a),
                              console.log('SUBMITDEFLATER InputJS', a, e)
                          },
                          style: Object(ce.a)(
                            { marginTop: 10, marginBottom: 10 },
                            e.style
                          ),
                        }
                      )
                    )
                  )
                  break
                case 'editor':
                  r(
                    Object(d.jsxs)('div', {
                      className: 'rdw-storybook-root',
                      children: [
                        Object(d.jsx)(rt.Editor, {
                          editorState: L,
                          wrapperClassName: 'demo-wrapper',
                          editorClassName: 'demo-editor',
                          onChange: function (e) {},
                          onEditorStateChange: function (t) {
                            q(t),
                              G(
                                mt()(
                                  Object(st.convertToRaw)(L.getCurrentContent())
                                )
                              ),
                              e.handleChange(
                                e.name,
                                mt()(
                                  Object(st.convertToRaw)(L.getCurrentContent())
                                )
                              )
                          },
                          onContentStateChange: function (e) {},
                          name: e.name,
                          label: e.label,
                          value: e.value,
                          required: e.required || !1,
                          title: e.title || '',
                          wrapperStyle: Object(ce.a)(
                            {
                              marginTop: 10,
                              marginBottom: 10,
                              minHeight: 300,
                              height: '100%',
                              width: '100%',
                              border: 1,
                              borderWidth: 1,
                              borderColor: '#d7d7d7',
                              borderStyle: 'solid',
                            },
                            e.style
                          ),
                          editorStyle: { padding: '0 5px', width: '100%' },
                        }),
                        e.preview
                          ? Object(d.jsx)('textarea', {
                              disabled: !0,
                              style: { width: '100%', flex: 1 },
                              value: M,
                            })
                          : null,
                      ],
                    })
                  )
                  break
                case 'textarea':
                  r(
                    Object(d.jsx)(
                      'textarea',
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          rows: 5,
                          type: 'text',
                          name: e.name,
                          label: e.label,
                          value: e.value,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || '',
                          onChange: function (t) {
                            e.handleChange(e.name, t.target.value)
                          },
                          style: Object(ce.a)(
                            { marginTop: 10, marginBottom: 10 },
                            e.style
                          ),
                        }
                      )
                    )
                  )
                  break
                case 'paste':
                case 'gluejar':
                case 'screenshot':
                case 'snipping':
                case 'snippet':
                  r(
                    Object(d.jsx)('input', {
                      type: 'text',
                      name: e.name,
                      label: e.label,
                      value: e.value,
                      className: e.className || 'form-control',
                      required: e.required || !1,
                      title: e.title || '',
                      placeholder: e.placeholder || '',
                      onPaste: function (e) {
                        console.log('e.clipboardData', e.clipboardData)
                      },
                      style: Object(ce.a)(
                        { marginTop: 10, marginBottom: 10 },
                        e.style
                      ),
                    })
                  )
                  break
                case 'file':
                  r(
                    Object(d.jsx)(
                      ot,
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          type: e.type,
                          name: e.name,
                          label: e.label,
                          value: e.value,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || '',
                          onChange: function (t, a) {
                            e.handleChange(e.name, a)
                          },
                          isSubmitting: e.isSubmitting,
                          style: Object(ce.a)({ marginTop: -25 }, e.style),
                        }
                      )
                    )
                  )
                  break
                case 'select':
                  r(
                    Object(d.jsxs)(
                      'select',
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          type: e.type,
                          name: e.name,
                          label: e.label,
                          value: e.value,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || 'Select An Option',
                          onChange: function (t) {
                            t.target.value &&
                              e.handleChange(e.name, t.target.value)
                          },
                          style: Object(ce.a)(
                            { marginTop: 10, marginBottom: 10 },
                            e.style
                          ),
                          children: [
                            Object(d.jsx)('option', {
                              value: '',
                              children: e.placeholder || 'Select An Option',
                            }),
                            e.options &&
                              e.options.map(function (e) {
                                return Object(d.jsx)('option', {
                                  value: Object.keys(e),
                                  children: Object.values(e),
                                })
                              }),
                          ],
                        }
                      )
                    )
                  )
                  break
                case 'select2':
                case 'multiselect':
                  r(
                    Object(d.jsx)(
                      ht,
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          type: e.type,
                          name: e.name,
                          label: e.label,
                          value: e.value,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || 'MultiSelect An Option',
                          onChange: function (t, a) {
                            e.handleChange(e.name, a)
                          },
                          style: Object(ce.a)(
                            { marginTop: 10, marginBottom: 10 },
                            e.style
                          ),
                        }
                      )
                    )
                  )
                  break
                case 'date':
                  r(
                    Object(d.jsx)(Ze.a, {
                      theme: xt,
                      children: Object(d.jsx)(
                        $e.a,
                        Object(ce.a)(
                          Object(ce.a)({}, e.inputProps),
                          {},
                          {
                            autoOk: !0,
                            variant: 'inline',
                            format: e.outputFormat ? e.outputFormat : 'D-M-Y',
                            name: e.name,
                            value: b,
                            required: e.required || !1,
                            title: e.title || '',
                            placeholder: e.placeholder || '',
                            className: '',
                            onChange: function (t) {
                              h(t),
                                e.handleChange(
                                  e.name,
                                  Ye()(t).format(e.inputFormat)
                                )
                            },
                            style: Object(ce.a)(
                              { padding: '6px 0', width: '100%' },
                              e.style
                            ),
                          }
                        )
                      ),
                    })
                  )
                  break
                case 'time':
                  r(
                    Object(d.jsx)(Ze.a, {
                      theme: xt,
                      children: Object(d.jsx)(
                        et.a,
                        Object(ce.a)(
                          Object(ce.a)({}, e.inputProps),
                          {},
                          {
                            autoOk: !0,
                            variant: 'inline',
                            format: e.outputFormat ? e.outputFormat : 'hh:mm',
                            name: e.name,
                            value: f,
                            required: e.required || !1,
                            title: e.title || '',
                            placeholder: e.placeholder || '',
                            className: '',
                            onChange: function (t) {
                              x(t),
                                e.handleChange(
                                  e.name,
                                  Ye()(t).format(e.inputFormat)
                                )
                            },
                            style: Object(ce.a)(
                              { paddingTop: 0, width: '100%' },
                              e.style
                            ),
                          }
                        )
                      ),
                    })
                  )
                  break
                case 'datetime':
                  r(
                    Object(d.jsx)(Ze.a, {
                      theme: xt,
                      children: Object(d.jsx)(
                        tt.a,
                        Object(ce.a)(
                          Object(ce.a)({}, e.inputProps),
                          {},
                          {
                            autoOk: !0,
                            variant: 'inline',
                            format: e.outputFormat ? e.outputFormat : 'hh:mm',
                            name: e.name,
                            value: f,
                            required: e.required || !1,
                            title: e.title || '',
                            placeholder: e.placeholder || '',
                            className: '',
                            onChange: function (t) {
                              y(t),
                                e.handleChange(
                                  e.name,
                                  Ye()(t).format(e.inputFormat)
                                )
                            },
                            style: Object(ce.a)(
                              { paddingTop: 0, width: '100%' },
                              e.style
                            ),
                          }
                        )
                      ),
                    })
                  )
                  break
                case 'radio':
                  r(
                    Object(d.jsx)('div', {
                      children:
                        e.options &&
                        e.options.map(function (t) {
                          return Object(d.jsx)('div', {
                            className: 'input-group',
                            children: Object(d.jsxs)('label', {
                              className: '',
                              children: [
                                Object(d.jsx)(
                                  'input',
                                  Object(ce.a)(
                                    Object(ce.a)({}, e),
                                    {},
                                    {
                                      type: 'radio',
                                      checked: Object.keys(t) == S,
                                      name: e.name,
                                      value: Object.keys(t),
                                      className: e.className || '',
                                      required: e.required || !1,
                                      title: e.title || '',
                                      placeholder: e.placeholder || '',
                                      onClick: function (a) {
                                        C(Object.keys(t)[0]),
                                          e.handleChange(
                                            e.name,
                                            Object.keys(t)[0]
                                          )
                                      },
                                      style: Object(ce.a)(
                                        { width: 14, margin: 0, marginTop: -5 },
                                        e.style
                                      ),
                                    }
                                  )
                                ),
                                Object(d.jsx)('span', {
                                  style: { marginLeft: 5 },
                                  children: Object.values(t),
                                }),
                              ],
                            }),
                          })
                        }),
                    })
                  )
                  break
                case 'checkbox':
                  r(
                    Object(d.jsx)('div', {
                      class: 'input-group',
                      children: Object(d.jsxs)('label', {
                        class: '',
                        children: [
                          Object(d.jsx)(
                            'input',
                            Object(ce.a)(
                              Object(ce.a)({}, e),
                              {},
                              {
                                type: 'checkbox',
                                defaultChecked: E,
                                name: e.name,
                                value: e.value,
                                className: e.className || '',
                                required: e.required || !1,
                                title: e.title || '',
                                placeholder: e.placeholder || '',
                                onChange: function (t) {
                                  R(t.target.checked),
                                    e.handleChange(
                                      e.name,
                                      !!t.target.checked && t.target.value,
                                      'checkbox',
                                      I ? 'set' : 'unset'
                                    )
                                },
                                style: Object(ce.a)(
                                  { width: 14, margin: 0, marginTop: -5 },
                                  e.style
                                ),
                              }
                            )
                          ),
                          Object(d.jsx)('span', {
                            style: { marginLeft: 5 },
                            children: e.label,
                          }),
                        ],
                      }),
                    })
                  )
                  break
                case 'checkboxes':
                  r(
                    Object(d.jsx)('div', {
                      children:
                        e.options &&
                        e.options.map(function (t) {
                          var a = Object.keys(t),
                            n = Object.values(t)
                          return Object(d.jsx)('div', {
                            class: 'input-group',
                            children: Object(d.jsxs)('label', {
                              class: '',
                              children: [
                                Object(d.jsx)(
                                  'input',
                                  Object(ce.a)(
                                    Object(ce.a)({}, e),
                                    {},
                                    {
                                      type: 'checkbox',
                                      checked: I[e.name],
                                      name: e.name,
                                      value: I[a],
                                      className: e.className || '',
                                      required: e.required || !1,
                                      title: e.title || '',
                                      placeholder: e.placeholder || '',
                                      onChange: function (t) {
                                        var n = !!t.target.checked
                                        k(
                                          Object(ce.a)(
                                            Object(ce.a)({}, I),
                                            {},
                                            Object(j.a)({}, a, n)
                                          )
                                        ),
                                          e.handleChange(
                                            e.name,
                                            I,
                                            'checkboxes',
                                            I ? 'set' : 'unset'
                                          )
                                      },
                                      style: Object(ce.a)(
                                        { width: 14, margin: 0, marginTop: -5 },
                                        e.style
                                      ),
                                    }
                                  )
                                ),
                                Object(d.jsx)('span', {
                                  style: { marginLeft: 5 },
                                  children: n,
                                }),
                              ],
                            }),
                          })
                        }),
                    })
                  )
                  break
                default:
                  r(
                    Object(d.jsx)(
                      'input',
                      Object(ce.a)(
                        Object(ce.a)({}, e),
                        {},
                        {
                          type: 'text',
                          name: e.name,
                          label: e.label,
                          value: e.value,
                          className: e.className || 'form-control',
                          required: e.required || !1,
                          title: e.title || '',
                          placeholder: e.placeholder || '',
                          onChange: function (t) {
                            e.handleChange(e.name, t.target.value)
                          },
                          style: Object(ce.a)(
                            { marginTop: 10, marginBottom: 10 },
                            e.style
                          ),
                        }
                      )
                    )
                  )
              }
            },
            [e.isSubmitting, e.value, i, b, f, I, L, e.response, S]
          ),
          Object(n.useEffect)(
            function () {
              e.handleChange(e.name, I, 'checkboxes', I ? 'set' : 'unset')
            },
            [I]
          ),
          Object(d.jsx)(d.Fragment, {
            children: Object(d.jsxs)('div', {
              className: e.mainDivClass,
              style: Object(ce.a)({ marginTop: 5 }, e.mainDivStyle),
              children: [
                Object(d.jsx)('div', {
                  className: e.inputDivClass,
                  style: Object(ce.a)(
                    { marginBottom: 0, marginTop: 0 },
                    e.inputDivStyle
                  ),
                  children: s,
                }),
                Object(d.jsx)('div', {
                  className: e.subtitleDivClass,
                  style: Object(ce.a)(
                    { marginBottom: e.subTitle ? 10 : 0 },
                    e.subTitleStyle
                  ),
                  children: Object(d.jsx)('span', {
                    style: {
                      fontWeight: 'normal',
                      fontStyle: 'italic',
                      fontSize: '0.8em',
                    },
                    children: e.subTitle || '',
                  }),
                }),
              ],
            }),
          })
        )
      }
      function vt(e) {
        var t = Object(n.useState)(!1),
          a = Object(W.a)(t, 2),
          i = a[0],
          c = a[1],
          l = Object(n.useState)(!1),
          o = Object(W.a)(l, 2),
          s = (o[0], o[1]),
          r = Object(n.useState)(!1),
          p = Object(W.a)(r, 2),
          j = p[0],
          u = p[1]
        Object(n.useEffect)(
          function () {
            c(e.isSubmitting)
          },
          [e.isSubmitting]
        ),
          Object(n.useEffect)(
            function () {
              if ((s(e.savedValues), e.progress)) u(e.progress)
              else {
                Object.keys(e.savedValues).length
                var t = Object.values(e.savedValues).filter(function (e) {
                  return 'object' === typeof e
                    ? null !== e &&
                        'undefined' !== typeof e &&
                        'undefined' !== e &&
                        Object.keys(e).length
                    : null !== e &&
                        'undefined' !== typeof e &&
                        'undefined' !== e
                }).length
                u((t = (t / 10) * 100) > 100 ? 100 : t)
              }
            },
            [e.progress, e.savedValues]
          )
        var b = function () {
          c(!1), e.onSubmitHandler()
        }
        return Object(d.jsxs)('div', {
          className: 'row',
          children: [
            Object(d.jsx)('div', {
              className: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
              children: Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  marginBottom: 20,
                  width: '100%',
                  flex: 1,
                  flexGrow: 1,
                },
                children: [
                  Object(d.jsx)('div', {
                    style: { marginTop: -40 },
                    children: Object(d.jsx)(ze.b.Provider, {
                      value: { className: 'extended-card-icon' },
                      children: Object(d.jsx)('div', {
                        style: {
                          padding: '1.5rem 2rem',
                          backgroundColor: '#de2668',
                          borderRadius: 3,
                        },
                        children: Object(d.jsx)(K.b, {}),
                      }),
                    }),
                  }),
                  Object(d.jsxs)('div', {
                    style: {
                      display: 'flex',
                      flexGrow: 1,
                      flexDirection: 'row',
                      justifyContent: 'flex-start',
                      marginTop: -25,
                      alignItems: 'center',
                      width: '100%',
                      maxWidth: '100%',
                      color: '#000',
                    },
                    children: [
                      Object(d.jsx)('div', {
                        style: {
                          paddingLeft: 10,
                          fontWeight: 'normal',
                          width: '40%',
                          maxWidth: '40%',
                          fontSize: '1.3em',
                        },
                        children: e.title || 'Form Generator',
                      }),
                      Object(d.jsx)('div', {
                        style: {
                          width: '100%',
                          maxWidth: '40%',
                          marginTop: 20,
                        },
                        children: e.editId
                          ? Object(d.jsxs)('div', {
                              style: { height: 20, width: 100, marginTop: -15 },
                              children: [
                                Object(d.jsxs)('span', {
                                  className: 'label label-info',
                                  style: { marginRight: 10 },
                                  children: ['DB ID:', ' '],
                                }),
                                e.editId,
                              ],
                            })
                          : Object(d.jsx)('div', {
                              className: 'progress',
                              style: { height: 20, width: 100 },
                              children: Object(d.jsxs)('div', {
                                className:
                                  'progress-bar progress-bar-striped progress-bar-animated',
                                role: 'progress-bar',
                                'aria-valuenow': j,
                                'aria-valuemin': '0',
                                'aria-valuemax': '100',
                                style: { width: j, height: 20 },
                                children: [j, '%'],
                              }),
                            }),
                      }),
                      e.editId
                        ? Object(d.jsx)('div', {
                            style: { width: '100%', maxWidth: '10%' },
                            children: Object(d.jsx)('button', {
                              type: 'button',
                              className: 'btn  btn-danger',
                              style: {
                                paddingBottom: 2,
                                marginBottom: 0,
                                marginTop: 10,
                              },
                              onClick: function () {
                                c(!1), e.onDeleteHandler()
                              },
                              children: Object(d.jsx)(We.a, {
                                style: {
                                  fontSize: '1.3em',
                                  paddingBottom: 0,
                                  marginBottom: 0,
                                },
                              }),
                            }),
                          })
                        : null,
                    ],
                  }),
                ],
              }),
            }),
            'undefined' === typeof e.fields
              ? null
              : e.fields.map(function (t, a) {
                  var n = e.fields.length,
                    c = Math.ceil(12 / n)
                  'object' === typeof e.cols && (c = e.cols[a])
                  var l = function () {
                    return (
                      t &&
                      t.map(function (t) {
                        var a = function (t, a) {
                          var n =
                              arguments.length > 2 && void 0 !== arguments[2]
                                ? arguments[2]
                                : 'normal',
                            i =
                              arguments.length > 3 && void 0 !== arguments[3]
                                ? arguments[3]
                                : 'set'
                          e.onChangeHandler && e.onChangeHandler(t, a, n, i)
                        }
                        return Object(d.jsx)(d.Fragment, {
                          children:
                            'custom' === t.type
                              ? Object(d.jsx)(d.Fragment, {
                                  children: t.component,
                                })
                              : Object(d.jsxs)(d.Fragment, {
                                  children: [
                                    Object(d.jsx)('div', {
                                      class: '',
                                      style: Object(ce.a)(
                                        {
                                          color: '#000',
                                          marginTop: 10,
                                          paddingTop: 0,
                                        },
                                        t.labelStyle
                                      ),
                                      children: t.label,
                                    }),
                                    Object(d.jsx)(
                                      gt,
                                      Object(ce.a)(
                                        {
                                          type: t.type,
                                          name: t.name,
                                          label: t.label,
                                          value: t.value,
                                          handleChange: function (e, t) {
                                            var n =
                                                arguments.length > 2 &&
                                                void 0 !== arguments[2]
                                                  ? arguments[2]
                                                  : 'normal',
                                              i =
                                                arguments.length > 3 &&
                                                void 0 !== arguments[3]
                                                  ? arguments[3]
                                                  : 'set'
                                            a(e, t, n, i)
                                          },
                                          handleSubmission: function (e) {
                                            b(e)
                                          },
                                          isSubmitting: i,
                                        },
                                        t.props
                                      )
                                    ),
                                  ],
                                }),
                        })
                      })
                    )
                  }
                  return 'undefined' !== typeof e.cols
                    ? Object(d.jsx)('div', {
                        class: 'col-xs-'
                          .concat(c, ' col-sm-')
                          .concat(c, ' col-md-')
                          .concat(c, ' col-lg-')
                          .concat(c),
                        children: l(),
                      })
                    : Object(d.jsx)('div', {
                        class: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
                        children: l(),
                      })
                }),
            Object(d.jsxs)('div', {
              className: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
              children: [
                Object(d.jsx)('div', {
                  className: 'col-xs-4 col-sm-4 col-md-4 col-lg-4',
                }),
                Object(d.jsx)('div', {
                  className: 'col-xs-4 col-sm-4 col-md-4 col-lg-4',
                  children: i
                    ? Object(d.jsx)('div', {
                        children: Object(d.jsx)('div', {
                          className: 'progress',
                          style: { height: 30, width: 150, marginTop: 20 },
                          children: Object(d.jsx)('div', {
                            className:
                              'progress-bar progress-bar-striped progress-bar-animated',
                            role: 'progressbar',
                            'aria-valuenow': e.progress,
                            'aria-valuemin': '0',
                            'aria-valuemax': '100',
                            style: {
                              width: ''.concat(e.progress || 50, '%'),
                              height: 30,
                            },
                          }),
                        }),
                      })
                    : Object(d.jsx)('div', {
                        style: {
                          flex: 1,
                          flexDirection: 'column',
                          flexGrow: 1,
                          color: '#000',
                          width: '100%',
                          height: '100%',
                        },
                        children: Object(d.jsx)('div', {
                          children: Object(d.jsx)('button', {
                            className: 'btn',
                            style: {
                              backgroundColor: '#e91e63',
                              color: 'white',
                              padding: '5px 50px',
                              marginTop: 20,
                            },
                            type: 'button',
                            onClick: b,
                            children: Object(d.jsx)('b', {
                              children: 'Submit',
                            }),
                          }),
                        }),
                      }),
                }),
              ],
            }),
          ],
        })
      }
      var yt = a(46),
        Tt = a(672),
        wt = (function (e) {
          Object(jt.a)(a, e)
          var t = Object(ut.a)(a)
          function a(e) {
            var n
            Object(dt.a)(this, a),
              ((n = t.call(this, e)).state = {
                apiValues: {
                  date: '17-12-1991',
                  dates: null,
                  timedob: null,
                  gender: null,
                  group: null,
                  fname: null,
                  genderXXXX: 'male',
                  ageroup: null,
                  picx: null,
                  tnc: 'yes',
                },
                savedValues: {},
                initialValues: {},
                isSubmitting: !1,
                fields: [],
                progress: 0,
                sentFiles: 0,
                serverResponse: {},
                editId: null,
              }),
              (n.inflate = function (e, t) {
                var a = n.props.initialState
                if (
                  (console.log('MCQx', t, a),
                  'object' != typeof t && 'function' != typeof t)
                )
                  return t
                if ('object' == typeof t && null != t) {
                  if ('undefined' != typeof t.inf && 'function' != typeof t.inf)
                    return t.inf
                  if (
                    'undefined' != typeof t.inf &&
                    'function' == typeof t.inf
                  ) {
                    var i = t.inf
                    return console.log('MCQx', t, a, i), i()
                  }
                  return t
                }
                return 'object' == typeof t && null == t ? null : void 0
              }),
              (n.deflate = function (e, t) {
                var a = n.props.initialState
                if (
                  (console.log(
                    'SUBMITDEFLATER deflatexxxx',
                    e,
                    t,
                    typeof a[e],
                    a[e]
                  ),
                  'object' != typeof t && 'function' != typeof t)
                )
                  return t
                if ('object' == typeof t && null != t && a[e]) {
                  if (
                    (console.log(
                      'initx OBJECT deflater',
                      e,
                      t,
                      typeof t,
                      typeof a[e],
                      a[e]
                    ),
                    'undefined' != typeof a[e].def &&
                      'function' == typeof a[e].def &&
                      'string' != typeof t[0] &&
                      'undefined' != typeof t[0])
                  ) {
                    var i = a[e].def
                    return console.log('deflater#1', i), i(t)
                  }
                  if (
                    'undefined' != typeof a[e].def &&
                    'string' == typeof a[e].def
                  ) {
                    var c = a[e].def
                    return console.log('deflater#2', c), c
                  }
                  return t
                }
                return 'object' == typeof t && null == t
                  ? (console.log('deflater#3', t), null)
                  : 'object' == typeof t && 'string' == typeof t[0]
                  ? (console.log('deflater#zZAZZ', t, t[0]), t[0])
                  : void 0
              }),
              (n.handleChanges = function (e, t) {
                var a = n.state.savedValues
                console.log(
                  'SUBMITDEFLATER Formake OCH',
                  e,
                  t,
                  n.deflate(e, t)
                ),
                  n.setState(function (n) {
                    return Object(ce.a)(
                      Object(ce.a)({}, n),
                      {},
                      {
                        savedValues: Object(ce.a)(
                          Object(ce.a)({}, a),
                          {},
                          Object(j.a)({}, e, t)
                        ),
                      }
                    )
                  })
                var i = null,
                  c = 0
                n.props.fields.map(function (a) {
                  a.map(function (a) {
                    'function' == typeof a.onChangeHandler &&
                      (i = a.onChangeHandler),
                      'file' == a.type &&
                        a.name == e &&
                        t.filePaths &&
                        t.filePaths > 0 &&
                        (c++,
                        n.setState(function (e) {
                          return Object(ce.a)(
                            Object(ce.a)({}, e),
                            {},
                            { sentFiles: c }
                          )
                        }))
                  })
                }),
                  'function' == typeof i && i(e, t),
                  console.log('SUBMITDEFLATER Formake OCH22', e, t),
                  n.props.onChangeHandler(e, t)
              }),
              (n.handleSubmit = function () {
                var e = n.state.savedValues
                if (
                  (n.props.onSubmit(e),
                  ('undefined' != typeof n.props.createApi ||
                    'undefined' != typeof n.props.editApi) &&
                    n.props.useAxios)
                ) {
                  var t = {}
                  n.props.fields.map(function (a) {
                    a.map(function (a) {
                      'file' == a.type
                        ? 'undefined' !== typeof e[a.name] && e[a.name]
                        : (t = Object(ce.a)(
                            Object(ce.a)({}, t),
                            {},
                            Object(j.a)({}, a.name, e[a.name])
                          ))
                    })
                  }),
                    V()({
                      url: n.state.edit
                        ? n.props.updateApi + '/' + n.state.editId
                        : n.props.createApi,
                      method: n.state.edit
                        ? n.props.updateType
                        : n.props.createType,
                      headers: Object(ce.a)(
                        Object(ce.a)(
                          { 'Content-Type': 'application/json' },
                          n.props.headers
                        ),
                        n.props.auth
                      ),
                      params: Object(ce.a)({}, n.props.params),
                      data: t,
                    }).then(
                      function (t) {
                        if (t.data.results && 1 == t.data.results.n) {
                          console.log('isSubmittingX', t.data.results.n),
                            n.setState(function (e) {
                              return Object(ce.a)(
                                Object(ce.a)({}, e),
                                {},
                                { isSubmitting: !0 }
                              )
                            })
                          setTimeout(function () {
                            n.setState(function (e) {
                              return Object(ce.a)(
                                Object(ce.a)({}, e),
                                {},
                                { progress: 50, sentFiles: 0 }
                              )
                            })
                          }, 200),
                            setInterval(function () {
                              var e = +n.state.progress + 20
                              n.setState(function (t) {
                                return Object(ce.a)(
                                  Object(ce.a)({}, t),
                                  {},
                                  { progress: e }
                                )
                              })
                            }, 200)
                        }
                        if (t.data.results && t.data.results._id) {
                          var a = {}
                          Object.keys(t.data.results).map(function (e) {
                            console.log(
                              'deflater results EDIT',
                              n.deflate(e, t.data.results[e])
                            ),
                              (a = Object(ce.a)(
                                Object(ce.a)({}, a),
                                {},
                                Object(j.a)(
                                  {},
                                  e,
                                  n.deflate(e, t.data.results[e])
                                )
                              ))
                          }),
                            n.setState(function (e) {
                              return Object(ce.a)(
                                Object(ce.a)({}, e),
                                {},
                                {
                                  editId: t.data.results._id,
                                  edit: !0,
                                  savedValues: a,
                                }
                              )
                            }),
                            n.props.fields.map(function (a) {
                              a.map(function (a) {
                                'file' == a.type &&
                                  'undefined' !== typeof e[a.name] &&
                                  'object' === typeof e[a.name] &&
                                  (a.props = Object(ce.a)(
                                    Object(ce.a)({}, a.props),
                                    {},
                                    { model_id: t.data.results._id }
                                  ))
                              })
                            }),
                            n.setState(function (e) {
                              return Object(ce.a)(
                                Object(ce.a)({}, e),
                                {},
                                { isSubmitting: !0 }
                              )
                            })
                          var i = 0
                          n.props.fields.map(function (e) {
                            e.map(function (e) {
                              e &&
                                'file' == e.type &&
                                (i++, (e.model_id = t.data.results._id))
                            })
                          })
                          var c = 100 / (i + 5)
                          setTimeout(function () {
                            n.setState(function (e) {
                              return Object(ce.a)(
                                Object(ce.a)({}, e),
                                {},
                                { progress: 50, sentFiles: i }
                              )
                            })
                          }, 200),
                            setInterval(function () {
                              var e = +n.state.progress + +c
                              n.setState(function (t) {
                                return Object(ce.a)(
                                  Object(ce.a)({}, t),
                                  {},
                                  { progress: e }
                                )
                              })
                            }, 200)
                        }
                      },
                      function (e) {
                        console.log('/request ', e)
                      }
                    )
                }
              }),
              (n.handleDelete = function () {
                'undefined' != typeof n.props.deleteApi &&
                  n.props.useAxios &&
                  window.confirm('Delete the item?') &&
                  V()({
                    url: n.props.deleteApi + '/' + n.state.editId,
                    method: n.props.deleteType,
                    headers: Object(ce.a)(
                      Object(ce.a)(
                        { 'Content-Type': 'application/json' },
                        n.props.headers
                      ),
                      n.props.auth
                    ),
                    params: Object(ce.a)({}, n.props.params),
                    data: {},
                  }).then(
                    function (e) {
                      window.location.reload(),
                        e.data.results &&
                          e.data.results._id &&
                          n.setState(function (e) {
                            return Object(ce.a)(
                              Object(ce.a)({}, e),
                              {},
                              { edit: !1, editId: null, savedValues: {} }
                            )
                          })
                    },
                    function (e) {
                      console.log('/request ', e)
                    }
                  )
              })
            e.fields
            return n
          }
          return (
            Object(pt.a)(a, [
              {
                key: 'componentDidMount',
                value: function () {
                  var e = this
                  if (
                    (console.log('Formake CDM', {
                      state: this.state,
                      props: this.props,
                    }),
                    this.props.isSubmitting &&
                      this.setState(function (t) {
                        return Object(ce.a)(
                          Object(ce.a)({}, t),
                          {},
                          { isSubmitting: e.props.isSubmitting }
                        )
                      }),
                    'undefined' != typeof this.props.fields &&
                      this.props.fields)
                  ) {
                    var t = this.state.savedValues,
                      a = this.props.fields,
                      n = this.props.initialState
                    if (a && a.length > 0) {
                      this.state.initialValues
                      var i = Object(ce.a)({}, n),
                        c = this.state.savedVals
                      'undefined' === typeof c && (c = {}),
                        0 == this.props.edit &&
                          a.map(function (t, c) {
                            t.map(function (t, l) {
                              if (t.name && n[t.name]) {
                                var o = e.inflate(t.name, n[t.name])
                                ;(i = Object(ce.a)(
                                  Object(ce.a)({}, i),
                                  {},
                                  Object(j.a)({}, t.name, o)
                                )),
                                  (a[c][l].value = o)
                              }
                            })
                          }),
                        'undefined' === typeof this.props.edit &&
                          a.map(function (t, c) {
                            t.map(function (t, l) {
                              if (t.name && n[t.name]) {
                                var o = e.inflate(t.name, n[t.name])
                                ;(i = Object(ce.a)(
                                  Object(ce.a)({}, i),
                                  {},
                                  Object(j.a)({}, t.name, o)
                                )),
                                  (a[c][l].value = o)
                              }
                            })
                          }),
                        1 == this.props.edit &&
                          this.props.useAxios &&
                          this.getEditData(),
                        1 == this.state.edit &&
                          this.props.useAxios &&
                          this.getEditData(),
                        this.setState(function (n) {
                          return Object(ce.a)(
                            Object(ce.a)({}, n),
                            {},
                            {
                              fields: a,
                              edit: e.props.edit,
                              editId: e.props.editId,
                              initialValues: i,
                              savedValues: Object(ce.a)(Object(ce.a)({}, t), i),
                            }
                          )
                        })
                    }
                  }
                },
              },
              {
                key: 'componentDidUpdate',
                value: function (e, t) {
                  var a = this
                  console.log('Formake MCQx', {
                    state: this.state,
                    props: this.props,
                  }),
                    t.progress !== this.state.progress &&
                      this.state.progress >= 100 &&
                      setTimeout(function () {
                        a.setState(function (e) {
                          return Object(ce.a)(
                            Object(ce.a)({}, e),
                            {},
                            { progress: 100, isSubmitting: !1 }
                          )
                        })
                      }, 1e3),
                    t.savedValues !== this.state.savedValues &&
                      this.props.updatedValues(this.state.savedValues),
                    t.edit === this.props.edit ||
                      this.props.useAxios ||
                      this.getEditData(),
                    t.savedValues !== this.state.initialValues &&
                      t.initialValues !== this.state.initialValues &&
                      0 == this.props.edit &&
                      this.setState(function (e) {
                        return Object(ce.a)(
                          Object(ce.a)({}, e),
                          {},
                          { savedValues: a.props.initialValues }
                        )
                      }),
                    e.isSubmitting !== this.props.isSubmitting &&
                      t.isSubmitting !== this.state.isSubmitting &&
                      e.isSubmitting !== this.props.isSubmitting &&
                      this.setState(function (e) {
                        return Object(ce.a)(
                          Object(ce.a)({}, e),
                          {},
                          { isSubmitting: a.props.isSubmitting }
                        )
                      })
                },
              },
              {
                key: 'getEditData',
                value: function () {
                  var e = this,
                    t = {}
                  V()({
                    url: this.props.editApi + '/' + this.props.editId,
                    method: this.props.editType,
                    headers: Object(ce.a)(
                      Object(ce.a)(
                        { 'Content-Type': 'application/json' },
                        this.props.headers
                      ),
                      this.props.auth
                    ),
                    params: Object(ce.a)({}, this.props.params),
                  }).then(
                    function (a) {
                      if (
                        (null == a.data.results &&
                          e.setState(function (e) {
                            return Object(ce.a)(
                              Object(ce.a)({}, e),
                              {},
                              {
                                editId: null,
                                edit: !1,
                                isSubmitting: !1,
                                savedValues: {},
                              }
                            )
                          }),
                        a.data.results && a.data.results._id)
                      ) {
                        var n = {}
                        Object.keys(a.data.results).map(function (t) {
                          console.log(
                            'deflater results EDIT',
                            e.deflate(t, a.data.results[t])
                          ),
                            (n = Object(ce.a)(
                              Object(ce.a)({}, n),
                              {},
                              Object(j.a)(
                                {},
                                t,
                                e.deflate(t, a.data.results[t])
                              )
                            ))
                        }),
                          e.setState(function (e) {
                            return Object(ce.a)(
                              Object(ce.a)({}, e),
                              {},
                              {
                                editId: a.data.results._id,
                                edit: !0,
                                savedValues: n,
                              }
                            )
                          })
                        var i = e.props.fields
                        i.map(function (n, c) {
                          n.map(function (n, l) {
                            n.name &&
                              a.data.results[n.name] &&
                              'file' != n.type &&
                              ((t = Object(ce.a)(
                                Object(ce.a)({}, t),
                                {},
                                Object(j.a)(
                                  {},
                                  n.name,
                                  e.deflate(n.name, a.data.results[n.name])
                                )
                              )),
                              (i[c][l].value = e.deflate(
                                n.name,
                                a.data.results[n.name]
                              ))),
                              n.name &&
                                a.data.results[n.name] &&
                                'file' == n.type &&
                                ((t = Object(ce.a)(
                                  Object(ce.a)({}, t),
                                  {},
                                  Object(j.a)(
                                    {},
                                    n.name,
                                    Object(ce.a)(
                                      Object(ce.a)({}, t[n.name]),
                                      {},
                                      {
                                        response: {
                                          updateData: Object(j.a)(
                                            {},
                                            n.name,
                                            e.deflate(
                                              n.name,
                                              a.data.results[n.name]
                                            )
                                          ),
                                        },
                                      }
                                    )
                                  )
                                )),
                                (i[c][l].props = Object(ce.a)(
                                  Object(ce.a)({}, i[c][l].props),
                                  {},
                                  {
                                    response: Object(j.a)(
                                      {},
                                      n.name,
                                      e.deflate(n.name, a.data.results[n.name])
                                    ),
                                  }
                                )))
                          })
                        }),
                          console.log('myfields', i),
                          e.setState(function (e) {
                            return Object(ce.a)(
                              Object(ce.a)({}, e),
                              {},
                              { fields: i }
                            )
                          })
                      }
                    },
                    function (e) {
                      console.log('/request ', e)
                    }
                  )
                },
              },
              {
                key: 'render',
                value: function () {
                  var e = this
                  return Object(d.jsx)(yt.a, {
                    utils: Tt.a,
                    children: Object(d.jsx)(vt, {
                      cols: this.props.cols,
                      spacing: this.props.spacing,
                      onChangeHandler: function (t, a) {
                        var n =
                            arguments.length > 2 && void 0 !== arguments[2]
                              ? arguments[2]
                              : 'normal',
                          i =
                            arguments.length > 3 && void 0 !== arguments[3]
                              ? arguments[3]
                              : 'set'
                        e.handleChanges(t, a, n, i)
                      },
                      onSubmitHandler: function () {
                        e.handleSubmit()
                      },
                      onDeleteHandler: function () {
                        e.handleDelete()
                      },
                      savedValues: this.state.savedValues,
                      edit: this.state.edit,
                      editId: this.state.editId,
                      isSubmitting: this.state.isSubmitting,
                      fields: this.state.fields,
                      progress: this.state.progress,
                    }),
                  })
                },
              },
            ]),
            a
          )
        })(n.Component)
      function St() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicine'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicine'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicine'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicine'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                        {
                          type: 'text',
                          name: 'exp_date',
                          label: 'Expiry Date',
                          props: {},
                        },
                        {
                          type: 'text',
                          name: 'price',
                          label: 'Price',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                        {
                          type: 'text',
                          name: 'mfd_date',
                          label: 'Manufacturing Date',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function Ct(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/vitallog',
                cols: { title: null, description: null },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/Vitallog'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/Vitallog'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/Vitallog'),
              }),
            ],
          }),
        })
      }
      function Pt() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/vitallog'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/vitallog'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/vitallog'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/vitallog'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function At(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/medhistory',
                cols: { title: null, description: null },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicalHistory'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicalHistory'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicalHistory'),
              }),
            ],
          }),
        })
      }
      function It() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalHistory'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalHistory'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalHistory'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalHistory'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function kt(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: 'medfacility',
                cols: { title: null, description: null },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicalfacility'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicalfacility'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/medicalfacility'),
              }),
            ],
          }),
        })
      }
      function Nt() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalfacility'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalfacility'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalfacility'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/medicalfacility'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function Dt(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/prescription',
                cols: { title: null, description: null },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/prescription'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/prescription'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/prescription'),
              }),
            ],
          }),
        })
      }
      function Et() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/prescription'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/prescription'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/prescription'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/prescription'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function Rt(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/category',
                cols: { title: null, description: null },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/category'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/category'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/category'),
              }),
            ],
          }),
        })
      }
      function Ft() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/category'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/category'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/category'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/category'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function Vt(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/order',
                cols: {
                  title: null,
                  description: null,
                  medicine: null,
                  dosage: null,
                  order_date: null,
                },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/order'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/order'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/order'),
              }),
            ],
          }),
        })
      }
      function Lt() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/order'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/order'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/order'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/order'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                        {
                          type: 'text',
                          name: 'medicine',
                          label: 'Medicine',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'dosage',
                          label: 'Dosage',
                          props: {},
                        },
                        {
                          type: 'text',
                          name: 'order_date',
                          label: 'Order Date',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function qt(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/orderitem',
                cols: {
                  title: null,
                  description: null,
                  exp_date: null,
                  mfd_date: null,
                  price: null,
                },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/orderitem'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/orderitem'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/orderitem'),
              }),
            ],
          }),
        })
      }
      function Bt() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/orderitem'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/orderitem'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/orderitem'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/orderitem'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      function Ut(e) {
        var t = e.history,
          a = e
        console.log('extprops', e)
        var n = Object(O.a)(function (e) {
          return {
            root: { display: 'flex', overflowY: 'scroll', width: '100%' },
          }
        })()
        return Object(d.jsx)('div', {
          className: n.root,
          children: Object(d.jsxs)('div', {
            style: { flex: 1 },
            children: [
              Object(d.jsxs)('div', {
                style: {
                  display: 'flex',
                  justifyContent: 'space-between',
                  padding: '2rem',
                },
                children: [
                  Object(d.jsx)('div', {
                    children: Object(d.jsx)('label', { children: 'Question' }),
                  }),
                  Object(d.jsx)('div', {
                    style: { display: 'flex' },
                    children: Object(d.jsxs)('div', {
                      children: [
                        Object(d.jsx)(Z.a, {
                          onClick: function () {
                            return t.push('/add-question')
                          },
                          variant: 'contained',
                          children: 'Add Question',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Explore',
                        }),
                        Object(d.jsx)(Z.a, {
                          variant: 'outlined',
                          style: { marginLeft: '0.5rem' },
                          children: 'Filter',
                        }),
                      ],
                    }),
                  }),
                ],
              }),
              Object(d.jsx)(He, {
                navigation: a,
                editRoute: '/delivery',
                cols: { title: null, description: null },
                viewApiType: 'GET',
                viewApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/delivery'),
                editApiType: 'PUT',
                editApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/delivery'),
                deleteApiType: 'DELETE',
                deleteApi: 'http://'
                  .concat(L.IP, ':')
                  .concat(L.PORT, '/api/delivery'),
              }),
            ],
          }),
        })
      }
      function Mt() {
        var e = Object(s.h)().qid
        return Object(d.jsx)(d.Fragment, {
          children: Object(d.jsx)('div', {
            className: 'container-fluid',
            children: Object(d.jsx)('div', {
              className: 'row',
              children: Object(d.jsx)('div', {
                className: 'col-md-12',
                children: Object(d.jsx)('div', {
                  className: 'Regular-form',
                  children: Object(d.jsx)(wt, {
                    useAxios: !0,
                    edit: !!e,
                    editId: e,
                    createApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/delivery'),
                    createType: 'POST',
                    editApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/delivery'),
                    editType: 'GET',
                    updateApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/delivery'),
                    updateType: 'PUT',
                    deleteApi: 'http://'
                      .concat(L.IP, ':')
                      .concat(L.PORT, '/api/delivery'),
                    deleteType: 'DELETE',
                    auth: { Authorization: 'Bearer TOKEN' },
                    headers: {},
                    params: {},
                    initialState: { title: null, description: null },
                    cols: [6, 6],
                    spacing: 3,
                    isSubmitting: !1,
                    updatedValues: function (e) {
                      return console.log('formValues UPDATING --\x3e ', e)
                    },
                    onChangeHandler: function (e, t) {
                      console.log('formValuesx UPDATING --\x3e ', e, t)
                    },
                    onSubmit: function (e) {
                      return console.log('formValues SUBMITTING', e)
                    },
                    fields: [
                      [
                        {
                          type: 'text',
                          name: 'title',
                          label: 'Title',
                          props: {},
                        },
                      ],
                      [
                        {
                          type: 'text',
                          name: 'description',
                          label: 'Description',
                          props: {},
                        },
                      ],
                    ],
                  }),
                }),
              }),
            }),
          }),
        })
      }
      var Gt = function () {
          var e = Object(r.c)(function (e) {
            return e.login
          })
          return (
            console.log(e.isLogged),
            Object(d.jsx)(o.a, {
              basename: window.location.pathname || '',
              children: Object(d.jsxs)(s.d, {
                children: [
                  e.isLogged
                    ? Object(d.jsx)(d.Fragment, {
                        children: Object(d.jsxs)('div', {
                          className: 'layout',
                          children: [
                            Object(d.jsx)(_, {}),
                            Object(d.jsxs)('div', {
                              className: 'body',
                              children: [
                                Object(d.jsx)(J, {}),
                                Object(d.jsxs)('div', {
                                  style: {
                                    flex: 1,
                                    overflowY: 'scroll',
                                    display: 'flex',
                                  },
                                  children: [
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/',
                                      component: v,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-medicine',
                                      component: St,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-medicine',
                                      component: _e,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/medicine/:qid',
                                      component: St,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-Vitallog',
                                      component: Pt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-Vitallog',
                                      component: Ct,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/Vitallog/:qid',
                                      component: Pt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-category',
                                      component: Ft,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-category',
                                      component: Rt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/category/:qid',
                                      component: Ft,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-prescription',
                                      component: Et,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-prescription',
                                      component: Dt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/prescription/:qid',
                                      component: Et,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-medhistory',
                                      component: It,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-medhistory',
                                      component: At,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/medhistory/:qid',
                                      component: It,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-medfacility',
                                      component: Nt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-medfacility',
                                      component: kt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/medfacility/:qid',
                                      component: Nt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-Order',
                                      component: Lt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-Order',
                                      component: Vt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/Order/:qid',
                                      component: Lt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-OrderItem',
                                      component: Bt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-OrderItem',
                                      component: qt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/OrderItem/:qid',
                                      component: Bt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/add-delivery',
                                      component: Mt,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/view-delivery',
                                      component: Ut,
                                    }),
                                    Object(d.jsx)(s.b, {
                                      exact: !0,
                                      path: '/delivery/:qid',
                                      component: Mt,
                                    }),
                                  ],
                                }),
                              ],
                            }),
                          ],
                        }),
                      })
                    : Object(d.jsx)(s.b, {
                        exact: !0,
                        path: '/',
                        component: Ne,
                      }),
                  Object(d.jsx)(s.a, { to: '/' }),
                ],
              }),
            })
          )
        },
        zt = function (e) {
          e &&
            e instanceof Function &&
            a
              .e(3)
              .then(a.bind(null, 1297))
              .then(function (t) {
                var a = t.getCLS,
                  n = t.getFID,
                  i = t.getFCP,
                  c = t.getLCP,
                  l = t.getTTFB
                a(e), n(e), i(e), c(e), l(e)
              })
        },
        Ht = a(80),
        _t = a(673),
        Wt = a.n(_t),
        Qt = Object(R.b)({
          name: 'cases',
          initialState: {
            loading: !1,
            caseData: [],
            caseById: null,
            error: null,
            caseDataById: null,
            modal: !1,
          },
          reducers: {
            setCase: function (e, t) {
              e.caseData = t.payload
            },
            setCaseById: function (e, t) {
              e.caseById = t.payload
            },
            setError: function (e, t) {
              e.error = t.payload
            },
            setLoading: function (e, t) {
              e.loading = t.payload
            },
            setCaseDataById: function (e, t) {
              e.caseDataById = t.payload
            },
            setModal: function (e, t) {
              e.modal = t.payload
            },
          },
        }),
        Yt = Qt.actions,
        Kt =
          (Yt.setModal,
          Yt.setCase,
          Yt.setError,
          Yt.setLoading,
          Yt.setCaseDataById,
          Yt.setCaseById,
          Qt.reducer),
        Xt = a(127),
        Jt = Object(Ht.b)({ login: H, cases: Re, users: Kt }),
        Zt = { key: 'root', version: 1, storage: Wt.a },
        $t = Object(Xt.g)(Zt, Jt),
        ea = Object(R.a)({
          reducer: $t,
          middleware: Object(R.c)({
            serializableCheck: {
              ignoredActions: [Xt.a, Xt.f, Xt.b, Xt.c, Xt.d, Xt.e],
            },
          }),
        }),
        ta = a(674),
        aa = Object(Xt.h)(ea)
      l.a.render(
        Object(d.jsx)(i.a.StrictMode, {
          children: Object(d.jsx)(r.a, {
            store: ea,
            children: Object(d.jsx)(ta.a, {
              loading: null,
              persistor: aa,
              children: Object(d.jsx)(ue, { children: Object(d.jsx)(Gt, {}) }),
            }),
          }),
        }),
        document.getElementById('root')
      ),
        zt()
    },
    484: function (e, t, a) {},
    694: function (e, t, a) {},
    729: function (e, t, a) {},
    731: function (e, t, a) {},
    751: function (e, t, a) {},
    752: function (e, t, a) {},
    753: function (e, t, a) {},
    755: function (e, t, a) {},
    780: function (e, t) {},
  },
  [[1251, 1, 2]],
])
//# sourceMappingURL=main.1c68009c.chunk.js.map
