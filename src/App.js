import React from 'react'
import './App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'
import { useSelector } from 'react-redux'
import Dashboard from './views/Dashboard/index'

import Navbar from './components/Navbar/Navbar'
import Sidebar from './components/Sidebar/Sidebar'
import Login from './views/Login'
import Register from './views/Register/Register'

import './App.css'

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

// import Orders from './views/Order/Orders'
// import Order from './views/Order/Order'

import Messages from './views/Message/Messages'
import Message from './views/Message/Message'

import Symptoms from './views/Symptom/Symptoms'
import Symptom from './views/Symptom/Symptom'

import Categories from './views/Category/Categories'
import Category from './views/Category/Category'

import Timings from './views/Timing/Timings'
import Timing from './views/Timing/Timing'

import Appointments from './views/Appointment/Appointments'
import Appointment from './views/Appointment/Appointment'

import Doctors from './views/Doctor/Doctors'
import Doctor from './views/Doctor/Doctor'

import Trendings from './views/Trending/Trendings'
import Trending from './views/Trending/Trending'

import Users from './views/User/Users'
import ViewDoctors from './views/User/ViewDoctors'
import User from './views/User/User'

import DoctorAppointments from './views/doctorAppointment/DoctorAppointments'
import DoctorAppointment from './views/doctorAppointment/DoctorAppointment'

import Contacts from './views/Contact/Contacts'
import Contact from './views/Contact/Contact'

import Comments from './views/Comment/Comments'
import Comment from './views/Comment/Comment'

import Families from './views/Family/Families'
import Family from './views/Family/Family'

import Fees from './views/Fee/Fees'
import Fee from './views/Fee/Fee'

import MedicalFacilities from './views/MedicalFacility/MedicalFacilities'
import MedicalFacility from './views/MedicalFacility/MedicalFacility'

import Profile from './views/Profile/Profile'

function App() {
  const user = useSelector((state) => state.login)
  console.log(user.isLogged)
  return (
    <Router basename={window.location.pathname || ''}>
      <Switch>
        {!user.isLogged ? (
          <>
            <Route exact path='/' component={Login} />
            <Route exact path='/register' component={Register} />
          </>
        ) : (
          <>
            <div className='layout'>
              <Navbar />
              <div className='body'>
                <Sidebar />
                <div
                  style={{
                    flex: 1,
                    overflowY: 'scroll',
                    display: 'flex',
                  }}
                >
                  {/* Dashboard */}
                  <Route exact path='/' component={Dashboard} />
                  <Route exact path='/profile' component={Profile} />
                  {/* Order  */}
                  {/* <Route exact path='/add-Order' component={Order} />
                  <Route exact path='/view-Order' component={Orders} />
                  <Route exact path='/Order/:qid' component={Order} /> */}
                  {/* Symptom  */}
                  <Route exact path='/add-symptom' component={Symptom} />
                  <Route exact path='/view-symptom' component={Symptoms} />
                  <Route exact path='/symptom/:qid' component={Symptom} />
                  {/* Category  */}
                  <Route exact path='/add-category' component={Category} />
                  <Route exact path='/view-category' component={Categories} />
                  <Route exact path='/category/:qid' component={Category} />
                  {/* Timing  */}
                  <Route exact path='/add-timing' component={Timing} />
                  <Route exact path='/view-timing' component={Timings} />
                  <Route exact path='/timing/:qid' component={Timing} />
                  {/* Doctor  */}
                  <Route exact path='/add-Doctor' component={Doctor} />
                  <Route exact path='/view-Doctor' component={Doctors} />
                  <Route exact path='/Doctor/:qid' component={Doctor} />

                  {/* Trending  */}
                  <Route exact path='/add-trending' component={Trending} />
                  <Route exact path='/view-trending' component={Trendings} />
                  <Route exact path='/trending/:qid' component={Trending} />

                  {/* User  */}
                  <Route exact path='/add-user' component={User} />
                  <Route exact path='/view-user' component={Users} />
                  <Route exact path='/view-udoctor' component={ViewDoctors} />
                  <Route exact path='/user/:qid' component={User} />

                  {/* Doctor Appointment  */}
                  <Route
                    exact
                    path='/add-doctorAppointment'
                    component={DoctorAppointment}
                  />
                  <Route
                    exact
                    path='/view-doctorAppointment'
                    component={DoctorAppointments}
                  />
                  <Route
                    exact
                    path='/doctorAppointment/:qid'
                    component={DoctorAppointment}
                  />

                  {/* Contact  */}
                  <Route exact path='/add-contact' component={Contact} />
                  <Route exact path='/view-contact' component={Contacts} />
                  <Route exact path='/contact/:qid' component={Contact} />

                  {/* Comment  */}
                  <Route exact path='/add-comment' component={Comment} />
                  <Route exact path='/view-comment' component={Comments} />
                  <Route exact path='/comment/:qid' component={Comment} />

                  {/* Family  */}
                  <Route exact path='/add-family' component={Family} />
                  <Route exact path='/view-family' component={Families} />
                  <Route exact path='/family/:qid' component={Family} />

                  {/* Message  */}
                  <Route exact path='/add-Message' component={Message} />
                  <Route exact path='/view-Message' component={Messages} />
                  <Route exact path='/Message/:qid' component={Message} />

                  {/* Fees  */}
                  <Route exact path='/add-fee' component={Fee} />
                  <Route exact path='/view-fee' component={Fees} />
                  <Route exact path='/fee/:qid' component={Fee} />

                  {/* MedicalFacility  */}
                  <Route
                    exact
                    path='/add-medicalFacility'
                    component={MedicalFacility}
                  />
                  <Route
                    exact
                    path='/view-medicalFacility'
                    component={MedicalFacilities}
                  />
                  <Route
                    exact
                    path='/medicalFacility/:qid'
                    component={MedicalFacility}
                  />
                  {/* Appointment  */}
                  <Route
                    exact
                    path='/add-appointment'
                    component={Appointment}
                  />
                  <Route
                    exact
                    path='/view-appointment'
                    component={Appointments}
                  />
                  <Route
                    exact
                    path='/appointment/:qid'
                    component={Appointment}
                  />
                </div>
              </div>
            </div>
          </>
        )}

        <Redirect to='/' />
      </Switch>
    </Router>
  )
}

export default App
