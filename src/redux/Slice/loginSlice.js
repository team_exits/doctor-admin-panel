import { createSlice } from '@reduxjs/toolkit'

export const loginSlice = createSlice({
  name: 'login',
  initialState: {
    login: null,
    loading: false,
    isLogged: false,
    token: null,
    user: {},
    error: null,
    selected: 0,
  },
  reducers: {
    setLogin: (state, action) => {
      console.log('REDUXLOGIN', action)
      state.login = action.payload.login
      state.isLogged = action.payload.isLogged
      state.user = action.payload.user
      state.token = action.payload.token
    },
    userUpdated: (state, action) => {
      const { data } = action.payload
      console.log('formData>>>>', data)
    },
    setLogout: (state) => {
      state.login = null
      state.isLogged = false
    },
    setLoading: (state, action) => {
      state.loading = action.payload
    },
    setError: (state, action) => {
      state.error = action.payload
    },
    setSelected: (state, action) => {
      state.selected = action.payload
    },
  },
})

//actions
export const {
  setLogin,
  setLogout,
  setLoading,
  setIsLogged,
  setError,
  setUser,
  setSelected,
  userUpdated,
} = loginSlice.actions

export default loginSlice.reducer
