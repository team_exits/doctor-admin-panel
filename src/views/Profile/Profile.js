import React, { useEffect, useState, setState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import Config from '../../Config'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import { useParams } from 'react-router-dom'
import { userUpdated } from '../../../src/redux/Slice/loginSlice'

const Profile = (props) => {
  const dispatch = useDispatch()

  let { qid } = useParams()
  console.log('qid >>> ', qid)
  // =========================
  const [selectedFile, setSelectedFile] = useState()
  const [userData, setUserData] = useState()

  const [isSelected, setIsSelected] = useState(false)

  //========== DialogBox =================
  const [open, setOpen] = React.useState(false)

  const handleClose = () => {
    setOpen(false)
  }
  //====================================

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0])
    setIsSelected(true)
  }

  const handleSubmission = (e) => {
    e.preventDefault()
    const formData = new FormData()

    formData.append('File', selectedFile)
    formData.append('Path', '/user')
    console.log('formdata>>>>>>>>>>', formData)

    axios({
      url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/upload`,
      method: 'POST',
      data: formData,
    })
      .then((result) => {
        console.log('Success:', result)
        setOpen(true)
      })
      .catch((error) => {
        console.error('Error:', error)
      })
  }

  const stateData = useSelector((state) => state.login.user.data)
  let role = stateData.roles
  console.log('dataState', stateData)
  const { name, roles, email, token, accessToken } = useSelector(
    (state) => state.login.user.data
  )
  var disabled = 'disabled'
  var enabled = !disabled
  const [showCardData2, setShowCardData2] = useState(false)
  const [buttonText2, setButtonText2] = useState('Open')
  const handleButton2 = () => {
    setShowCardData2(!showCardData2)
    if (!showCardData2 == true) {
      setButtonText2('Close')
    } else {
      setButtonText2('Edit')
    }
  }
  const [btnDisabled, setBtnDisabled] = useState(true)

  const [formData, setFormData] = useState({
    // id: stateData.id,
    name: '',
    email: '',
    phoneNo: '',
    gender: '',
    username: '',
    categories: '',
    address: '',
    website: '',
    lat: '',
    long: '',
    doctorType: '',
    education: '',
    experience: '',
    speciality: '',
    hospitalName: '',
    profilePic: '',
    mobile: '',
    roles: stateData.roles[0],
  })

  let id = stateData._id
  console.log('ID>>>>', id)

  console.log('Local FormData', formData)

  let ID = typeof stateData.id == undefined ? stateData._id : stateData.id
  const submit = (e) => {
    e.preventDefault()

    axios({
      method: 'PUT',
      url: `http://${Config.IP}:${Config.PORT}/api/admin/user/` + ID,
      data: formData,
      headers: {
        Authorization: 'Bearer ' + accessToken,
        // 'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        alert('Sucessfully Updated  ')
        dispatch(
          userUpdated({
            data: formData,
          })
        )
        console.log('response', response)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })
  }
  useEffect(() => {
    // e.preventDefault()
    axios({
      method: 'GET',
      url: `http://${Config.IP}:${Config.PORT}/api/admin/user/` + ID,
    })
      .then((response) => {
        let data = response.data.results
        console.log(' USer Data >>>>>>>>>>>', data)
        setUserData(data)
        setFormData({
          name: data.name ? data.name : '',
          email: data.email ? data.email : '',
          phoneNo: data.phoneNo ? data.phoneNo : '',
          gender: data.gender ? data.gender : '',
          username: data.username ? data.username : '',
          categories: data.categories ? data.categories : '',
          address: data.address ? data.address : '',
          website: data.website ? data.website : '',
          lat: data.lat ? data.lat : '',
          long: data.long ? data.long : '',
          doctorType: data.doctorType ? data.doctorType : '',
          education: data.education ? data.education : '',
          experience: data.experience ? data.experience : '',
          speciality: data.speciality ? data.speciality : '',
          hospitalName: data.hospitalName ? data.hospitalName : '',
          profilePic: data.profilePic ? data.profilePic : '',
          mobile: data.mobile ? data.mobile : '',
          roles: stateData.roles[0],
        })
        // console.log('User data', userData)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })
  }, [])

  return (
    <>
      <div className='container-fluid mt-12'>
        <div className='card mt-12'>
          <div
            className='card-header'
            style={{
              backgroundColor: '#78aee9',
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItem: 'center',
              marginTop: '1%',
            }}
          >
            Profile
            <button
              // onClick={() => handleButton2()}
              onChange={(text) => setBtnDisabled(!text.target.value)}
              disabled={btnDisabled}
              style={{
                marginRight: '20',
                backgroundColor: '#2584eb',
                color: 'white',
                padding: 5,
                paddingLeft: 20,
                paddingRight: 20,
                borderRadius: 20,
                border: 2,
                borderColor: '#2584eb',
              }}
            >
              <b>{buttonText2}</b>
            </button>
          </div>

          {userData && (
            <form onSubmit={(e) => submit(e)}>
              {/* {showCardData2 ? ( */}
              <div className='row' style={{ marginTop: '2%' }}>
                <div className='col-md-6'>
                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Name:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      name='name'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({ ...formData, name: event.target.value })
                      }
                      defaultValue={formData.name}
                    ></input>
                  </div>
                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> User Name:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          username: event.target.value,
                        })
                      }
                      defaultValue={formData.username}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b>Address:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          address: event.target.value,
                        })
                      }
                      defaultValue={formData.address}
                    ></input>
                    {/* <button
                    class='btn btn-primary col-md-2'
                    style={{ height: 40, marginLeft: 5 }}
                  >
                    <b> Generate</b>
                  </button> */}
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Website:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          website: event.target.value,
                        })
                      }
                      defaultValue={formData.website}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Phone Number:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          phoneNo: event.target.value,
                        })
                      }
                      defaultValue={formData.phoneNo}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Mobile Number:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({ ...formData, mobile: event.target.value })
                      }
                      defaultValue={formData.mobile}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Lat:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({ ...formData, lat: event.target.value })
                      }
                      defaultValue={formData.lat}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Language:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({ ...formData, lang: event.target.value })
                      }
                      defaultValue={formData.lang}
                    ></input>
                  </div>
                </div>

                <div className='col-md-6'>
                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 150, height: 45 }}
                    >
                      <b> Picture(.jpg, .png, ):</b>
                    </label>
                    <input
                      style={{ marginLeft: '19%' }}
                      type='file'
                      id='myfile'
                      name='myfile'
                      multiple
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          profilePic: event.target.value,
                        })
                      }
                      defaultValue={formData.profilePic}
                    ></input>
                  </div>

                  <div className='col-md-6'>
                    <div className=' d-flex justify-content-center'>
                      <label
                        className='form-control'
                        style={{ border: 'none', marginRight: '30%' }}
                      >
                        <img
                          src='https://www.w3schools.com/howto/img_avatar.png'
                          alt='Avatar'
                          class='form-control'
                        />
                      </label>
                      <button
                        class='btn btn-primary col-md-4'
                        style={{
                          height: 40,
                          marginLeft: '5%',
                          display: 'flex',
                        }}
                      >
                        <b> Upload</b>
                      </button>
                    </div>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Email:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({ ...formData, email: event.target.value })
                      }
                      defaultValue={formData.email}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280, height: 20 }}
                    >
                      <b>Doctor Type: </b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          doctorType: event.target.value,
                        })
                      }
                      defaultValue={formData.doctorType}
                    ></input>
                    {/* <select
                    className='form-select col-sm-7'
                    name='docType'
                    id='docType'
                    onChange={(event) =>
                      setFormData({
                        ...formData,
                        doctorType: event.target.value,
                      })
                    }
                    Value={ formData.doctorType}
                  >
                    <option Value='Select'>Select</option>
                    <option Value='Doctor'>Doctor</option>
                    <option Value='Specialist'>Specialist</option>
                  </select> */}
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Education:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          education: event.target.value,
                        })
                      }
                      defaultValue={formData.education}
                    ></input>
                  </div>

                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Experience:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          experience: event.target.value,
                        })
                      }
                      defaultValue={formData.experience}
                    ></input>
                  </div>
                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Speciality:</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          speciality: event.target.value,
                        })
                      }
                      defaultValue={formData.speciality}
                    ></input>
                  </div>
                  <div className=' d-flex justify-content-center'>
                    <label
                      className='form-control'
                      style={{ border: 'none', width: 280 }}
                    >
                      <b> Hospital Name</b>
                    </label>
                    <input
                      style={{ marginLeft: '10%' }}
                      type='text'
                      className='form-control bg-light'
                      onChange={(event) =>
                        setFormData({
                          ...formData,
                          hospitalName: event.target.value,
                        })
                      }
                      defaultValue={formData.hospitalName}
                    ></input>
                  </div>
                  <div className='d-flex justify-content-center'>
                    <button
                      type='submit'
                      className='btn  btn-block btn-lg gradient-custom-4 text-body'
                      style={{ backgroundColor: '#00b0f0', color: 'white' }}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
              {/* <Dialog open={open} onClose={handleClose}>
                <DialogContent>
                  <DialogTitle>File uploaded Successfully...</DialogTitle>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleClose} color='primary'>
                    Close
                  </Button>
                </DialogActions>
              </Dialog> */}
              {/* <>
                {qid == undefined && (
                  <>
                    <input type='file' name='file' onChange={changeHandler} />
                    {isSelected ? (
                      <div>
                        <p>Filename: {selectedFile.name}</p>
                        <p>Filetype: {selectedFile.type}</p>
                        <p>Size in bytes: {selectedFile.size}</p>
                        <p>
                          lastModifiedDate:
                          {selectedFile.lastModifiedDate.toLocaleDateString()}
                        </p>
                      </div>
                    ) : (
                      <p>Select a file</p>
                    )}
                    <button
                      type='submit'
                      style={{
                        backgroundColor: "#EA4B64",
                        color: "#FFFFFF",
                        width: "10rem",
                        height: "2.4rem",
                      }}
                      onClick={handleSubmission}
                    >
                      + Upload
                    </button>
                  </>
                )}
              </> */}
              {/* ) : null} */}
            </form>
          )}
        </div>
      </div>
    </>
  )
}

export default Profile
