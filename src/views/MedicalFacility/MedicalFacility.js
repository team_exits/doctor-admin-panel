import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'

export default function MedicalFacility() {
  let { qid } = useParams()

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/medicalFacility`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/medicalFacility`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/medicalFacility`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/medicalFacility`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  name: null,
                  address: null,
                  profile_pic: null,
                  website: null,
                  email: null,
                  phone_no: null,
                  mobile_no: null,
                  lat: null,
                  lang: null,
                  appointments: null,
                  medicalFacility: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[6, 6]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'name',
                      label: 'Name',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'address',
                      label: 'Address',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'profile_pic',
                      label: 'Profile Picture',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'website',
                      label: 'Website',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'lang',
                      label: 'Language',
                      props: {},
                    },
                  ],
                  [
                    {
                      type: 'text',
                      name: 'email',
                      label: 'Email',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'phone_no',
                      label: 'Phone Number',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'mobile_no',
                      label: 'Mobile Number',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'lat',
                      label: 'Latitude',
                      props: {},
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
