import React from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { setLogin } from '../../redux/Slice/loginSlice'
import Config from '../../Config'
import axios from 'axios'
export default function Login() {
  const dispatch = useDispatch()

  const [formData, setFormData] = React.useState({
    username: '',
    email: '',
    password: '',
  })

  function handleChange(evt) {
    const value =
      evt.target.type === 'checkbox' ? evt.target.checked : evt.target.value
    setFormData({
      ...formData,
      [evt.target.name]: value,
    })
  }

  const login = async (e) => {
    e.preventDefault()
    console.log(formData)

    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      }

      const { data } = await axios.post(
        `http://${Config.IP}:${Config.PORT}/api/auth/signin`,
        { username: formData.username, password: formData.password },
        config
      )

      console.log('signIn axios>> ', data)

      if (data.success) {
        dispatch(setLogin({ isLogged: true, user: { data } }))

        // dispatch(
        //   setLogin({
        //     isLogged: true,
        //     login: {
        //       name: data ? data.user.name : null,
        //       username: data ? data.user.username : null,
        //       roles: data ? data.user.roles : null,
        //       email: data ? data.user.email : null,
        //       mobile: data ? data.user.mobile : null,
        //       roles: data ? data.user.roles[0] : null,
        //     },
        //     user: data?.user,
        //     token: data?.accessToken,
        //   })
        // )
      } else {
        dispatch(setLogin({ isLogged: false, login: formData }))
      }
    } catch (err) {
      console.log(err)
      if (err == 'Error: Request failed with status code 404') {
        alert('User Not Found')
      }
    }
  }

  return (
    <>
      <div
        style={{
          flex: '1',
          flexDirection: 'column',
          justifyContent: 'center',
          minHeight: '100vh',
          alignItems: 'center',
        }}
      >
        <div className='mask d-flex align-items-center h-100 gradient-custom-3'>
          <div className='container h-100'>
            <div className='row d-flex justify-content-center align-items-center h-100'>
              <div className='col-12 col-md-9 col-lg-7 col-xl-6'>
                <div
                  className='card'
                  style={{
                    borderRadius: '15px',
                    marginTop: '30%',
                    marginBottom: '30%',
                  }}
                >
                  <div className='card-body p-5'>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                      <h1 style={{ color: '#2a2a72' }}>Doctor App</h1>
                    </div>

                    <form>
                      <div className='form-group'>
                        <label for='exampleInputEmail1'>
                          <b>Enter Username</b>
                        </label>
                        {/* <input
                          type='email'
                          className='form-control col-lg-12'
                          name='email'
                          value={formData.email}
                          onChange={handleChange}
                          placeholder='Enter email'
                        /> */}
                        <input
                          type='text'
                          className='form-control col-lg-12'
                          name='username'
                          value={formData.username}
                          onChange={handleChange}
                          placeholder='Enter Username'
                        />
                      </div>
                      <div className='form-group'>
                        <label for='exampleInputPassword1'>
                          <b>Password</b>
                        </label>
                        <input
                          type='password'
                          className='form-control col-lg-12'
                          name='password'
                          value={formData.password}
                          onChange={handleChange}
                          placeholder='Enter Password'
                        />
                      </div>
                      {/* <div className='form-group form-check'>
                        <input
                          type='checkbox'
                          className='form-check-input'
                          id='exampleCheck1'
                        />
                        <label className='form-check-label' for='exampleCheck1'>
                          Check me out
                        </label>
                      </div> */}
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          marginTop: '1rem',
                        }}
                      >
                        <button
                          type='submit'
                          className='btn'
                          style={{ backgroundColor: '#2a2a72', color: 'white' }}
                          onClick={(e) => login(e)}
                        >
                          Login
                        </button>
                        <button
                          type='submit'
                          className='btn'
                          style={{ backgroundColor: '#2a2a72', color: 'white' }}
                        >
                          <Link
                            to='register'
                            style={{ textDecoration: 'none', color: 'white' }}
                          >
                            Register
                          </Link>
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
