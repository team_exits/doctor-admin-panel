import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'

export default function Message() {
  let { qid } = useParams()

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/message`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/message`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/message`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/message`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  user: null,
                  doc_id: null,
                  message: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[6, 6]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'user',
                      label: 'User',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'doc_id',
                      label: 'Doc ID',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'message',
                      label: 'Message',
                      props: {},
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
