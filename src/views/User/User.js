import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'
import { useDispatch, useSelector } from 'react-redux'

export default function User() {
  let { qid } = useParams()

  console.log('qid', qid)
  const login = useSelector((state) => state.login)
  const { token } = login
  console.log('TokenHai', token)
  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
                createType='POST'
                editApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
                editType='GET'
                updateApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
                updateType='PUT'
                deleteApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + token }}
                headers={{}}
                params={{}}
                initialState={{
                  content: null,
                  // levels: '60deacc31c0b9a091c2cb2f4',
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[6, 6]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'name',
                      label: 'Name',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'username',
                      label: 'User Name',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'email',
                      label: 'Email ID',
                      props: {},
                    },
                    {
                      type: 'password',
                      name: 'password',
                      label: 'Password',
                      props: {},
                    },
                  ],
                  [
                    {
                      type: 'text',
                      name: 'mobile',
                      label: 'Mobile Number',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'countryCode',
                      label: 'Country Code',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'referralCode',
                      label: 'Referral Code',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'profilePic',
                      label: 'Profile Picture ',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'roles',
                      label: 'Roles',
                      props: {},
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
