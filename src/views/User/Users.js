import { Button, makeStyles } from '@material-ui/core'
import React from 'react'
import Table from '../../components/Table/Table'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import ExtendedDataTables from '../../components/ExtendedDataTables/ExtendedDataTables'
import Config from '../../Config'

export default function ReadUser(props) {
  // const [error, setError] = React.useState(null);
  const { history } = props
  const navigation = props
  // const [loading, setLoading] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()

  const handleClickGroup = (value) => {
    history.push(`/user/${value._id}`)
  }
  const login = useSelector((state) => state.login)
  const { token } = login
  console.log('TokenHai', token)
  console.log()
  return (
    <div className={classes.root}>
      <div style={{ flex: 1 }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '2rem',
          }}
        >
          <div>
            <label>User</label>
          </div>
          <div style={{ display: 'flex' }}>
            <div>
              <Button
                onClick={() => history.push('/add-user')}
                variant='contained'
              >
                Add User
              </Button>
              <Button variant='outlined' style={{ marginLeft: '0.5rem' }}>
                Export
              </Button>
              <Button variant='outlined' style={{ marginLeft: '0.5rem' }}>
                Filter
              </Button>
            </div>
          </div>
        </div>
        <ExtendedDataTables
          navigation={navigation}
          editRoute='/user'
          // deleteRoute="/delete_FORM_ROUTE"
          cols={{
            // _id: "UID",
            name: null,
            username: null,
            email: null,
            status: null,
            profilePic: null,
            // roles: null,
          }}
          viewApiType='GET'
          viewApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
          editApiType='PUT'
          editApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
          deleteApiType='DELETE'
          deleteApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
          headers={{
            Authorization: 'Bearer ' + token,
          }}
          // params={}
        />
      </div>
    </div>
  )
}
