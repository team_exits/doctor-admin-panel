import { Button, makeStyles } from '@material-ui/core'
import React from 'react'
import Table from '../../components/Table/Table'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import ExtendedDataTables from '../../components/ExtendedDataTables/ExtendedDataTables'
import Config from '../../Config'

export default function ViewDoctors(props) {
  const { history } = props
  const navigation = props
  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()

  const dispatch = useDispatch()
  const login = useSelector((state) => state.login)
  const { token } = login
  console.log('TokenHai', token)
  console.log()
  return (
    <div className={classes.root}>
      <div style={{ flex: 1 }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '2rem',
          }}
        >
          <div>
            <label>User</label>
          </div>
        </div>
        <ExtendedDataTables
          navigation={navigation}
          editRoute='/user'
          // deleteRoute="/delete_FORM_ROUTE"
          cols={{
            username: null,
            name: null,
            address: null,
            website: null,
            phoneNo: null,
            lat: null,
            long: null,
            email: null,
            doctorType: null,
            education: null,
            experience: null,
            speciality: null,
            hospitalName: null,
            profilePic: null,
            mobile: null,
            verify: (obj) => {
              return {
                colName: 'Verify',
                return: (
                  <>
                    <button className='btn btn-success'>Approve</button>
                  </>
                ),
              }
            },
          }}
          viewApiType='GET'
          viewApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
          editApiType='PUT'
          editApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
          deleteApiType='DELETE'
          deleteApi={`${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`}
          headers={{
            Authorization: 'Bearer ' + token,
          }}
          // params={}
        />
      </div>
    </div>
  )
}
