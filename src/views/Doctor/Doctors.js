import { Button, makeStyles } from '@material-ui/core'
import React, { useState } from 'react'
import Table from '../../components/Table/Table'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { deleteDataById, getAllData } from '../../redux/Slice/caseSlice'
import { useDispatch, useSelector } from 'react-redux'
import Config from '../../Config'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import ExtendedDataTables from '../../components/ExtendedDataTables/ExtendedDataTables'
import { object } from 'yup'
export default function ReadDoctor(props) {
  const { history } = props

  const navigation = props
  console.log('extprops', props)

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()
  // console.log('questionApi : ', questionApi)
  return (
    <div className={classes.root}>
      <div style={{ flex: 1 }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '2rem',
          }}
        >
          <div>
            <label>Question</label>
          </div>
          <div style={{ display: 'flex' }}>
            <div>
              <Button
                onClick={() => history.push('/add-question')}
                variant='contained'
              >
                Add Question
              </Button>
              <Button variant='outlined' style={{ marginLeft: '0.5rem' }}>
                Explore
              </Button>
              <Button variant='outlined' style={{ marginLeft: '0.5rem' }}>
                Filter
              </Button>
            </div>
          </div>
        </div>
        <ExtendedDataTables
          navigation={navigation}
          editRoute='/doctor'
          // deleteRoute="/delete_FORM_ROUTE"
          cols={{
            // _id: "UID",
            name: null,
            address: null,
            profile_pic: null,
            doctor_type: null,
            website: null,
            email: null,
            phone_no: null,
            mobile_no: null,
            lat: null,
            lang: null,
            education: null,
            experience: null,
            appointments: null,
            doctorAppointment: null,
            symptom: null,
          }}
          viewApiType='GET'
          viewApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
          editApiType='PUT'
          editApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
          deleteApiType='DELETE'
          deleteApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
          // headers={}
          // params={}
        />
      </div>
    </div>
  )
}
