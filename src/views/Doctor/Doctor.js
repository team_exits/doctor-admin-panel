import React, { useState, Modal } from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import axios from 'axios'

export default function Doctor() {
  let { qid } = useParams()
  console.log('qid >>> ', qid)
  // =========================
  const [selectedFile, setSelectedFile] = useState()
  const [isSelected, setIsSelected] = useState(false)

  //========== DialogBox =================
  const [open, setOpen] = React.useState(false)

  const handleClose = () => {
    setOpen(false)
  }
  //====================================

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0])
    setIsSelected(true)
  }

  const handleSubmission = (e) => {
    e.preventDefault()
    const formData = new FormData()

    formData.append('File', selectedFile)
    formData.append('Path', '/doctor')
    console.log('formdata>>>>>>>>>>', formData)

    axios({
      url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/upload`,
      method: 'POST',
      data: formData,
    })
      .then((result) => {
        console.log('Success:', result)
        setOpen(true)
      })
      .catch((error) => {
        console.error('Error:', error)
      })
  }

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/doctor`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  name: null,
                  address: null,
                  profile_pic: null,
                  website: null,
                  doctor_type: null,
                  email: null,
                  phone_no: null,
                  mobile_no: null,
                  lat: null,
                  lang: null,
                  education: null,
                  experience: null,
                  appointments: null,
                  doctorAppointment: null,
                  symptom: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[7, 7]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'name',
                      label: 'Name',
                      props: {},
                    },
                    {
                      type: 'select',
                      name: 'doctor_type',
                      label: 'Doctor Type',
                      props: {
                        options: [
                          { doctor: 'Doctor' },
                          { specialist: 'Specialist' },
                        ],
                      },
                    },
                    {
                      type: 'text',
                      name: 'address',
                      label: 'Address',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'website',
                      label: 'Website',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'lang',
                      label: 'Language',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'experience',
                      label: 'Experience',
                      props: {},
                    },
                  ],
                  [
                    {
                      type: 'text',
                      name: 'email',
                      label: 'Email ',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'category',
                      label: 'Category',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'phone_no',
                      label: 'Phone Number',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'mobile_no',
                      label: 'Mobile Number',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'lat',
                      label: 'Latitude',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'education',
                      label: 'Education',
                      props: {},
                    },
                  ],
                ]}
              />
              <Dialog open={open} onClose={handleClose}>
                <DialogContent>
                  <DialogTitle>File uploaded Successfully...</DialogTitle>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleClose} color='primary'>
                    Close
                  </Button>
                </DialogActions>
              </Dialog>
              <>
                {qid == undefined && (
                  <>
                    <input type='file' name='file' onChange={changeHandler} />
                    {isSelected ? (
                      <div>
                        <p>Filename: {selectedFile.name}</p>
                        <p>Filetype: {selectedFile.type}</p>
                        <p>Size in bytes: {selectedFile.size}</p>
                        <p>
                          lastModifiedDate:
                          {selectedFile.lastModifiedDate.toLocaleDateString()}
                        </p>
                      </div>
                    ) : (
                      <p>Select a file</p>
                    )}
                    <button
                      type='submit'
                      style={{
                        backgroundColor: '#EA4B64',
                        color: '#FFFFFF',
                        width: '10rem',
                        height: '2.4rem',
                      }}
                      onClick={handleSubmission}
                    >
                      + Upload
                    </button>
                  </>
                )}
              </>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
