import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import { fade, makeStyles } from "@material-ui/core/styles";
import {
  AiOutlineCalendar,
  AiOutlineHeart,
  AiOutlineHome,
  AiOutlineLoading3Quarters,
  AiTwotoneCalendar,
} from "react-icons/ai";
import {
  RiCupLine,
  RiTShirt2Line,
  RiPencilRuler2Line,
  RiTodoLine,
} from "react-icons/ri";
import { BiTask } from "react-icons/bi";
import { GoLaw } from "react-icons/go";
import { MdEvent, MdDateRange } from "react-icons/md";
import { SiGooglesheets } from "react-icons/si";
import { HiDocumentText } from "react-icons/hi";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "10ch",
      },
    },
  },
}));

export default function LeftSection() {
  const history = useHistory();

  const classes = useStyles();
  return (
    <div className="feed">
      <div className="searchbar">
        <div className="left">
          {/* <div style={{alignItems:"center",display:'flex',flexDirection:'row',justifyContent:'space-evenly',marginTop:'3%'}}>
          <div className="" style={{backgroundColor:"#fff",borderRadius:'8px'}}>
              <h4 style={{padding:'.3rem'}}><BiTask style={{marginRight:'5px'}}/>Add Case</h4>
          </div>
          <div className="" style={{backgroundColor:"#fff",borderRadius:'8px'}}>
          <h4 style={{padding:'.3rem'}}><AiTwotoneCalendar style={{marginRight:'5px'}}/>Calendar</h4>
          </div>
          </div> */}
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ "aria-label": "search" }}
            />
          </div>
        </div>
        <div className="right">
          <h5 style={{ marginTop: 5, fontSize: 15 }}>
            <AiOutlineCalendar /> Set Date
          </h5>
        </div>
      </div>
      <div className="middle-bar">
        <div className="outside">
          <h1 style={{ marginTop: 15, color: "white" }}>WELCOME</h1>
          <p style={{ marginTop: 15, color: "white" }}></p>
          {/* <h5 style={{ marginTop: 15, color: "white" }}>Business health</h5> */}
          <div className="box">
            <div
              className="inside-box"
              style={{
                marginTop: 15,
                height: 60,
                width: 180,
                backgroundColor: "white",
                borderRadius: "10px",
              }}
            >
              <div className="inside-box-1">
                {/* <AiOutlineHeart size={30} color="white" style={{ margin: 5 }} /> */}
                <h5
                  style={{
                    fontSize: "20px",
                    paddingLeft: "1rem",
                    width: "50%",
                    color: "#fff",
                    border: "1px solid",
                    borderRadius: " 6px",
                    padding: "4px",
                    textAlign: "center",
                    background: "#5c65fe",
                    marginRight: "10px",
                  }}
                >
                  25
                </h5>
              </div>
              <div className="inside-box-2">
                <h5>Amazon</h5>
                <p style={{ fontSize: 10 }}>Books</p>
              </div>
            </div>
            <div
              className="inside-box-main"
              style={{
                marginTop: 15,
                paddingleft: 30,
                height: 60,
                width: 180,
                backgroundColor: "white",
                borderRadius: "10px",
              }}
            >
              <div className="inside-box-3">
                {/* <AiOutlineHome size={30} color="white" style={{ margin: 5 }} />
                 */}
                <h5
                  style={{
                    fontSize: "20px",
                    paddingLeft: "1rem",
                    width: "50%",
                    color: "#fff",
                    border: "1px solid",
                    borderRadius: " 6px",
                    padding: "4px",
                    textAlign: "center",
                    background: "#5c65fe",
                  }}
                >
                  25
                </h5>
              </div>
              <div className="inside-box-4">
                <h5>Total Task :20</h5>
                <p style={{ fontSize: 10 }}>Pending Task</p>
              </div>
            </div>
          </div>
        </div>
        <div className="inside">
          <AiOutlineLoading3Quarters
            size={40}
            color="white"
            style={{ margin: 30 }}
          />
          {/* <h6 style={{ color: "white", marginTop: 35, paddingLeft: 20 }}>
            Unit stock (K)
          </h6> */}
          {/* <h1 style={{ color: "white", marginTop: 5, paddingLeft: 20 }}>
            525 Units
          </h1> */}
          <h3 style={{ color: "white", marginTop: 5, paddingLeft: 20 }}>
            Orders: 20
          </h3>
          <h6 style={{ color: "white", marginTop: 10, paddingLeft: 20 }}>
            Processed Orders : 20{/* Stock exceeding */}
          </h6>
          <h6 style={{ color: "white", paddingLeft: 20 }}>
            Pending Orders : 40{/* sales By 25% */}
          </h6>
          <br />
        </div>
      </div>
      <div className="end-bar">
        {/* <div className="img-side">
          <div className="img-side-inside-1">
            <h4 style={{ padding: ".5rem", marginLeft: "1rem" }}>TO DO:</h4>
          </div>
          <div
            className="img-side-inside-1 img-margin"
            style={{
              marginBottom: "10px",
              width: "80%",
              margin: "0 auto",
              borderRadius: "10px",
              backgroundColor: "#fff",
            }}
          >
            <h4
              style={{ padding: ".5rem", textAlign: "left", fontWeight: "500" }}
            >
              Manage Task
            </h4>
          </div>
          <div
            className="img-side-inside-1 img-margin"
            style={{
              marginBottom: "10px",
              width: "80%",
              margin: "0 auto",
              borderRadius: "10px",
              backgroundColor: "#fff",
            }}
          >
            <h4
              style={{ padding: ".5rem", textAlign: "left", fontWeight: "500" }}
            >
              Pay Bills
            </h4>
          </div>
          <div
            className="img-side-inside-1 img-margin"
            style={{
              marginBottom: "10px",
              width: "80%",
              margin: "0 auto",
              borderRadius: "10px",
              backgroundColor: "#fff",
            }}
          >
            <h4
              style={{ padding: ".5rem", textAlign: "left", fontWeight: "500" }}
            >
              Meeting
            </h4>
          </div>
          <div
            className="img-side-inside-1 img-margin"
            style={{
              marginBottom: "10px",
              width: "80%",
              margin: "0 auto",
              borderRadius: "10px",
              backgroundColor: "#fff",
            }}
          >
            <h4
              style={{ padding: ".5rem", textAlign: "left", fontWeight: "500" }}
            >
              Conference Call
            </h4>
          </div>
          {/* <div className="img-side-inside-2">
            <h6 style={{ paddingLeft: 10, marginTop: 5 }}>MONTHLY EMPLOYEE</h6>
            <h4 style={{ paddingLeft: 10, marginTop: 10 }}>Neil P. Harrison</h4>
            <h6 style={{ paddingLeft: 10, marginTop: 3 }}>Fightright</h6>
          </div> </div> */}

        {/* <div className="img-outside">
          <div className="img-outside-inside-1">
            <h5 style={{ paddingLeft: 40, marginTop: 10 }}>
              What do you wish to do!
            </h5>
          </div>
          <div className="img-outside-inside-2" style={{ flexWrap: "wrap" }}>
            <div
              onClick={() => {
                history.push("/add-case");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1"
            >
              <BiTask size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add case </h6>
            </div>
            <div
              onClick={() => {
                history.push("/add-advocate");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1"
            >
              <GoLaw size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add Advocate </h6>
            </div>
            <div
              onClick={() => {
                history.push("/add-todo");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1"
            >
              <RiTodoLine size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add To Do </h6>
            </div>
            <div
              onClick={() => {
                history.push("/add-order");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1"
            >
              <SiGooglesheets size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add Order </h6>
            </div>
            <div
              onClick={() => {
                history.push("/add-documents");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1"
            >
              <HiDocumentText size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add Document</h6>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              marginTop: "10px",
              marginLeft: "8px",
            }}
          >
            <div
              onClick={() => {
                history.push("/calender");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1 new-img"
            >
              <MdDateRange size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add Calender </h6>
            </div>
            <div
              onClick={() => {
                history.push("/add-client");
              }}
              style={{ cursor: "pointer" }}
              className="img-outside-inside-2-1"
            >
              <MdDateRange size={50} style={{ marginTop: 10 }} />
              <h6 style={{ marginTop: 10 }}>Add client </h6>
            </div>
          </div>
        </div>
       */}
      </div>
    </div>
  );
}
