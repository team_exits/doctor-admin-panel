import React, { useEffect, useState } from 'react'
import DashboardCalendar from './DashboardCalendar'
import Config from '../../Config'
import axios from 'axios'
import { useSelector } from 'react-redux'
import './Dashboard.css'
import LeftSection from './LeftSection/index.js'

const Dashboard = () => {
  const [userList, setUserList] = useState([])
  const [contactData, setContactData] = useState([])
  const [appointmentData, setAppointmentData] = useState([])

  const { name, roles, email, token } = useSelector(
    (state) => state.login.user.data.user
  )
  useEffect(() => {
    console.log('Inisde UseEffect>>>>>>>>>>>>>>>>>>')
    //User GET request
    axios({
      method: 'GET',
      url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/role`,
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        console.log(' Roles Data >>>>>>>>>>>', response.data)
        const usrRole = response.data.role.filter((d, i) => {
          console.log('usrRole>', d.name, d.name.match(/user/gi)?.length)
          return d.name.match(/user/gi)?.length > 0
        })[0]
        console.log('usrRoleeeeeeeee>>>', usrRole)
        axios({
          method: 'GET',
          url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/admin/user`,
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json',
          },
        })
          .then((response2) => {
            console.log(
              ' Response 2 Data Roles >>>>>>>>>>>',
              response2.data.results
            )
            const usrlst = response2.data.results
            // setuserList(usrlst)
            console.log('User List>>>>>>>>>>>Response2', usrlst, usrRole)
            let user = usrlst

            console.log('User>>>', user)
            setUserList(user)
          })
          .catch((error2) => {
            console.log('Error2 >>', error2)
          })
      })
      .catch((error) => {
        console.log('Error >>', error)
      })

    // axios for contact
    axios({
      method: 'GET',
      url: `http://${Config.IP}:${Config.PORT}/api/contact`,
    })
      .then((response) => {
        let contact = response.data.results
        console.log(' contactData>>>>>>>>>>>', contact)
        setContactData(contact)
        console.log('contactData>>>>>>>>>>>>', contactData)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })

    // axios for appointment
    axios({
      method: 'GET',
      url: `http://${Config.IP}:${Config.PORT}/api/appointment`,
    })
      .then((response) => {
        let appointment = response.data.results
        console.log(' appointmentData>>>>>>>>>>>', appointment)
        setAppointmentData(appointment)
        console.log('appointmentData>>>>>>>>>>>>', appointmentData)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })
  }, [])
  return (
    <>
      <div
        className='home-content'
        style={{
          position: 'relative',
        }}
      >
        <div
          className='overview-boxes'
          style={{ paddingTop: '50px', display: 'flex', width: '135%' }}
        >
          <div
            class='box'
            style={{
              padding: '15px 104px',
              borderRadius: '15px',
              display: 'flex',
              alignItems: 'center',
              backgroundColor: '#cce6ff',
              marginLeft: '5%',
              marginRight: '5%',
              textAlign: 'center',
              justifyContent: 'center',
              color: ' rgba(0, 0, 0, 0.87)',
              transition: ' box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
              display: ' flex',
              flexDirection: ' column',
              position: ' relative',
              minWidth: ' 0px',
              overflowWrap: ' break-word',
              backgroundClip: ' border-box',
              border: ' 0px solid rgba(0, 0, 0, 0.125)',
              borderRadius: ' 0.75rem',
              boxShadow:
                ' rgb(0 0 0 / 10%) 0rem 0.25rem 0.375rem -0.0625rem, rgb(0 0 0 / 6%) 0rem 0.125rem 0.25rem -0.0625rem',
              overflow: ' visible',
            }}
          >
            <div class='left-side'>
              <div class='box-topic' style={{ fontSize: 20, fontWeight: 800 }}>
                Total User
              </div>
              <div
                class='number'
                style={{
                  fontSize: 35,
                  fontWeight: 600,
                  marginTop: '-5px',
                  display: 'inline-block',
                }}
              >
                {userList.length}
              </div>
            </div>
          </div>

          <div
            class='box'
            style={{
              padding: '15px 104px',
              borderRadius: '15px',
              display: 'flex',
              backgroundColor: ' #f1d9f1',
              marginRight: '5%',
              textAlign: 'center',
              justifyContent: 'center',
              color: ' rgba(0, 0, 0, 0.87)',
              transition: ' box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
              display: ' flex',
              flexDirection: ' column',
              position: ' relative',
              minWidth: ' 0px',
              overflowWrap: ' break-word',
              backgroundClip: ' border-box',
              border: ' 0px solid rgba(0, 0, 0, 0.125)',
              borderRadius: ' 0.75rem',
              boxShadow:
                ' rgb(0 0 0 / 10%) 0rem 0.25rem 0.375rem -0.0625rem, rgb(0 0 0 / 6%) 0rem 0.125rem 0.25rem -0.0625rem',
              overflow: ' visible',
            }}
          >
            <div class='left-side'>
              <div class='box-topic' style={{ fontSize: 20, fontWeight: 800 }}>
                Total Contacts
              </div>
              <div
                class='number'
                style={{
                  fontSize: 35,
                  fontWeight: 600,
                  marginTop: '-5px',
                  display: 'inline-block',
                }}
              >
                {contactData.length}
              </div>
            </div>
          </div>

          <div
            class='box'
            style={{
              padding: '15px 104px',
              borderRadius: '15px',
              display: 'flex',
              backgroundColor: '#ffe8b3',
              marginRight: '5%',
              textAlign: 'center',
              justifyContent: 'center',
              color: ' rgba(0, 0, 0, 0.87)',
              transition: ' box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
              display: ' flex',
              flexDirection: ' column',
              position: ' relative',
              minWidth: ' 0px',
              overflowWrap: ' break-word',
              backgroundClip: ' border-box',
              border: ' 0px solid rgba(0, 0, 0, 0.125)',
              borderRadius: ' 0.75rem',
              boxShadow:
                ' rgb(0 0 0 / 10%) 0rem 0.25rem 0.375rem -0.0625rem, rgb(0 0 0 / 6%) 0rem 0.125rem 0.25rem -0.0625rem',
              overflow: ' visible',
            }}
          >
            <div class='left-side'>
              <div class='box-topic' style={{ fontSize: 20, fontWeight: 800 }}>
                Total Appointments
              </div>
              <div
                class='number'
                style={{
                  fontSize: 35,
                  fontWeight: 600,
                  marginTop: '-5px',
                  display: 'inline-block',
                }}
              >
                {appointmentData.length}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Dashboard
