import React from "react";
import InfiniteCalendar from "react-infinite-calendar";
import "react-infinite-calendar/styles.css";

// Render the Calendar
var today = new Date();
var lastWeek = new Date(
  today.getFullYear(),
  today.getMonth(),
  today.getDate() - 7
);

const DashboardCalendar = () => {
  return (
    <div style={{ marginTop: "1rem" }}>
      <InfiniteCalendar
        width="100%"
        height={400}
        selected={today}
        disabledDays={[0, 6]}
        minDate={lastWeek}
      />
    </div>
  );
};

export default DashboardCalendar;
