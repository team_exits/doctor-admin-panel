import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'

export default function Fee() {
  let { qid } = useParams()

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  consult_type: null,
                  description: null,
                  price: null,
                  unit: null,
                  unit_caption: null,

                  doctors: null,
                  users: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[6, 6]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'consult_type',
                      label: 'Consult Type',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'description',
                      label: 'Description',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'price',
                      label: 'Price',
                      props: {},
                    },
                  ],
                  [
                    {
                      type: 'text',
                      name: 'unit',
                      label: 'Unit',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'unit_caption',
                      label: ' Unit Caption',
                      props: {},
                    },
                    // {
                    //   type: 'text',
                    //   name: 'doctor_id',
                    //   label: ' Doctor Id',
                    //   props: {},
                    // },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
