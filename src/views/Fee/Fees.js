import { Button, makeStyles } from '@material-ui/core'
import React, { useState } from 'react'
import Table from '../../components/Table/Table'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import { deleteDataById, getAllData } from '../../redux/Slice/caseSlice'
import { useDispatch, useSelector } from 'react-redux'
import Config from '../../Config'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import ExtendedDataTables from '../../components/ExtendedDataTables/ExtendedDataTables'
import { object } from 'yup'
export default function ReadFee(props) {
  const { history } = props

  const navigation = props
  console.log('extprops', props)

  const useStyles = makeStyles((theme) => ({
    root: {
      // flexGrow: 1,
      // backgroundColor: "white",
      display: 'flex',
      overflowY: 'scroll',
      width: '100%',
    },
  }))
  const classes = useStyles()
  // console.log('questionApi : ', questionApi)
  return (
    <div className={classes.root}>
      <div style={{ flex: 1 }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '2rem',
          }}
        >
          <div>
            <label>Question</label>
          </div>
          <div style={{ display: 'flex' }}>
            <div>
              <Button
                onClick={() => history.push('/add-question')}
                variant='contained'
              >
                Add Question
              </Button>
              <Button variant='outlined' style={{ marginLeft: '0.5rem' }}>
                Explore
              </Button>
              <Button variant='outlined' style={{ marginLeft: '0.5rem' }}>
                Filter
              </Button>
            </div>
          </div>
        </div>
        <ExtendedDataTables
          navigation={navigation}
          editRoute='/fee'
          // deleteRoute="/delete_FORM_ROUTE"
          cols={{
            // _id: "UID",
            consult_type: null,
            description: null,
            price: null,
            unit: null,
            unit_caption: null,

            doctors: null,
            users: null,
          }}
          viewApiType='GET'
          viewApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
          editApiType='PUT'
          editApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
          deleteApiType='DELETE'
          deleteApi={`http://${Config.IP}:${Config.PORT}/api/fee`}
          // headers={}
          // params={}
        />
      </div>
    </div>
  )
}
