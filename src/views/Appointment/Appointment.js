import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'

export default function Appointment() {
  let { qid } = useParams()

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/appointment`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/appointment`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/appointment`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/appointment`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  category: null,
                  medicalFacilities: null,
                  doctor: null,
                  timing: null,
                  patient: null,
                  user_id: null,
                  users: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[7, 7]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    // {
                    //   type: 'text',
                    //   name: 'category',
                    //   label: 'Category',
                    //   props: {},
                    // },
                    // {
                    //   type: 'text',
                    //   name: 'medicalFacilities',
                    //   label: 'Medical Facilities',
                    //   props: {},
                    // },
                    {
                      type: 'text',
                      name: 'patient',
                      label: 'Patient',
                      props: {},
                    },
                  ],
                  [
                    // {
                    //   type: 'text',
                    //   name: 'doctor',
                    //   label: 'Doctor',
                    //   props: {},
                    // },
                    // {
                    //   type: 'text',
                    //   name: 'timing',
                    //   label: 'Time',
                    //   props: {},
                    // },
                    // {
                    //   type: 'text',
                    //   name: 'user_id',
                    //   label: 'User id',
                    //   props: {},
                    // },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
