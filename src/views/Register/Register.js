import React, { useEffect, useState } from 'react'
import { Dropdown } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { setLogin } from '../../redux/Slice/loginSlice'
import axios from 'axios'
import Config from '../../Config'
import { useSelector } from 'react-redux'

export default function Register() {
  const dispatch = useDispatch()
  const [rolesData, setRolesData] = useState([])
  const [categoryData, setCategoryData] = useState([])

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    phoneNo: '',
    gender: 'Male',
    username: '',
    categories: '',
    roles: '',
  })

  useEffect(() => {
    setFormData({
      name: '',

      email: '',
      password: '',
      phoneNo: '',
      gender: 'Male',
      username: '',
      categories: '',
      roles: '',
    })

    dispatch(
      setLogin({
        login: null,
        loading: false,
        isLogged: false,
        token: null,
        user: {},
        error: null,
        selected: 0,
      })
    )

    axios({
      method: 'GET',
      url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/role`,
    })
      .then((response) => {
        console.log(' roleData >>>>>>>>>>>', response.data.role)
        setRolesData(response.data.role)
        console.log('roleData', rolesData)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })

    //category axios GET Method
    axios({
      method: 'GET',
      url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/category`,
    })
      .then((response) => {
        console.log(' categoryData >>>>>>>>>>>', response.data.results)
        setCategoryData(response.data.results)
      })
      .catch((error) => {
        console.log('Error >>', error)
      })
  }, [])

  const OnhandleChange = (event) => {
    console.log('Gender>>', event.target.value)
    setFormData({ ...formData, gender: event.target.value })
  }
  function handleChange(evt) {
    const value =
      evt.target.type === 'radio' ? evt.target.checked : evt.target.value
    setFormData({
      ...formData,
      [evt.target.name]: value,
    })
    console.log(formData)
  }
  const register = async (e) => {
    e.preventDefault()
    console.log(formData)

    if (
      formData.name != '' ||
      formData.email != '' ||
      formData.password != ''
    ) {
      try {
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        }

        const { data } = await axios.post(
          `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/auth/signup`,
          {
            username: formData.username,
            password: formData.password,
            name: formData.name,
            phoneNo: formData.phoneNo,
            gender: formData.gender,
            email: formData.email,
            roles: formData.roles,
            categories: formData.categories,
          },
          config
        )

        console.log('data', data)

        if (data.success) {
          alert('Registered Successfully...')

          dispatch(setLogin({ isLogged: true, user: { data } }))
          setFormData({
            name: '',

            email: '',
            password: '',
            phoneNo: '',
            gender: '',
            username: '',
            roles: '',
            categories: '',
          })
        } else {
          // dispatch(setLogin({ isLogged: false, user: formData }))
        }
      } catch (err) {
        console.log(err)
        if (err == 'Error: Request failed with status code 404') {
          alert('User Not Found')
        }
      }
    } else {
      alert('Please fill all fields ')
    }
  }

  return (
    <>
      <div
        style={{
          flex: '1',
          flexDirection: 'column',
          justifyContent: 'center',
          minHeight: '100vh',
          alignItems: 'center',
          height: '500px',
          overflowY: 'scroll',
        }}
      >
        <div className='mask d-flex align-items-center h-100 gradient-custom-5'>
          <div className='container h-100'>
            <div className='row d-flex justify-content-center align-items-center h-100'>
              <div className='col-12 col-md-6 col-lg-6 col-xl-6'>
                <div className='card' style={{ borderRadius: '15px' }}>
                  <div className='card-body p-5'>
                    <h2 className='text-uppercase text-center mb-2'>
                      Create an account
                    </h2>

                    <form onSubmit={(e) => register(e)}>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          marginBottom: '-5%',
                        }}
                      >
                        <div className='form-outline mb-4'>
                          <label className='form-label' for='form3Example1cg'>
                            Name
                          </label>
                          <input
                            type='text'
                            className='form-control form-control-lg'
                            name='name'
                            value={formData.name}
                            onChange={handleChange}
                          />
                        </div>
                        <div
                          className='form-outline mb-4'
                          style={{ marginLeft: '0.5rem' }}
                        >
                          <label className='form-label' for='form3Example1cg'>
                            User Name
                          </label>
                          <input
                            type='text'
                            className='form-control form-control-lg'
                            name='username'
                            value={formData.username}
                            onChange={handleChange}
                          />
                        </div>
                      </div>

                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          marginBottom: '-5%',
                        }}
                      >
                        <div className='form-outline mb-4'>
                          <label className='form-label' for='form3Example3cg'>
                            Phone
                          </label>

                          <input
                            type='text'
                            className='form-control form-control-lg'
                            name='phoneNo'
                            value={formData.phoneNo}
                            onChange={handleChange}
                          />
                        </div>

                        <div
                          className='form-outline mb-4'
                          style={{ marginLeft: '0.5rem' }}
                        >
                          <label className='form-label' for='form3Example4cg'>
                            Email
                          </label>

                          <input
                            type='email'
                            className='form-control form-control-lg'
                            name='email'
                            value={formData.email}
                            onChange={handleChange}
                          />
                        </div>
                      </div>

                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          marginBottom: '-5%',
                        }}
                      >
                        <div className='form-outline mb-4'>
                          <label className='form-label' for='form3Example4cdg'>
                            Password
                          </label>
                          <input
                            type='password'
                            className='form-control form-control-lg'
                            name='password'
                            value={formData.password}
                            onChange={handleChange}
                          />
                        </div>

                        <div
                          className='form-outline mb-4'
                          style={{ marginLeft: '0.5rem' }}
                        >
                          <label className='form-label' for='form3Example4cdg'>
                            Confirm Password
                          </label>
                          <input
                            type='password'
                            className='form-control form-control-lg'
                            name='confirmpassword'
                            value={formData.confirmpassword}
                            onChange={handleChange}
                          />
                        </div>
                      </div>

                      <div
                        className='form-outline mb-4'
                        style={{ display: 'flex', marginBottom: '-5%' }}
                      >
                        <div style={{ flex: '0.2', fontSize: '18px' }}>
                          <label className='form-label' for='form3Example4cdg'>
                            Gender
                          </label>
                        </div>
                        <div
                          class='form-check form-check-inline'
                          style={{ flex: '0.8', fontSize: '18px' }}
                        >
                          <div>
                            <input
                              type='radio'
                              value='MALE'
                              defaultChecked
                              name='gender'
                              onChange={OnhandleChange}
                            />{' '}
                            Male
                            {/* <input
                              class='form-check-input'
                              type='radio'
                              name='gender'
                              value={formData.gender}
                              checked={formData.gender === 'Male'}
                              onChange={OnhandleChange}
                            />
                            <label
                              class='form-check-label'
                              for='inlineRadio1'
                              style={{ textAlign: 'center' }}
                            >
                              Male
                            </label> */}
                          </div>
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          <div class='form-check form-check-inline'>
                            {/* <input
                              class='form-check-input'
                              type='radio'
                              name='gender'
                              value={formData.gender}
                              checked={formData.gender === 'Female'}
                              onChange={OnhandleChange}
                            />
                            <label
                              class='form-check-label'
                              for='inlineRadio2'
                              style={{ textAlign: 'center' }}
                            >
                              Female
                            </label> */}
                            <input
                              type='radio'
                              value='Female'
                              onChange={OnhandleChange}
                              name='gender'
                            />{' '}
                            Female
                          </div>
                        </div>
                      </div>

                      <div class='form-group' style={{ display: 'flex' }}>
                        <label
                          className='form-label'
                          for='form3Example4cdg'
                          style={{ fontSize: '18px', flex: '0.2' }}
                        >
                          Category
                        </label>

                        <div style={{ fontSize: '15px', flex: '0.8' }}>
                          <select
                            className='form-control bg-light'
                            onChange={(event) => {
                              setFormData({
                                ...formData,
                                categories: event.target.value,
                              })
                            }}
                            value={formData.categories}
                          >
                            <option>Select Category</option>
                            {categoryData.map((d, i) => {
                              return <option value={d.name}>{d.name}</option>
                            })}
                          </select>
                        </div>
                      </div>

                      <div class='form-group' style={{ display: 'flex' }}>
                        <label
                          className='form-label'
                          for='form3Example4cdg'
                          style={{ fontSize: '18px', flex: '0.2' }}
                        >
                          Roles
                        </label>

                        <div style={{ fontSize: '15px', flex: '0.8' }}>
                          <select
                            className='form-control bg-light'
                            onChange={(event) => {
                              setFormData({
                                ...formData,
                                roles: event.target.value,
                              })
                            }}
                            value={formData.roles}
                          >
                            <option>Select Role</option>
                            {rolesData.map((d, i) => {
                              return <option value={d.name}>{d.name}</option>
                            })}
                          </select>
                        </div>
                      </div>

                      <div className='d-flex justify-content-center'>
                        <button
                          type='submit'
                          className='btn  btn-block btn-lg gradient-custom-4 text-body'
                          style={{ backgroundColor: '#00b0f0', color: 'white' }}
                        >
                          Register
                        </button>
                      </div>

                      <p className='text-center text-muted mt-3 mb-0'>
                        Have already an account?
                      </p>
                      <a
                        href='#!'
                        className='fw-bold text-body'
                        style={{ marginLeft: '18%' }}
                      >
                        <Link to='login'>Login here</Link>
                      </a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
