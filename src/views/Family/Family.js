import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'

export default function Family() {
  let { qid } = useParams()

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/family`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/family`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/family`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/family`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  user_id: null,
                  relationship: null,
                  primary_user: null,
                  users: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[6, 6]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'relationship',
                      label: 'Relationship',
                      props: {},
                    },
                    // {
                    //   type: 'text',
                    //   name: 'user_id',
                    //   label: 'user id',
                    //   props: {},
                    // },
                  ],
                  [
                    {
                      type: 'text',
                      name: 'primary_user',
                      label: 'Primary user',
                      props: {},
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
