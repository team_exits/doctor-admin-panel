import React from 'react'
import FormGenerator from '../../components/Formake/FormGenerator'
import Formake from '../../components/Formake/Formake'
import { useParams } from 'react-router-dom'
import Config from '../../Config'
import '../../components/Formake/Formake.css'

export default function Symptom() {
  let { qid } = useParams()

  return (
    <>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='Regular-form'>
              <Formake
                useAxios={true}
                edit={qid ? true : false}
                editId={qid}
                // title={qid ? 'Edit Form' : 'Add Form' }
                createApi={`http://${Config.IP}:${Config.PORT}/api/symptom`}
                createType='POST'
                editApi={`http://${Config.IP}:${Config.PORT}/api/symptom`}
                editType='GET'
                updateApi={`http://${Config.IP}:${Config.PORT}/api/symptom`}
                updateType='PUT'
                deleteApi={`http://${Config.IP}:${Config.PORT}/api/symptom`}
                deleteType='DELETE'
                auth={{ Authorization: 'Bearer ' + 'TOKEN' }}
                headers={{}}
                params={{}}
                initialState={{
                  name: null,
                  description: null,
                  images: null,
                  medicalFacility: null,
                  doctors: null,
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[6, 6]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log('formValues UPDATING --> ', formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log('formValuesx UPDATING --> ', name, value)
                }}
                onSubmit={(values) =>
                  console.log('formValues SUBMITTING', values)
                }
                fields={[
                  [
                    {
                      type: 'text',
                      name: 'name',
                      label: 'Name',
                      props: {},
                    },
                    {
                      type: 'text',
                      name: 'description',
                      label: 'Description',
                      props: {},
                    },
                    // {
                    //   type: 'text',
                    //   name: 'doctor_id',
                    //   label: 'doctor id',
                    //   props: {},
                    // },
                  ],
                  [
                    // {
                    //   type: 'text',
                    //   name: 'medical_Facilities',
                    //   label: 'Medical Facilities',
                    //   props: {},
                    // },
                    {
                      type: 'text',
                      name: 'images',
                      label: 'Images',
                      props: {},
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
