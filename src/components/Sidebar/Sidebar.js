import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar'
import 'react-pro-sidebar/dist/css/styles.css'
import { FaGem, FaHeart } from 'react-icons/fa'
import { BiMenu, BiMenuAltLeft, BiBorderOuter } from 'react-icons/bi'
import { MdDashboard } from 'react-icons/md'
import { BsFillCalendarFill } from 'react-icons/bs'
import { GoLaw } from 'react-icons/go'
import { HiTemplate } from 'react-icons/hi'
import { FaShippingFast } from 'react-icons/fa'
import { ImPriceTags } from 'react-icons/im'
import { GrStackOverflow } from 'react-icons/gr'
import { RiShoppingBasketFill } from 'react-icons/ri'
import axios from 'axios'
import Config from '../../Config'
import React, { useEffect, useState } from 'react'

import { useSelector } from 'react-redux'

import './Sidebar.css'
import { useHistory } from 'react-router-dom'

const Sidebar = () => {
  const history = useHistory()

  const [userRole, setRoleData] = useState([])
  useEffect(() => {
    axios({
      method: 'GET',
      url: `http://${Config.IP}:${Config.PORT}/api/role`,
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        console.log(' Roles Data >>>>>>>>>>>', response.data)
        const usrRole = response.data.role.filter((d, i) => {
          console.log('usrRole>', d.name, d.name.match(/user/gi)?.length)
          return d.name.match(/user/gi)?.length > 0
        })[0]
        console
          .log('usrRoleeeeeeeee>>>', usrRole)

          .catch((error2) => {
            console.log('Error2 >>', error2)
          })
      })
      .catch((error) => {
        console.log('Error >>', error)
      })
  }, [])
  const stateData = useSelector((state) => state.login.user.data)
  let role = stateData.roles

  console.log('Role ==', role)

  const { name, roles, email, token } = useSelector(
    (state) => state.login.user.data
  )

  console.log('roles', roles)

  const [toggleSidebar, setToggleSidebar] = React.useState(false)

  const handleClick = () => {
    setToggleSidebar(!toggleSidebar)
  }

  return (
    <ProSidebar collapsed={toggleSidebar}>
      <Menu iconShape='square'>
        {roles[0] === 'ROLE_DOCTOR' ? (
          <>
            <MenuItem
              iconShape={'round'}
              onClick={() => handleClick()}
              icon={toggleSidebar ? <BiMenu /> : <BiMenuAltLeft />}
            >
              <div style={{ padding: '0.5rem' }}>
                <h4 style={{ color: '#fff' }}>Dr App</h4>
              </div>
            </MenuItem>
            <MenuItem
              onClick={() => history.push('')}
              iconShape='circle'
              icon={<MdDashboard />}
            >
              Dashboard
            </MenuItem>

            <SubMenu title='DoctorAppointment' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-doctorAppointment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Doctor Appointment
              </MenuItem>
              {/* `<MenuItem
                onClick={() => history.push('/add-doctorAppointment')}
                iconShape='round'
               
              >
                Add Doctor Appointment
              </MenuItem>` */}
            </SubMenu>

            <SubMenu title='Timing' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-timing')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Timing
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-timing')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Timing
              </MenuItem>
            </SubMenu>
            <SubMenu title='Fee' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-fee')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Fees
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-fee')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Fees
              </MenuItem>
            </SubMenu>
            <SubMenu title='Comment' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-comment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Comment
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-comment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Comment
              </MenuItem>
            </SubMenu>
          </>
        ) : (
          <>
            <SubMenu title='Doctor' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-udoctor')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Doctor
              </MenuItem>
            </SubMenu>
            {/* ==== User ==== */}
            <SubMenu title='User' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-user')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View User
              </MenuItem>
              {/* <MenuItem
                onClick={() => history.push('/add-user')}
                iconShape='round'
                
              >
                Add User
              </MenuItem> */}
            </SubMenu>
            <SubMenu title='Trending' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-trending')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Trending
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-trending')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Trending
              </MenuItem>
            </SubMenu>
            <SubMenu title='Comment' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-comment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Comment
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-comment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Comment
              </MenuItem>
            </SubMenu>

            {/* <SubMenu title='Symptom' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-symptom')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Symptom
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-symptom')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Symptom
              </MenuItem>
            </SubMenu> */}

            <SubMenu title='Category' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-category')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Category
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-category')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Category
              </MenuItem>
            </SubMenu>

            {/* <SubMenu title='Family' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-family')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Family
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-family')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Family
              </MenuItem>
            </SubMenu> */}

            {/* <SubMenu title='Appointment' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-appointment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Appointment
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-appointment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add Appointment
              </MenuItem>
            </SubMenu> */}

            {/* ==== DoctorAppointment ==== */}
            <SubMenu title='DoctorAppointment' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-doctorAppointment')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View Doctor Appointment
              </MenuItem>
              {/* `<MenuItem
                onClick={() => history.push('/add-doctorAppointment')}
                iconShape='round'
               
              >
                Add Doctor Appointment
              </MenuItem>` */}
            </SubMenu>

            {/* ==== Contact ==== */}
            <SubMenu title='Contact' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-contact')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View contact
              </MenuItem>
              {/* <MenuItem
                onClick={() => history.push('/add-contact')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add contact
              </MenuItem> */}
            </SubMenu>

            {/* <SubMenu title='MedicalFacility' icon={<ImPriceTags />}>
              <MenuItem
                onClick={() => history.push('/view-medicalFacility')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                View MedicalFacility
              </MenuItem>
              <MenuItem
                onClick={() => history.push('/add-medicalFacility')}
                iconShape='round'
                // icon={<HiDocumentText />}
              >
                Add MedicalFacility
              </MenuItem>
            </SubMenu> */}
          </>
        )}
      </Menu>
    </ProSidebar>
  )
}

export default Sidebar
