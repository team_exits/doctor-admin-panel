import React from 'react'
import { Field, ErrorMessage, useField } from 'formik'
import TextError from './TextError'
import { CommonContext } from '../../../context/CommonContextApi'

function Select(props) {
  const {
    label,
    name,
    options,
    level,
    handleMcq,
    handleStandard,
    handleValue,
    title,
    ...rest
  } = props

  const [field] = useField(props)
  console.log(title)

  const showTo = title || null

  const { show, close, setShow, allSelect } = React.useContext(CommonContext)

  React.useEffect(() => {
    if (field.value !== undefined) {
      console.log(field.value)
      if (showTo) {
        if (field.value == 'Standard') {
          handleStandard()
        } else if (field.value == 'MCQ') {
          handleMcq()
        } else {
          handleValue()
        }
        allSelect(field.value, name, level)
      }
    }
  }, [field.value])

  // console.log(props);
  return (
    <div className='form-control'>
      <label htmlFor={name}>{label}</label>
      <Field
        as='select'
        id={name}
        name={name}
        className='select'
        // onChange={(e) => setMyselect(e.target.value)}
        {...rest}
      >
        {options.map((option, index) => {
          // console.log(option);

          if (index === 0) {
            return (
              <option key={option.value} value={option.value} selected>
                {option.key}
              </option>
            )
          }

          return (
            <option key={option.value} value={option.value}>
              {option.key}
            </option>
          )
        })}
      </Field>
      <ErrorMessage component={TextError} name={name} />
    </div>
  )
}

export default Select
