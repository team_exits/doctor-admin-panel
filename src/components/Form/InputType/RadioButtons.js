import React from "react";
import { Field, ErrorMessage, useField } from "formik";
import TextError from "./TextError";
import { CommonContext } from "../../../context/CommonContextApi";

function RadioButtons(props) {
  const { label, name, options, level, ...rest } = props;

  const [isChecked, setIsChecked] = React.useState("");

  const [field] = useField(props);

  const { allSelect } = React.useContext(CommonContext);

  // console.log(field);

  React.useEffect(() => {
    console.log(isChecked);
    if (isChecked !== undefined) {
      // console.log(field);
      allSelect(isChecked, name, level);
    }
  }, [isChecked]);

  return (
    <div className="form-control">
      <label>{label}</label>
      <Field name={name}>
        {({ field }) => {
          let fields = field.value || "CNR_NO";
          // console.log(fields);
          setIsChecked(fields);
          return options.map((option) => {
            return (
              <React.Fragment key={option.key}>
                <input
                  type="radio"
                  id={option.value}
                  {...field}
                  {...rest}
                  value={option.value}
                  checked={fields === option.value}
                />
                <label htmlFor={option.value}>{option.key}</label>
              </React.Fragment>
            );
          });
        }}
      </Field>
      <ErrorMessage component={TextError} name={name} />
    </div>
  );
}

export default RadioButtons;
