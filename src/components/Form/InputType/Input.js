import { ErrorMessage, Field } from "formik";
import React from "react";
import TextError from "./TextError";

const Input = ({ label, name, value, ...rest }) => {
  return (
    <div className="form-control">
      <label htmlFor={name}>{label}</label>
      <Field className="input" id={name} name={name} {...rest} />
      <ErrorMessage name={name} component={TextError} />
    </div>
  );
};

export default Input;
