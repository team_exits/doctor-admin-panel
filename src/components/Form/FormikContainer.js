import React from 'react'

import { Formik, Form } from 'formik'
import FormikControl from './FormikControl'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { purple, blue } from '@material-ui/core/colors'
import './FormikContainer.css'

const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: '#3246D3',
    '&:hover': {
      backgroundColor: '#3457D5',
    },
  },
}))(Button)

const FormikContainer = ({
  inputData,
  onSubmit,
  handleStandard,
  handleMcq,
  handleValue,
}) => {
  console.log(inputData.initialInput)
  // const loginData = {
  //   formData: [
  //     { name: "email", type: "email", control: "input", label: "Email" },
  //     {
  //       name: "password",
  //       type: "password",
  //       control: "input",
  //       label: "Password",
  //     },
  //     //   {
  //     //     name: "selectOption",
  //     //     control: "select",
  //     //     label: "Select a topic",
  //     //     options: [
  //     //       { key: "Select an option", value: "" },
  //     //       { key: "Option 1", value: "option1" },
  //     //       { key: "Option 2", value: "option2" },
  //     //       { key: "Option 3", value: "option3" },
  //     //     ],
  //     //   },
  //   {
  //     name: "radioOption",
  //     type: "radio",
  //     control: "radio",
  //     label: "Radio Topic",
  //     options: [
  // { key: "Option 1", value: "option1" },
  // { key: "Option 2", value: "option2" },
  // { key: "Option 3", value: "option3" },
  //     ],
  //   },
  //   ],
  // };

  const validateInput = (value) => {
    let error
    if (!value) {
      error = 'Required'
    }

    return error
  }

  //   const validationSchema = Yup.object({
  //     email: Yup.string().required("Required"),
  //   });

  return (
    <Formik
      enableReinitialize
      initialValues={inputData.initialInput || {}}
      //   validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      {(formik) => {
        return (
          <Form>
            <div className={inputData.columns}>
              {inputData.formData.map((value) => {
                return (
                  <>
                    {!value.hideField && (
                      <FormikControl
                        control={value.control}
                        type={value.type}
                        label={value.label}
                        name={value.name}
                        options={value.options}
                        validate={validateInput}
                        level={value.level}
                        checked={value.checked}
                        handleStandard={handleStandard}
                        handleMcq={handleMcq}
                        handleValue={handleValue}
                        title={inputData.title}
                      />
                    )}
                  </>
                )
              })}
            </div>
            <ColorButton fullWidth type='submit'>
              <b>Submit</b>
            </ColorButton>
          </Form>
        )
      }}
    </Formik>
  )
}

export default React.memo(FormikContainer)
