import React from 'react'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/Notifications'
import IconButton from '@material-ui/core/IconButton'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import './Navbar.css'
import { useHistory, Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setLogout } from '../../redux/Slice/loginSlice'

const Navbar = () => {
  const history = useHistory()

  const dispatch = useDispatch()

  return (
    <div
      // style={{ boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px" }}
      className='navbar'
    >
      <div>
        <h1
          onClick={() => history.push('/')}
          style={{
            color: '#F2F3F4',
            paddingLeft: '2rem',
            cursor: 'pointer',
          }}
        >
          Dr App
        </h1>
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div>
          <IconButton>
            <NotificationsIcon style={{ color: '#fff' }} />
          </IconButton>
        </div>
        <div>
          <IconButton>
            <MailIcon style={{ color: '#fff' }} />
          </IconButton>
        </div>
        <div>
          <IconButton>
            <Link to='/profile'>
              {' '}
              <AccountCircle style={{ color: '#fff' }} />
            </Link>
          </IconButton>
        </div>
        <div>
          <IconButton onClick={() => dispatch(setLogout())}>
            <ExitToAppIcon style={{ color: '#fff' }} />
          </IconButton>
        </div>
      </div>
    </div>
  )
}

export default Navbar
