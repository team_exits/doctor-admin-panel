import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { setModal } from "../../redux/Slice/caseSlice";
import { useDispatch } from "react-redux";
import { Button } from "@material-ui/core";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
  },
}));

export default function SimpleModal({ toggle }) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const dispatch = useDispatch();

  const handleClose = () => {
    dispatch(setModal(false));
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <CheckCircleOutlineIcon style={{ height: 100, width: 100 }} />
      <h2 id="simple-modal-title">New Case Added.</h2>
      <br />
      <p id="simple-modal-description">
        {/* Duis mollis, est non commodo luctus, nisi erat porttitor ligula. */}
        <Button
          variant="contained"
          onClick={() => dispatch(setModal(false))}
          color="primary"
        >
          Close
        </Button>
      </p>
    </div>
  );

  return (
    <div>
      <Modal
        open={toggle}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
