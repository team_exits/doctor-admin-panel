import * as Config from "../../../Config";

const select2Template = {
  type: "select",
  // type: "select2",
  name: "${dbtc.name}",
  label: "${capitalize(dbtc.name)}",
  props: {
    api: {
      method: "GET",
      //   url: {Config.BASE_URL + "/moduleNameLC"},
      headers: {},
      params: {},
    },
    pick: {
      // _id: "name",
      // _id: function (obj) {
      //   return (
      //     obj.name + "_" + obj.cover + ">>>>HELLO WORLD"
      //   );
      // },
      // _id: ["name","-","createdAt","+++++","_id"],
    },
    // // BELOW OPTIONS FORMAT ONLY FOR SELECT API SELECTS
    options: [
      {
        value: null,
        label: "Select An Option",
        color: "#FF5630",
        //   isDisabled: true,
        isFixed: true,
      },
      {
        value: "other",
        label: "Other Option",
        color: "#0052CC",
      },
    ],
    // // BELOW OPTIONS FORMAT FOR NORMAL SELECTS
    // options: [{ none: "Select An Option" }, { other: "Other Option" }],
    // onChangeHandler: (name, value) => {
    //   console.log("this > onChangeHandler", name, value);
    // },
  },
};

const selectTemplate = {
  type: "editor",
  name: "${dbtc.name}",
  label: "${capitalize(dbtc.name)}",
  props: {
    options: [{ male: "Male" }, { female: "Female" }, { other: "Other" }],
  },
  // onChangeHandler: (name, value) => {
  //   console.log("this > onChangeHandler", name, value);
  // },
};

const textTemplate = {
  type: "text",
  name: "${dbtc.name}",
  label: "${capitalize(dbtc.name)}",
  props: {},
  // onChangeHandler: (name, value) => {
  //   console.log("CRFN", name, value);
  // },
};

const fileTemplate = {
  type: "file",
  name: "${dbtc.name}",
  label: "${capitalize(dbtc.name)}",
  //   value: "17-12-1991",
  props: {
    preview: true,
    upsert: false,
    model: "${moduleName}", // DB SCHEMA NAME IN MONGO
    model_id: null, //DB ID will be autopopulated Later on if pre-defined will lead to edit mode
    storage_path: "${moduleName}", //Folder Name for Storing Files
    model_key: "${moduleNameLC}_file", //column to be updated
    api: {
      method: "POST",
      //   url:{Config.BASE_URL + "/upload"} ,
      //   getUrl:{Config.BASE_URL + "/"} ,
      headers: {},
    },
  },
};

const radioTemplate = {
  type: "radio",
  name: "${dbtc.name}",
  label: "${capitalize(dbtc.name)}",
  props: {
    options: [{ 0: "Select an Option" }, { 1: "Other" }],
  },
};
