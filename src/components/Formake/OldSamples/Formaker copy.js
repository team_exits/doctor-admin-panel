import React from "react";
import { IconContext } from "react-icons";
import { GiProgression } from "react-icons/gi";
import { MdUpdate } from "react-icons/md";
import AuthForm from "../../../components/Form/AuthForm/AuthForm";
import FormikContainer from "../../../components/Form/FormikContainer";
import FormGenerator from "./FormGenerator";
import Formake from "./Formake";

import "../Formake/Formake.css";
import moment from "moment";

export default function Formaker() {
  const loginState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
    ],
    title: "Stacked Form",
    icon: <GiProgression />,
    buttonText: "Login",
    classForField: "col-md-12",
  };

  const horState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
    ],
    title: "Horizontal Form",
    icon: <GiProgression />,
    buttonText: "Login",
    classForField: "col-md-12",
  };

  const RegisterState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Current Password",
      },
      {
        name: "selectOption",
        control: "select",
        label: "Select a topic",
        options: [
          { key: "Select an option", value: "" },
          { key: "Option 1", value: "option1" },
          { key: "Option 2", value: "option2" },
          { key: "Option 3", value: "option3" },
        ],
      },
      {
        name: "radioOption",
        type: "radio",
        control: "radio",
        label: "Radio Topic",
        options: [
          { key: "Option 1", value: "option1" },
          { key: "Option 2", value: "option2" },
          { key: "Option 3", value: "option3" },
        ],
      },
    ],
    title: "Register Form",
    icon: <GiProgression />,
    buttonText: "Register",
    classForField: "col-md-12",
  };

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="Regular-form">
              <Formake
                useAxios={true}
                // edit={true}
                edit={false}
                // editId={"60de96a7e459890982f60399"}
                createApi="http://192.168.2.107:8081/api/food"
                createType="POST"
                editApi="http://192.168.2.107:8081/api/food"
                editType="GET"
                updateApi="http://192.168.2.107:8081/api/food"
                updateType="PUT"
                deleteApi="http://192.168.2.107:8081/api/food"
                deleteType="DELETE"
                auth={{ Authorization: "Bearer " + "TOKEN" }}
                headers={{}}
                params={{}}
                initialState={{
                  name: null,
                  cover_pic: null,
                  pic1: null,
                  pic2: null,
                  pic3: null,
                  pic4: null,
                  pic5: null,
                  veg: null,
                  desc: null,
                  ingredients: null,
                  role: "60de5f1ad7e44cace70dca7a",
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[8, 4]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log("formValues UPDATING --> ", formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log("formValuesx UPDATING --> ", name, value);
                }}
                onSubmit={(values) =>
                  console.log("formValues SUBMITTING", values)
                }
                fields={[
                  [
                    {
                      type: "select2",
                      name: "role",
                      label: "Food role",
                      props: {
                        api: {
                          method: "GET",
                          url: "http://192.168.2.107:8081/api/food",
                          headers: {},
                          params: {},
                        },
                        pick: {
                          _id: "name",
                          // _id: ["name","-","created_at","+++++","_id"],
                        },
                        // options: [
                        //   {
                        //     value: "blue",
                        //     label: "Blue",
                        //     color: "#0052CC",
                        //     isDisabled: true,
                        //   },
                        //   {
                        //     value: "red",
                        //     label: "Red",
                        //     color: "#FF5630",
                        //     isFixed: true,
                        //   },
                        //   { value: "green", label: "Green", color: "#36B37E" },
                        // ],
                      },
                    },
                    {
                      type: "text",
                      name: "name",
                      label: "Food Name",
                      props: {},
                    },
                    {
                      type: "file",
                      name: "cover_pic",
                      label: "Cover Pic",
                      //   value: "17-12-1991",
                      props: {
                        preview: true,
                        upsert: false,
                        model: "food", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "Food", //Folder Name for Storing Files
                        model_key: "cover_pic", //column to be updated
                        api: {
                          method: "POST",
                          url: "http://192.168.2.107:8081/upload",
                          getUrl: "http://192.168.2.107:8081/",
                          headers: {},
                        },
                      },
                    },
                    {
                      type: "radio",
                      name: "veg",
                      label: "Veg",
                      props: {
                        options: [{ 1: "Veg" }, { 0: "NonVeg" }],
                      },
                    },
                    {
                      type: "editor",
                      name: "ingredients",
                      label: "Ingredients",
                      props: {
                        preview: true,
                      },
                    },
                    {
                      type: "text",
                      name: "desc",
                      label: "Description",
                      props: {
                        rows: 10,
                      },
                    },
                  ],
                  [
                    {
                      type: "file",
                      name: "pic1",
                      label: "Pic #1",
                      //   value: "17-12-1991",
                      props: {
                        model: "food", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "Food", //Folder Name for Storing Files
                        model_key: "pic1", //column to be updated
                        api: {
                          method: "POST",
                          url: "http://192.168.2.107:8081/upload",
                          headers: {},
                        },
                      },
                    },
                    {
                      type: "file",
                      name: "pic2",
                      label: "Pic #2",
                      //   value: "17-12-1991",
                      props: {
                        model: "food", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "Food", //Folder Name for Storing Files
                        model_key: "pic2", //column to be updated
                        api: {
                          method: "POST",
                          url: "http://192.168.2.107:8081/upload",
                          headers: {},
                        },
                      },
                    },
                    {
                      type: "file",
                      name: "pic3",
                      label: "Pic #3",
                      //   value: "17-12-1991",
                      props: {
                        model: "food", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "Food", //Folder Name for Storing Files
                        model_key: "pic3", //column to be updated
                        api: {
                          method: "POST",
                          url: "http://192.168.2.107:8081/upload",
                          headers: {},
                        },
                      },
                    },
                    {
                      type: "file",
                      name: "pic4",
                      label: "Pic #4",
                      //   value: "17-12-1991",
                      props: {
                        model: "food", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "Food", //Folder Name for Storing Files
                        model_key: "pic4", //column to be updated
                        api: {
                          method: "POST",
                          url: "http://192.168.2.107:8081/upload",
                          headers: {},
                        },
                      },
                    },
                    {
                      type: "file",
                      name: "pic5",
                      label: "Pic #5",
                      //   value: "17-12-1991",
                      props: {
                        model: "food", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "Food", //Folder Name for Storing Files
                        model_key: "pic5", //column to be updated
                        api: {
                          method: "POST",
                          url: "http://192.168.2.107:8081/upload",
                          headers: {},
                        },
                      },
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
