import React, { useParams } from "react";
import Formake from "./Formake";
import Config from "../../../Config";

import "../Formake/Formake.css";

export default function Formaker(props) {
  const { token } = props;
  const { id } = useParams();
  return (
    <>
      <div className="containerc-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="Regular-form">
              <Formake
                useAxios={true}
                // edit={false}
                edit={id ? true : false}
                editId={id}
                createApi={Config.API_URL + "/moduleNameLC"}
                createType="POST"
                editApi={Config.API_URL + "/moduleNameLC"}
                editType="GET"
                updateApi={Config.API_URL + "/moduleNameLC"}
                updateType="PUT"
                deleteApi={Config.API_URL + "/moduleNameLC"}
                deleteType="DELETE"
                auth={{ Authorization: "Bearer " + "TOKEN" }}
                headers={{}}
                params={{}}
                initialState={{
                  name: null,
                  cover_pic: null,
                  pic1: null,
                  pic2: null,
                  pic3: null,
                  pic4: null,
                  pic5: null,
                  veg: null,
                  desc: null,
                  levels: "60deacc31c0b9a091c2cb2f4",
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[8, 4]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log("formValues UPDATING --> ", formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log("formValuesx UPDATING --> ", name, value);
                }}
                onSubmit={(values) =>
                  console.log("formValues SUBMITTING", values)
                }
                fields={[]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
