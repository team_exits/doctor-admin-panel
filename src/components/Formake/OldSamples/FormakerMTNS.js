import React from "react";
import { IconContext } from "react-icons";
import { GiProgression } from "react-icons/gi";
import { MdUpdate } from "react-icons/md";
import AuthForm from "../../../components/Form/AuthForm/AuthForm";
import FormikContainer from "../../../components/Form/FormikContainer";
import FormGenerator from "./FormGenerator";
import Formake from "./Formake";

import "../Formake/Formake.css";
import moment from "moment";

export default function Formaker() {
  const loginState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
    ],
    title: "Stacked Form",
    icon: <GiProgression />,
    buttonText: "Login",
    classForField: "col-md-12",
  };

  const horState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
    ],
    title: "Horizontal Form",
    icon: <GiProgression />,
    buttonText: "Login",
    classForField: "col-md-12",
  };

  const RegisterState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Current Password",
      },
      {
        name: "selectOption",
        control: "select",
        label: "Select a topic",
        options: [
          { key: "Select an option", value: "" },
          { key: "Option 1", value: "option1" },
          { key: "Option 2", value: "option2" },
          { key: "Option 3", value: "option3" },
        ],
      },
      {
        name: "radioOption",
        type: "radio",
        control: "radio",
        label: "Radio Topic",
        options: [
          { key: "Option 1", value: "option1" },
          { key: "Option 2", value: "option2" },
          { key: "Option 3", value: "option3" },
        ],
      },
    ],
    title: "Register Form",
    icon: <GiProgression />,
    buttonText: "Register",
    classForField: "col-md-12",
  };

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="Regular-form">
              <Formake
                useAxios={true}
                // edit={true}
                edit={true}
                editId={"60debdfa1c0b9a091c2cb2fe"}
                createApi="http://192.168.2.107:5020/api/question"
                createType="POST"
                editApi="http://192.168.2.107:5020/api/question"
                editType="GET"
                updateApi="http://192.168.2.107:5020/api/question"
                updateType="PUT"
                deleteApi="http://192.168.2.107:5020/api/question"
                deleteType="DELETE"
                auth={{ Authorization: "Bearer " + "TOKEN" }}
                headers={{}}
                params={{}}
                initialState={{
                  name: null,
                  cover_pic: null,
                  pic1: null,
                  pic2: null,
                  pic3: null,
                  pic4: null,
                  pic5: null,
                  veg: null,
                  desc: null,
                  levels: "60deacc31c0b9a091c2cb2f4",
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[8, 4]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log("formValues UPDATING --> ", formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log("formValuesx UPDATING --> ", name, value);
                }}
                onSubmit={(values) =>
                  console.log("formValues SUBMITTING", values)
                }
                fields={[
                  [
                    {
                      type: "select2",
                      name: "levels",
                      label: "Level",
                      props: {
                        api: {
                          method: "GET",
                          url: "http://192.168.2.107:5020/api/level",
                          headers: {},
                          params: {},
                        },
                        pick: {
                          _id: "name",
                          // _id: ["name","-","created_at","+++++","_id"],
                        },
                        // options: [
                        //   {
                        //     value: "blue",
                        //     label: "Blue",
                        //     color: "#0052CC",
                        //     isDisabled: true,
                        //   },
                        //   {
                        //     value: "red",
                        //     label: "Red",
                        //     color: "#FF5630",
                        //     isFixed: true,
                        //   },
                        //   { value: "green", label: "Green", color: "#36B37E" },
                        // ],
                      },
                    },
                    {
                      type: "select",
                      name: "questionType",
                      label: "Question Type",
                      props: {
                        options: [{ MCQ: "MCQ" }, { STANDARD: "Standard" }],
                      },
                    },
                  ],
                  [
                    {
                      type: "text",
                      name: "question",
                      label: "Question",
                      props: {},
                    },
                    {
                      type: "text",
                      name: "answer",
                      label: "Answer",
                      props: {},
                    },
                    {
                      type: "text",
                      name: "points",
                      label: "Points",
                      // props: {},
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
