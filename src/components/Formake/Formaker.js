import React from "react";
import { IconContext } from "react-icons";
import { GiProgression } from "react-icons/gi";
import { MdUpdate } from "react-icons/md";
import Formake from "./Formake";

import "./Formake.css";
import moment from "moment";

export default function Formaker() {
  const loginState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
    ],
    title: "Stacked Form",
    icon: <GiProgression />,
    buttonText: "Login",
    classForField: "col-md-12",
  };

  const horState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
    ],
    title: "Horizontal Form",
    icon: <GiProgression />,
    buttonText: "Login",
    classForField: "col-md-12",
  };

  const RegisterState = {
    formData: [
      {
        name: "name",
        type: "text",
        control: "input",
        label: "Name",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Password",
      },
      {
        name: "description",
        type: "text",
        control: "input",
        label: "Current Password",
      },
      {
        name: "selectOption",
        control: "select",
        label: "Select a topic",
        options: [
          { key: "Select an option", value: "" },
          { key: "Option 1", value: "option1" },
          { key: "Option 2", value: "option2" },
          { key: "Option 3", value: "option3" },
        ],
      },
      {
        name: "radioOption",
        type: "radio",
        control: "radio",
        label: "Radio Topic",
        options: [
          { key: "Option 1", value: "option1" },
          { key: "Option 2", value: "option2" },
          { key: "Option 3", value: "option3" },
        ],
      },
    ],
    title: "Register Form",
    icon: <GiProgression />,
    buttonText: "Register",
    classForField: "col-md-12",
  };

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="Regular-form">
              <Formake
                useAxios={true}
                // edit={true}
                // edit={true}
                // editId={"60cc97dc812e107d9c9f486b"}
                createApi="http://192.168.2.107:5020/api/question"
                createType="POST"
                editApi="http://localhost:5020/api/question"
                editType="GET"
                updateApi="http://192.168.2.107:5020/api/question"
                updateType="PUT"
                deleteApi="http://192.168.2.107:5020/api/question"
                deleteType="DELETE"
                auth={{ Authorization: "Bearer " + "TOKEN" }}
                headers={{}}
                params={{}}
                initialState={{
                  question: null,
                  cover_pic: null,
                  pic1: null,
                  pic2: null,
                  pic3: null,
                  pic4: null,
                  pic5: null,
                  veg: null,
                  desc: null,
                  // mcq: {
                  //   mcq1: "Hello",
                  //   mcq2: "world",
                  //   mcq3: "!!!!!",
                  // },
                  mcq: {
                    inf: (obj) => {
                      return {
                        mcq1: "Hello",
                        mcq2: "world",
                        // mcq3: "!!!!!",
                      };
                    },
                    def: (obj) => {
                      console.log("MCQx DEFLATER", obj);
                      return "world";
                    },
                  },
                  levels: {
                    inf: (obj) => {
                      console.log("Formake INF", obj);
                      return "hello";
                    },
                    // inf: "Anto", // can be string inflater or deflater also
                    // def: "60cc97c0812e107d9c9f486a", // can be string inflater or deflater also
                    def: (obj) => {
                      console.log("deflater", obj);
                      if (typeof obj == "object") {
                        return obj[0]._id || obj[0];
                      } else {
                        return null;
                      }
                    },
                  },
                }}
                // colwidths in number totaling upto 12 e.g. 8+4= 12
                cols={[8, 4]}
                spacing={3}
                isSubmitting={false}
                //used for default change handler functions having switch case inside the to set proper setState
                updatedValues={(formValues) =>
                  console.log("formValuesxxx UPDATING --> ", formValues)
                }
                onChangeHandler={(name, value) => {
                  console.log("formValuesx UPDATING --> ", name, value);
                }}
                onSubmit={(values) =>
                  console.log("formValues SUBMITTING", values)
                }
                fields={[
                  [
                    {
                      type: "select2",
                      name: "levels",
                      label: "Level",
                      props: {
                        api: {
                          method: "GET",
                          url: "http://192.168.2.107:5020/api/level",
                          headers: {},
                          params: {},
                        },
                        pick: {
                          _id: "name",
                          // _id: ["name","-","created_at","+++++","_id"],
                        },
                        // options: [
                        //   {
                        //     value: "blue",
                        //     label: "Blue",
                        //     color: "#0052CC",
                        //     isDisabled: true,
                        //   },
                        //   {
                        //     value: "red",
                        //     label: "Red",
                        //     color: "#FF5630",
                        //     isFixed: true,
                        //   },
                        //   { value: "green", label: "Green", color: "#36B37E" },
                        // ],
                      },
                    },
                    {
                      type: "mcq",
                      name: "mcq",
                      label: "MCQ",
                      props: {
                        // options: [
                        //   { male: "Male" },
                        //   { female: "Female" },
                        //   { other: "Other" },
                        // ],
                        left: "$",
                      },
                    },
                    {
                      type: "select2",
                      name: "levels",
                      label: "Level",
                      props: {
                        api: {
                          method: "GET",
                          url: "http://192.168.2.107:5020/api/level",
                          headers: {},
                          params: {},
                        },
                        pick: {
                          _id: "name",
                          _id: function (obj) {
                            return (
                              obj.name + "_" + obj.cover + ">>>>HELLO WORLD"
                            );
                          },
                          // _id: ["name","-","createdAt","+++++","_id"],
                        },
                        // options: [
                        //   {
                        //     value: "blue",
                        //     label: "Blue",
                        //     color: "#0052CC",
                        //     isDisabled: true,
                        //   },
                        //   {
                        //     value: "red",
                        //     label: "Red",
                        //     color: "#FF5630",
                        //     isFixed: true,
                        //   },
                        //   { value: "green", label: "Green", color: "#36B37E" },
                        // ],
                      },
                    },
                    {
                      type: "date",
                      name: "date",
                      label: "DOBx",
                      // value: "17-12-1991",
                      props: {
                        subTitle: "Enter Birthdate",
                        // style: { backgroundColor: "#f00" },
                        inputFormat: "DD-MM-YYYY",
                        outputFormat: "Do of MMMM 'YY",
                      },
                    },
                    {
                      type: "date",
                      name: "dates",
                      label: "Date Of Birth",
                      // value: "01-01-2001",
                      props: {
                        subTitle: "Enter Birthdate",
                        // style: { backgroundColor: "#f00" },
                        inputFormat: "DD-MM-YYYY",
                        outputFormat: "Do of MMMM 'YY",
                      },
                    },
                    {
                      type: "time",
                      name: "timedob",
                      label: "Time",
                      // value: "04:16",
                      props: {
                        subTitle: "Enter Birth Timing",
                        // style: { backgroundColor: "#f00" },
                        inputFormat: "hh:mm",
                        outputFormat: "HH:mm",
                      },
                    },
                    {
                      type: "select",
                      name: "gender",
                      label: "Gender",
                      props: {
                        options: [
                          { male: "Male" },
                          { female: "Female" },
                          { other: "Other" },
                        ],
                        left: "$",
                      },
                    },
                    {
                      type: "select",
                      name: "group",
                      label: "Group",
                      props: {
                        options: [
                          { male: "Male" },
                          { female: "Female" },
                          { other: "Other" },
                        ],
                        left: "$",
                      },
                    },
                    {
                      type: "editor",
                      name: "bio",
                      label: "Your BIO",
                      props: {
                        preview: true,
                        options: [
                          { male: "Male" },
                          { female: "Female" },
                          { other: "Other" },
                        ],
                        left: "$",
                      },
                      // onChangeHandler: (name, value) => {
                      //   console.log("CRFN", name, value);
                      // },
                    },
                    // {
                    //   type: "paste",
                    // },
                  ],
                  [
                    {
                      type: "text",
                      name: "question",
                      label: "First Name",
                      props: {
                        options: [
                          { male: "Male" },
                          { female: "Female" },
                          { other: "Other" },
                        ],
                        left: "$",
                      },
                      // onChangeHandler: (name, value) => {
                      //   console.log("CRFN", name, value);
                      // },
                    },
                    {
                      type: "textarea",
                      name: "addressss",
                      label: "Address",
                      props: {
                        options: [
                          { male: "Male" },
                          { female: "Female" },
                          { other: "Other" },
                        ],
                        left: "$",
                      },
                      // onChangeHandler: (name, value) => {
                      //   console.log("CRFN", name, value);
                      // },
                    },
                    {
                      type: "radio",
                      name: "genderXXXX",
                      label: "Gender",
                      props: {
                        options: [
                          { male: "Male" },
                          { female: "Female" },
                          { other: "Other" },
                        ],
                        left: "$",
                      },
                    },
                    {
                      type: "checkboxes",
                      name: "ageroup",
                      label: "Age Group",
                      props: {
                        options: [
                          { young: "18 - 25" },
                          { adult: "25 - 64" },
                          { senior: "65+" },
                        ],
                        left: "$",
                      },
                    },

                    {
                      type: "file",
                      name: "picx",
                      label: "ProfilePic",
                      //   value: "17-12-1991",
                      placeholder: "enter DOB",
                      props: {
                        model: "user", // DB SCHEMA NAME IN MONGO
                        model_id: null, //DB ID will be autopopulated Lateron
                        storage_path: "User", //Folder Name for Storing Files
                        model_key: "images", //column to be updated
                      },
                    },
                    {
                      type: "custom",
                      component: (
                        <div>
                          <h1>T&C</h1>
                          <img
                            src="https://www.cnet.com/a/img/pLwWAw3f1OdjhUWe1-u0jcqhIcI=/1200x630/2019/09/10/d3dc3047-4c7f-499e-8b8c-240b3e27c6d2/google-logo-6.jpg"
                            style={{ width: 100, height: 100 }}
                          />
                        </div>
                      ),
                    },

                    {
                      type: "checkbox",
                      name: "tnc",
                      label: "I Agree",
                      value: "yes",
                      props: {
                        left: "$",
                      },
                    },
                  ],
                ]}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
