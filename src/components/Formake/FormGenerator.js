import React, { useEffect, useState, useRef } from "react";
import { IconContext } from "react-icons";
import { GiProgression } from "react-icons/gi";
import { MdUpdate } from "react-icons/md";
import { BsTrash } from "react-icons/bs";
import Input from "./Input";

import "./Formake.css";
import axios from "axios";

export default function FormGenerator(props) {
  const [submitting, setSubmitting] = useState(false);
  const [savedValues, setsavedValues] = useState(false);
  const [localProgress, setLocalProgress] = useState(false);

  useEffect(() => {
    setSubmitting(props.isSubmitting);
  }, [props.isSubmitting]);
  useEffect(() => {
    setsavedValues(props.savedValues);
    if (props.progress) {
      setLocalProgress(props.progress);
    } else {
      const total = Object.keys(props.savedValues).length;
      let currentProgress = Object.values(props.savedValues).filter((svf) => {
        if (typeof svf === "object") {
          return (
            svf !== null &&
            typeof svf !== "undefined" &&
            svf !== "undefined" &&
            Object.keys(svf).length
          );
        } else {
          return (
            svf !== null && typeof svf !== "undefined" && svf !== "undefined"
          );
        }
      }).length;

      currentProgress = (currentProgress / 10) * 100;
      setLocalProgress(currentProgress > 100 ? 100 : currentProgress);
    }
    // finallySubmit()
  }, [props.progress, props.savedValues]);
  const generateFields = () => {
    const frmFields =
      typeof props.fields === "undefined"
        ? null
        : props.fields.map((fieldList, index) => {
          const len = props.fields.length;
          let colWidth = Math.ceil(12 / len);
          if (typeof props.cols === "object") {
            colWidth = props.cols[index];
          }

          const rndr = () => {
            return (
              fieldList &&
              fieldList.map((thisFormField) => {
                const handleInputChange = (
                  name,
                  value,
                  type = "normal",
                  behaviour = "set"
                ) => {
                  if (props.onChangeHandler) {
                    props.onChangeHandler(name, value, type, behaviour);
                  } else {
                  }
                };

                return (
                  <>
                    {thisFormField.type === "custom" ? (
                      <>{thisFormField.component}</>
                    ) : (
                      <>
                        <div
                          //   label
                          class=""
                          style={{
                            color: "#000",
                            marginTop: 10,
                            paddingTop: 0,
                            ...thisFormField.labelStyle,
                          }}
                        >
                          {thisFormField.label}
                        </div>
                        <Input
                          type={thisFormField.type}
                          name={thisFormField.name}
                          label={thisFormField.label}
                          value={thisFormField.value}
                          handleChange={(
                            name,
                            value,
                            type = "normal",
                            behaviour = "set"
                          ) => {
                            handleInputChange(name, value, type, behaviour);
                          }}
                          handleSubmission={(values) => {
                            handleSubmit(values);
                          }}
                          isSubmitting={submitting}
                          {...thisFormField.props}
                        />
                      </>
                    )}
                  </>
                );
              })
            );
          };

          return typeof props.cols !== "undefined" ? (
            <div
              class={`col-xs-${colWidth} col-sm-${colWidth} col-md-${colWidth} col-lg-${colWidth}`}
            >
              {rndr()}
            </div>
          ) : (
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              {rndr()}
            </div>
          );
        });
    return frmFields;
  };
  const handleSubmit = () => {
    setSubmitting(false);

    props.onSubmitHandler();
  };
  const handleDelete = () => {
    setSubmitting(false);
    props.onDeleteHandler();
  };
  // const finallySubmit = () => {
  //   setTimeout(() => {
  //     setSubmitting(true);
  //   }, 200);
  // };

  return (
    <div className="row">
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div
          style={{
            display: "flex",
            marginBottom: 20,
            width: "100%",
            flex: 1,
            flexGrow: 1,
          }}
        >
          <div style={{ marginTop: -40 }}>
            <IconContext.Provider value={{ className: "extended-card-icon" }}>
              <div
                style={{
                  padding: "1.5rem 2rem",
                  backgroundColor: "#de2668",
                  borderRadius: 3,
                }}
              >
                <MdUpdate />
              </div>
            </IconContext.Provider>
          </div>
          <div
            style={{
              display: "flex",
              flexGrow: 1,
              flexDirection: "row",
              justifyContent: "flex-start",
              marginTop: -25,
              alignItems: "center",
              width: "100%",
              maxWidth: "100%",
              color: "#000",
            }}
          >
            <div
              style={{
                // marginTop: -10,
                paddingLeft: 10,
                fontWeight: "normal",
                width: "40%",
                maxWidth: "40%",
                fontSize: "1.3em",
              }}
            >
              {props.title || "Form Generator"}
            </div>
            <div style={{ width: "100%", maxWidth: "40%", marginTop: 20 }}>
              {props.editId ? (
                <div
                  style={{
                    height: 20,
                    width: 100,
                    marginTop: -15,
                  }}
                >
                  <span
                    className="label label-info"
                    style={{ marginRight: 10 }}
                  >
                    DB ID:{" "}
                  </span>
                  {props.editId}
                </div>
              ) : (
                <div
                  className="progress"
                  style={{
                    height: 20,
                    width: 100,
                    // marginTop: -5,
                  }}
                >
                  <div
                    className="progress-bar progress-bar-striped progress-bar-animated"
                    role="progress-bar"
                    aria-valuenow={localProgress}
                    aria-valuemin="0"
                    aria-valuemax="100"
                    style={{ width: localProgress, height: 20 }}
                  >
                    {localProgress}%
                  </div>
                </div>
              )}
            </div>
            {props.editId ? (
              <div style={{ width: "100%", maxWidth: "10%" }}>
                <button
                  type="button"
                  className="btn  btn-danger"
                  style={{ paddingBottom: 2, marginBottom: 0, marginTop: 10 }}
                  onClick={handleDelete}
                >
                  <BsTrash
                    style={{
                      fontSize: "1.3em",
                      paddingBottom: 0,
                      marginBottom: 0,
                    }}
                  />
                </button>
              </div>
            ) : null}
          </div>
        </div>
      </div>
      {generateFields()}
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          {submitting ? (
            <div>
              {/* props.progress {props.progress} <br /> */}
              <div
                className="progress"
                style={{ height: 30, width: 150, marginTop: 20 }}
              >
                <div
                  className="progress-bar progress-bar-striped progress-bar-animated"
                  role="progressbar"
                  aria-valuenow={props.progress}
                  aria-valuemin="0"
                  aria-valuemax="100"
                  style={{ width: `${props.progress || 50}%`, height: 30 }}
                ></div>
              </div>
            </div>
          ) : (
            <div
              style={{
                flex: 1,
                flexDirection: "column",
                flexGrow: 1,
                color: "#000",
                width: "100%",
                height: "100%",
              }}
            >
              <div>
                <button
                  className="btn"
                  style={{
                    backgroundColor: "#e91e63",
                    color: "white",
                    padding: "5px 50px",
                    marginTop: 20,
                  }}
                  type="button"
                  onClick={handleSubmit}
                >
                  <b>Submit</b>
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
