import React, { useState, useEffect } from "react";

import { BsTrash } from "react-icons/bs";

const MCQ = (props) => {
  console.log("MCQxProps", props);
  const [mcqs, setMCQs] = useState({});
  useEffect(() => {
    if (props.value) {
      setMCQs(props.value);
    }
  }, [props.value]);
  const handleChanges = (name, value, type = "normal", behaviour = "set") => {
    console.log("MCQxCDU", name, value, mcqs, props);
    let newMCQs = { ...mcqs, [name]: value };
    setMCQs(newMCQs);
    props.onChange(props.name, mcqs);
  };
  const addRows = () => {
    console.log("ADDMCQ", Object.keys(mcqs).length, mcqs);
    let name = props.name + (+Object.keys(mcqs).length + +1);
    let newMCQs = { ...mcqs, [name]: null };
    setMCQs(newMCQs);
  };
  const delRow = (name, i) => {
    let updatedMCQs = mcqs;
    // let updatedMCQs = Object.keys(mcqs).map((key, i) => {
    //   console.log("DELMCQ", key);
    //   return key !== name ? mcqs[key] : false;
    // });
    delete updatedMCQs[name];
    let newMCQs = {};
    Object.values(updatedMCQs).map((mcqItem, i) => {
      let mcqName = props.name + (+i + +1);
      newMCQs = { ...newMCQs, [mcqName]: mcqItem };
    });

    // let mcqName = props.name + (+Object.keys(mcqs).length + +1);
    // let newMCQs = { ...mcqs, [mcqName]: null };
    setMCQs(newMCQs);
    props.onChange(props.name, newMCQs);
  };
  return (
    <div>
      <span
        class="label label-danger"
        style={{ marginBottom: 1, cursor: "pointer" }}
        onClick={() => {
          addRows();
        }}
      >
        <b> + Add Another </b>
      </span>
      {Object.keys(mcqs).map((mcq, i) => {
        console.log("MCQx loop", mcq);
        const thisMCQ = mcqs[mcq];

        return (
          <>
            <div
              class="row"
              style={{
                display: "flex",
                flex: 1,
                height: "100%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                class="col-xs-1 col-sm-1 col-md-1 col-lg-1"
                style={{ marginTop: 10 }}
              >
                <b>#&nbsp;{i + 1}</b>
              </div>

              <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <input
                  type="text"
                  name={mcq}
                  className="form-control"
                  value={thisMCQ}
                  onChange={(evt) => {
                    handleChanges(mcq, evt.target.value);
                  }}
                  //   key={}
                />
              </div>

              <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <button
                  type="button"
                  className="btn btn-danger btn-sm btn-small"
                  style={{ marginTop: 5 }}
                  onClick={() => {
                    delRow(mcq, i);
                  }}
                >
                  <BsTrash
                    style={{
                      // fontSize: "1.3em",
                      // paddingBottom: 0,
                      // marginBottom: 0,
                      marginTop: 3,
                      marginBottom: 0,
                    }}
                  />
                </button>
              </div>
            </div>
          </>
        );
      })}
    </div>
  );
};

export default MCQ;
